#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import base

import cw

from typing import Optional


class Package(base.CWBinaryBase):
    """widファイルの情報カードのデータ。
    type:InfoCardと区別が付くように、Packageは暫定的に"7"とする。
    """
    def __init__(self, parent: None, f: "cw.binary.cwfile.CWFile", yadodata: bool = False, nameonly: bool = False,
                 materialdir: str = "Material", image_export: bool = True) -> None:
        from . import event

        base.CWBinaryBase.__init__(self, parent, f, yadodata, materialdir, image_export)
        self.type = 7
        f.dword()  # 不明
        self.name = f.string()
        self.id = f.dword()
        if nameonly:
            return
        events_num = f.dword()
        self.events = [event.SimpleEvent(self, f) for _cnt in range(events_num)]

        self.data: Optional[cw.data.CWPyElement] = None

    def get_data(self) -> "cw.data.CWPyElement":
        if self.data is None:
            self.data = cw.data.make_element("Package")
            prop = cw.data.make_element("Property")
            e = cw.data.make_element("Id", str(self.id))
            prop.append(e)
            e = cw.data.make_element("Name", self.name)
            prop.append(e)
            self.data.append(prop)
            e = cw.data.make_element("Events")
            for event in self.events:
                e.append(event.get_data())
            self.data.append(e)
        return self.data

    @staticmethod
    def unconv(f: "cw.binary.cwfile.CWFileWriter", data: "cw.data.CWPyElement") -> None:
        from . import event

        name = ""
        resid = 0
        events: Optional[cw.data.CWPyElement] = None

        for e in data:
            if e.tag == "Property":
                for prop in e:
                    if prop.tag == "Id":
                        resid = int(prop.text)
                    elif prop.tag == "Name":
                        name = prop.text
            elif e.tag == "Events":
                events = e

        f.write_dword(0)  # 不明
        f.write_string(name)
        f.write_dword(resid)
        if events is None:
            f.write_dword(0)
        else:
            f.write_dword(len(events))
            for evt in events:
                event.SimpleEvent.unconv(f, evt)


def main() -> None:
    pass


if __name__ == "__main__":
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import pygame

import cw

from typing import Iterable, Optional, Tuple


def animate_sprite(sprite: "cw.sprite.base.CWPySprite", anitype: str, clearevent: bool = True, background: bool = False,
                   statusbutton: bool = False, battlespeed: bool = False) -> bool:
    if threading.currentThread() != cw.cwpy:
        raise Exception()

    if not hasattr(sprite, "update_" + anitype):
        print("Not found " + anitype + " animation.")
        print(sprite)
        return False

    if clearevent:
        lock_menucards = cw.cwpy.lock_menucards
        cw.cwpy.lock_menucards = True

    sprite.old_status = sprite.status
    sprite.status = anitype
    sprite.skipped = False
    sprite.start_animation = pygame.time.get_ticks()
    stw = cw.sprite.base.StopTheWorld(sprite.start_animation, 0)

    if battlespeed and isinstance(sprite, cw.sprite.card.CWPyCard):
        sprite.battlespeed = True

    skip = _get_skipstatus(clearevent)
    draw = False

    while cw.cwpy.is_running() and not cw.cwpy.cut_animation and sprite.status == anitype:
        stw.is_waiting()
        if cw.cwpy.setting.stop_the_world_with_iconized and cw.cwpy.frame.is_iconized:
            cw.cwpy.input(inputonly=clearevent)
            cw.cwpy.get_eventhandler().run()
            cw.cwpy.wait_frame(1)
            continue
        sprite.start_animation = stw.start_ticks

        clip = pygame.rect.Rect(sprite.rect)
        sprite.skipped |= skip
        sprite.update()
        clip.union_ip(sprite.rect)
        if sprite.status != anitype:
            cw.cwpy.add_lazydraw(clip=clip)
            break

        skip |= _get_skipstatus(clearevent)

        if skip:
            cw.cwpy.add_lazydraw(clip)
        else:
            clip2 = _inputevent(clip, clearevent, statusbutton)
            assert clip2
            clip = clip2
            cw.cwpy.add_lazydraw(clip=clip)
            cw.cwpy.wait_frame(1, canskip=draw)

        draw = True

    sprite.skipped = False

    if battlespeed and isinstance(sprite, cw.sprite.card.CWPyCard):
        sprite.battlespeed = False

    if statusbutton:
        cw.cwpy.clear_inputevents()
    else:
        cw.cwpy.update_mousepos()
        cw.cwpy.input(inputonly=clearevent)
        cw.cwpy.get_eventhandler().run()

    if clearevent and cw.cwpy.lock_menucards:
        cw.cwpy.lock_menucards = lock_menucards

    if draw:
        cw.cwpy.lazy_draw()

    return skip


def animate_sprites(sprites: Iterable["cw.sprite.base.CWPySprite"], anitype: str, clearevent: bool = True,
                    battlespeed: bool = False) -> None:
    """spritesに含まれる全てのスプライトをanitypeの
    アニメーションで動かす。
    """
    sprandanimes = [(s, anitype) for s in sprites]
    animate_sprites2(sprandanimes, clearevent, battlespeed)


def animate_sprites2(sprandanimes: Iterable[Tuple["cw.sprite.base.CWPySprite", str]], clearevent: bool = True,
                     battlespeed: bool = False) -> bool:
    """スプライト毎にアニメーション内容を指定する。
    """
    if threading.currentThread() != cw.cwpy:
        raise Exception()

    for spr, anitype in sprandanimes:
        if not hasattr(spr, "update_" + anitype):
            print("Not found " + anitype + " animation.")
            print(sprandanimes)
            return False

    if clearevent:
        lock_menucards = cw.cwpy.lock_menucards
        cw.cwpy.lock_menucards = True

    tick = pygame.time.get_ticks()
    stw = cw.sprite.base.StopTheWorld(tick, 0)
    for sprite, anitype in sprandanimes:
        sprite.old_status = sprite.status
        sprite.status = anitype
        sprite.skipped = False
        sprite.start_animation = tick
        if battlespeed and isinstance(sprite, cw.sprite.card.CWPyCard):
            sprite.battlespeed = True

    animating = True
    skip = _get_skipstatus(clearevent)
    draw = False

    while cw.cwpy.is_running() and not cw.cwpy.cut_animation and animating:
        stw.is_waiting()
        if cw.cwpy.setting.stop_the_world_with_iconized and cw.cwpy.frame.is_iconized:
            cw.cwpy.input(inputonly=clearevent)
            cw.cwpy.get_eventhandler().run()
            cw.cwpy.wait_frame(1)
            continue

        clip = None
        upd = False
        for sprite, anitype in sprandanimes:
            sprite.start_animation = stw.start_ticks
            if sprite.status != anitype:
                continue
            if clip:
                clip.union_ip(sprite.rect)
            else:
                clip = pygame.rect.Rect(sprite.rect)
            sprite.skipped |= skip
            sprite.update()
            if sprite.status == anitype:
                upd = True
            clip.union_ip(sprite.rect)
        if not upd:
            if clip:
                cw.cwpy.add_lazydraw(clip=clip)
            break

        skip |= _get_skipstatus(clearevent)

        if skip:
            if clip:
                cw.cwpy.add_lazydraw(clip)
        else:
            clip = _inputevent(clip, clearevent, False)
            if clip:
                cw.cwpy.add_lazydraw(clip=clip)
            cw.cwpy.wait_frame(1, canskip=draw)

        draw = True
        animating = False

        for sprite, anitype in sprandanimes:
            if sprite.status == anitype:
                animating = True
                break

    for sprite, anitype in sprandanimes:
        sprite.skipped = False
        if battlespeed and isinstance(sprite, cw.sprite.card.CWPyCard):
            sprite.battlespeed = False

    cw.cwpy.update_mousepos()
    cw.cwpy.input(inputonly=clearevent)
    cw.cwpy.get_eventhandler().run()

    if clearevent and cw.cwpy.lock_menucards:
        cw.cwpy.lock_menucards = lock_menucards

    if draw:
        cw.cwpy.lazy_draw()

    return skip


def _inputevent(clip: Optional[pygame.rect.Rect], clearevent: bool, statusbutton: bool) -> Optional[pygame.rect.Rect]:
    if statusbutton:
        cw.cwpy.clear_inputevents()
    else:
        cw.cwpy.update_mousepos()
        cw.cwpy.update_groups((cw.cwpy.sbargrp,))
        cw.cwpy.input(inputonly=clearevent)
        cw.cwpy.get_eventhandler().run()
    return clip


def start_animation(sprite: "cw.sprite.base.CWPySprite", anitype: str) -> None:
    """spriteのアニメーションを開始する。
    アニメーションは他のイベント進行と平行して実行される。
    animate_sprite()と違ってフレームが進まなかったり飛んだりする
    場合があるので、update_<anitype>()の実装は、そうした場合でも
    正しく動くように行わなければならない。
    """
    if threading.currentThread() != cw.cwpy:
        raise Exception()

    if not hasattr(sprite, "update_" + anitype):
        print("Not found " + anitype + " animation.")
        print(sprite)
        return

    if sprite.anitype == "":
        sprite.old_status = sprite.status
    sprite.status = anitype
    sprite.anitype = anitype
    sprite.start_animation = pygame.time.get_ticks()
    sprite.frame = 0

    cw.cwpy.animations.add(sprite)


def _get_skipstatus(clearevent: bool) -> bool:
    if not clearevent and (cw.cwpy.keyevent.is_keyin(pygame.K_RETURN) or cw.cwpy.keyevent.is_mousein()):
        cw.cwpy.cut_animation = True
        return True

    if not clearevent or not cw.cwpy.setting.can_skipanimation:
        return False

    breakflag = False
    events = pygame.event.get((pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP, pygame.KEYDOWN, pygame.KEYUP))
    for e in events:
        if e.type in (pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP, pygame.KEYDOWN, pygame.KEYUP):
            if e.type in (pygame.MOUSEBUTTONUP, pygame.MOUSEBUTTONDOWN) and hasattr(e, "button"):
                if not cw.cwpy.setting.can_skipwait_with_wheel and e.button in (4, 5):
                    # ホイールによる空白時間スキップ無効の設定
                    cw.thread.post_pygameevent(e)
                    continue
            breakflag = True
        cw.thread.post_pygameevent(e)

    if not breakflag:
        breakflag = bool(cw.cwpy.event.get_event() and cw.cwpy.event.is_stoped())

    if breakflag or cw.cwpy.keyevent.is_keyin(pygame.K_RETURN) or cw.cwpy.keyevent.is_mousein():
        return True

    return False


def main() -> None:
    pass


if __name__ == "__main__":
    main()

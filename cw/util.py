#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import stat
import shutil
import re
import time
import struct
import zipfile
import lhafile
import operator
import threading
import hashlib
import subprocess
import io
import traceback
import datetime
import ctypes
import ctypes.util
import array
import unicodedata
import functools
import webbrowser
import math
import itertools
import warnings
import types
from functools import reduce

import wx
import wx.adv
import wx.lib.agw.aui.tabart
import wx.lib.mixins.listctrl
import wx.richtext
import wx.grid
import pygame
import pygame.image
import pygame.surface
from pygame import KEYDOWN, KEYUP, MOUSEBUTTONDOWN, MOUSEBUTTONUP, USEREVENT

import grapheme
import regex

import cw

import typing
from typing import BinaryIO, Callable, Dict, Iterable, List, Literal, Optional, Sequence, Set, TextIO, Tuple, Type,\
    TypeVar, Union
import abc


if sys.platform == "win32":
    import win32api
    import win32con
    import pythoncom
    import win32com.shell.shell as win32shell
    import win32com.shell.shellcon
    import ctypes.wintypes
    import msvcrt
    # NuitkaやPyInstallerに必要なモジュールを知らせる
    # import wx._html
    # import wx._xml
    # 以下はcx_Freezeでも必要
    import win32timezone


# ------------------------------------------------------------------------------
# 汎用クラス
# ------------------------------------------------------------------------------

class MusicInterface(object):
    def __init__(self, channel: int, mastervolume: int) -> None:
        self.channel = channel
        self.path = ""
        self.fpath = ""
        self.subvolume = 100
        self.loopcount = 0
        self.movie_scr: Optional[pygame.surface.Surface] = None
        self.mastervolume = mastervolume
        self._winmm = False
        self._bass = False
        self._movie: Optional[pygame.movie.Movie] = None
        self.inusecard = False

    def update_scale(self) -> None:
        if self._movie:
            self.movie_scr = pygame.surface.Surface(cw.s(self._movie.get_size())).convert()
            rect = cw.s(pygame.rect.Rect((0, 0), self._movie.get_size()))
            self._movie.set_display(self.movie_scr, rect)

    def play(self, path: str, updatepredata: bool = True, restart: bool = False, inusecard: bool = False,
             subvolume: int = 100, loopcount: int = 0, fade: int = 0, fullpath: str = "") -> None:
        if not updatepredata:
            # サウンドフォントやスキンの変更等で鳴らし直す場合
            subvolume = self.subvolume
            loopcount = self.loopcount
        self._play(path, updatepredata, restart, inusecard, subvolume, loopcount, fade, fullpath=fullpath)

    def _play(self, path: str, updatepredata: bool = True, restart: bool = False, inusecard: bool = False,
              subvolume: int = 100, loopcount: int = 0, fade: int = 0, fullpath: str = "") -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self._play, path, updatepredata, restart, inusecard, subvolume, loopcount, fade,
                              fullpath)
            return

        assert threading.currentThread() == cw.cwpy

        cw.cwpy.stop_the_world_with_iconized()

        if cw.cwpy.ydata and cw.cwpy.is_playingscenario():
            cw.cwpy.ydata.changed()
        fpath = self.get_path(path, inusecard)
        self.path = path
        if not (cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init()) and \
                not cw.bassplayer.is_alivablewithpath(fpath):
            return

        if cw.cwpy.rsrc:
            fpath = cw.cwpy.rsrc.get_filepath(fpath)

        if not fpath:
            fpath = fullpath

        if cw.fsync.is_waiting(fpath):
            cw.fsync.sync()
        if not os.path.isfile(fpath):
            self.stop_impl(fade, stopfadeout=False, updatepredata=False)
        else:
            assert threading.currentThread() == cw.cwpy

            if restart or self.fpath != fpath:
                self.stop_impl(fade, stopfadeout=False, updatepredata=False)
                self.set_volume()
                self._winmm = False
                self._bass = False
                bgmtype = load_bgm(fpath)
                if bgmtype != -1:
                    filesize = 0
                    if os.path.isfile(fpath):
                        try:
                            filesize = os.path.getsize(fpath)
                        except Exception:
                            cw.util.print_ex()

                    if bgmtype == 2:
                        volume = self._get_volumevalue(fpath) * subvolume / 100.0
                        try:
                            cw.bassplayer.play_bgm(fpath, volume, loopcount=loopcount, channel=self.channel, fade=fade)
                            self._bass = True
                        except Exception:
                            cw.util.print_ex()
                    elif bgmtype == 1:
                        if sys.platform == "win32":
                            name = "cwbgm_" + str(self.channel)
                            mciSendStringW = ctypes.windll.winmm.mciSendStringW
                            mciSendStringW('open "%s" alias %s' % (fpath, name), 0, 0, 0)
                            volume = cw.cwpy.setting.vol_bgm_midi if is_midi(fpath) else cw.cwpy.setting.vol_bgm
                            volume = int(volume * 1000)
                            volume = volume * subvolume // 100
                            mciSendStringW("setaudio %s volume to %s" % (name, volume), 0, 0, 0)
                            mciSendStringW("play %s" % (name), 0, 0, 0)
                            self._winmm = True
                        elif cw.util.splitext(fpath)[1].lower() in (".mpg", ".mpeg"):
                            try:
                                if cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init():
                                    pygame.mixer.quit()
                                encoding = cw.filesystem_encoding
                                self._movie = pygame.movie.Movie(fpath.encode(encoding))
                                volume = self._get_volumevalue(fpath) * subvolume / 100.0
                                self._movie.set_volume(volume)
                                self.movie_scr = pygame.surface.Surface(cw.s(self._movie.get_size())).convert()
                                rect = cw.s(pygame.rect.Rect((0, 0), self._movie.get_size()))
                                self._movie.set_display(self.movie_scr, rect)
                                self._movie.play()
                            except Exception:
                                cw.util.print_ex()
                    elif cw.cwpy.setting.sdlmixer_enabled:
                        if filesize == 57 and cw.util.get_md5(fpath) == "d11be4c76fc63a6ba299c2f3bd3880b0":
                            # BUG: reset.mid
                            # 繰り返し流すとハングアップ pygame 1.9.1
                            if pygame.mixer.get_init():
                                pygame.mixer.music.play(0)
                        elif filesize == 737 and cw.util.get_md5(fpath) == "41b0a6aaa8ffefa9ce6742e80e393075":
                            # BUG: DefReset.mid
                            # 繰り返し流すとシステムが不安定になる pygame 1.9.1
                            if pygame.mixer.get_init():
                                pygame.mixer.music.play(0)
                        elif cw.util.splitext(fpath)[1].lower() == ".mp3":
                            # 互換動作: 1.28以前はMP3がループ再生されない
                            if pygame.mixer.get_init():
                                volume = self._get_volumevalue(fpath) * subvolume / 100.0
                                pygame.mixer.music.set_volume(volume)
                                if cw.cwpy.sct.lessthan("1.28", cw.cwpy.sdata.get_versionhint()):
                                    pygame.mixer.music.play(0)
                                else:
                                    pygame.mixer.music.play(loopcount - 1)
                        elif pygame.mixer.get_init():
                            pygame.mixer.music.play(-1)
            else:
                if self.subvolume != subvolume:
                    self.subvolume = subvolume
                    self.set_volume(fade=fade)
                else:
                    self.set_volume()
                if self._bass:
                    # ループ回数は常に設定する
                    cw.bassplayer.set_bgmloopcount(loopcount, channel=self.channel)

            self.fpath = fpath
            self.subvolume = subvolume
            self.loopcount = loopcount
            self.path = path

        if updatepredata and cw.cwpy.sdata and cw.cwpy.sdata.pre_battleareadata and \
                cw.cwpy.sdata.pre_battleareadata[1][3] == self.channel:
            areaid, bgmpath, battlebgmpath = cw.cwpy.sdata.pre_battleareadata
            bgmpath = (path, subvolume, loopcount, self.channel)
            cw.cwpy.sdata.pre_battleareadata = (areaid, bgmpath, battlebgmpath)

    def stop(self, fade: int = 0) -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self.stop, fade)
            return
        self.stop_impl(fade=fade, stopfadeout=True, updatepredata=True)

    def stop_impl(self, fade: int, stopfadeout: bool, updatepredata: bool = True) -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self.stop_impl, fade, stopfadeout)
            return

        assert threading.currentThread() == cw.cwpy

        if cw.bassplayer.is_alivablewithpath(self.fpath):
            # フェードアウト中のBGMも停止する必要があるため、
            # self._bass == Falseの時も停止処理を行う
            cw.bassplayer.stop_bgm(channel=self.channel, fade=fade, stopfadeout=stopfadeout)
            self._bass = False

        if sys.platform == "win32" and self._winmm:
            name = "cwbgm_" + str(self.channel)
            mciSendStringW = ctypes.windll.winmm.mciSendStringW
            mciSendStringW("stop %s" % (name), 0, 0, 0)
            mciSendStringW("close %s" % (name), 0, 0, 0)
            self._winmm = False
        elif self._movie:
            assert self.movie_scr
            self._movie.stop()
            self._movie = None
            self.movie_scr = None
            if cw.cwpy.setting.sdlmixer_enabled:
                cw.util.sdlmixer_init()
        elif cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init():
            if 0 < fade:
                pygame.mixer.music.fadeout(fade)
            else:
                pygame.mixer.music.stop()
        remove_soundtempfile("Bgm")
        self.fpath = ""
        self.path = ""
        # pygame.mixer.musicで読み込んだ音楽ファイルを解放する
        if cw.cwpy.rsrc:
            path = "DefReset"
            path = find_resource(join_paths(cw.cwpy.setting.skindir, "Bgm", path), cw.cwpy.rsrc.ext_bgm)
            load_bgm(path)

        if updatepredata and cw.cwpy.sdata and cw.cwpy.sdata.pre_battleareadata and \
                cw.cwpy.sdata.pre_battleareadata[1][3] == self.channel:
            areaid, bgmpath, battlebgmpath = cw.cwpy.sdata.pre_battleareadata
            bgmpath = ("", 100, 0, self.channel)
            cw.cwpy.sdata.pre_battleareadata = (areaid, bgmpath, battlebgmpath)

    def _get_volumevalue(self, fpath: str) -> float:
        if not cw.cwpy.setting.play_bgm:
            return 0

        if is_midi(fpath):
            volume = cw.cwpy.setting.vol_bgm_midi
        else:
            volume = cw.cwpy.setting.vol_bgm

        return volume * self.mastervolume / 100.0

    def set_volume(self, volume: Optional[float] = None, fade: int = 0) -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self.set_volume, volume)
            return

        if volume is None:
            volume = self._get_volumevalue(self.fpath)
        volume = volume * self.subvolume / 100.0

        assert threading.currentThread() == cw.cwpy
        if self._bass:
            cw.bassplayer.set_bgmvolume(volume, channel=self.channel, fade=fade)
        elif self._movie:
            self._movie.set_volume(volume)
        elif cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init():
            pygame.mixer.music.set_volume(volume)

    def set_mastervolume(self, volume: int) -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self.set_mastervolume, volume)
            return

        self.mastervolume = volume
        self.set_volume()

    def get_path(self, path: str, inusecard: bool = False) -> str:
        if os.path.isabs(path):
            return path
        elif inusecard:
            path = cw.util.join_yadodir(path)
            self.inusecard = True
        else:
            inusepath = cw.util.get_inusecardmaterialpath(path, cw.M_MSC)
            if os.path.isfile(inusepath):
                path = inusepath
                self.inusecard = True
            else:
                path = get_materialpath(path, cw.M_MSC)
                self.inusecard = False

        return path


class SoundInterface(object):
    def __init__(self, sound: Optional[Union[str, pygame.mixer.Sound]] = None, path: str = "",
                 is_midi: bool = False) -> None:
        self._sound = sound
        self._path = path
        self.subvolume = 100
        self.channel = -1
        self._type = -1
        self.mastervolume = 0
        self._is_midi = is_midi

    def copy(self) -> "SoundInterface":
        sound = SoundInterface()
        sound._sound = self._sound
        sound._path = self._path
        sound.subvolume = self.subvolume
        sound.channel = self.channel
        sound._type = self._type
        sound.mastervolume = self.mastervolume
        sound._is_midi = self._is_midi
        return sound

    def get_path(self) -> str:
        return self._path

    def _play_before(self, from_scenario: bool, channel: int, fade: int) -> str:
        if from_scenario:
            sound = cw.cwpy.lastsound_scenario[channel]
            if sound:
                sound.stop_impl(from_scenario, fade=fade, stopfadeout=False)
                cw.cwpy.lastsound_scenario[channel] = None
            cw.cwpy.lastsound_scenario[channel] = self
            return "Sound"
        else:
            if cw.cwpy.lastsound_system:
                cw.cwpy.lastsound_system.stop_impl(from_scenario, fade=fade, stopfadeout=False)
                cw.cwpy.lastsound_system = None
            cw.cwpy.lastsound_system = self
            return "SystemSound"

    def play(self, from_scenario: bool = False, subvolume: int = 100, loopcount: int = 1, channel: int = 0,
             fade: int = 0) -> None:
        cw.cwpy.stop_the_world_with_iconized()

        self._type = -1
        self.mastervolume = cw.cwpy.music[0].mastervolume
        if self._sound and 0 <= channel and channel < cw.bassplayer.MAX_SOUND_CHANNELS:
            self.channel = channel
            self.subvolume = subvolume

            if cw.cwpy.setting.play_sound:
                volume = cw.cwpy.setting.vol_sound_midi if self._is_midi else cw.cwpy.setting.vol_sound
                volume = (volume * cw.cwpy.music[0].mastervolume) / 100.0 * subvolume / 100.0
            else:
                volume = 0

            if cw.bassplayer.is_alivablewithpath(self._path):
                if threading.currentThread() != cw.cwpy:
                    cw.cwpy.exec_func(self.play, from_scenario, subvolume, loopcount, channel, fade)
                    return
                assert threading.currentThread() == cw.cwpy
                tempbasedir = self._play_before(from_scenario, channel, fade)
                assert isinstance(self._sound, str)
                try:
                    path = get_soundfilepath(tempbasedir, self._sound)
                    cw.bassplayer.play_sound(path, volume, from_scenario, loopcount=loopcount, channel=channel,
                                             fade=fade)
                    self._type = 0
                except Exception:
                    cw.util.print_ex()
            elif sys.platform == "win32" and isinstance(self._sound, str):
                if threading.currentThread() == cw.cwpy:
                    cw.cwpy.frame.exec_func(self.play, from_scenario, subvolume, loopcount, channel, fade)
                    return
                assert threading.currentThread() != cw.cwpy
                tempbasedir = self._play_before(from_scenario, channel, fade)
                if from_scenario:
                    name = "cwsnd1_" + str(channel)
                else:
                    name = "cwsnd2"

                mciSendStringW = ctypes.windll.winmm.mciSendStringW
                path = get_soundfilepath(tempbasedir, self._sound)
                mciSendStringW('open "%s" alias %s' % (path, name), 0, 0, 0)
                volume = int(volume * 1000)
                mciSendStringW("setaudio %s volume to %s" % (name, volume), 0, 0, 0)
                mciSendStringW("play %s" % (name), 0, 0, 0)
                self._type = 1
            else:
                if threading.currentThread() != cw.cwpy:
                    cw.cwpy.exec_func(self.play, from_scenario, subvolume, loopcount, channel, fade)
                    return
                assert threading.currentThread() == cw.cwpy
                tempbasedir = self._play_before(from_scenario, channel, fade)
                if cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init():
                    if from_scenario:
                        chan = pygame.mixer.Channel(channel + 1)
                    else:
                        chan = pygame.mixer.Channel(0)

                    assert isinstance(self._sound, pygame.mixer.Sound)
                    self._sound.set_volume(volume)
                    chan.play(self._sound, loopcount - 1, fade_ms=fade)
                    self._type = 2

    def stop(self, from_scenario: bool, fade: int = 0) -> None:
        self.stop_impl(from_scenario, fade=fade, stopfadeout=True)

    def stop_impl(self, from_scenario: bool, fade: int, stopfadeout: bool) -> None:
        self.mastervolume = 0
        if self._type != -1 and self._sound and 0 <= self.channel and self.channel < cw.bassplayer.MAX_SOUND_CHANNELS:
            if from_scenario:
                tempbasedir = "Sound"
            else:
                tempbasedir = "SystemSound"

            if self._type == 0:
                if threading.currentThread() != cw.cwpy:
                    cw.cwpy.exec_func(self.stop_impl, from_scenario, fade, stopfadeout)
                    return
                assert threading.currentThread() == cw.cwpy
                try:
                    cw.bassplayer.stop_sound(from_scenario, channel=self.channel, fade=fade, stopfadeout=stopfadeout)
                    remove_soundtempfile(tempbasedir)
                except Exception:
                    cw.util.print_ex()
            elif self._type == 1:
                if sys.platform == "win32":
                    if threading.currentThread() == cw.cwpy:
                        cw.cwpy.frame.exec_func(self.stop_impl, from_scenario, fade, stopfadeout)
                        return
                    assert threading.currentThread() != cw.cwpy
                    if from_scenario:
                        name = "cwsnd1_" + str(self.channel)
                    else:
                        name = "cwsnd2"

                    mciSendStringW = ctypes.windll.winmm.mciSendStringW
                    mciSendStringW("stop %s" % (name), 0, 0, 0)
                    mciSendStringW("close %s" % (name), 0, 0, 0)
                    remove_soundtempfile(tempbasedir)
            else:
                if threading.currentThread() != cw.cwpy:
                    cw.cwpy.exec_func(self.stop_impl, from_scenario, fade, stopfadeout)
                    return
                assert threading.currentThread() == cw.cwpy
                if cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init():
                    if from_scenario:
                        chan = pygame.mixer.Channel(self.channel + 1)
                    else:
                        chan = pygame.mixer.Channel(0)

                    if 0 < fade:
                        chan.fadeout(fade)
                    else:
                        chan.stop()

    def _get_volumevalue(self, fpath: str) -> float:
        if not cw.cwpy.setting.play_sound:
            return 0

        if is_midi(fpath):
            volume = cw.cwpy.setting.vol_sound_midi
        else:
            volume = cw.cwpy.setting.vol_sound

        return volume * self.mastervolume / 100.0

    def set_mastervolume(self, from_scenario: bool, volume: int) -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self.set_mastervolume, from_scenario, volume)
            return

        self.mastervolume = volume
        self.set_volume(from_scenario)

    def set_volume(self, from_scenario: bool, volume: Optional[int] = None) -> None:
        if threading.currentThread() != cw.cwpy:
            cw.cwpy.exec_func(self.set_volume, from_scenario, volume)
            return
        if self._type == -1:
            return

        if volume is None:
            fvolume = self._get_volumevalue(self._path)
        else:
            fvolume = float(volume)
        fvolume = fvolume * self.subvolume / 100.0

        assert threading.currentThread() == cw.cwpy
        if self._type == 0:
            cw.bassplayer.set_soundvolume(fvolume, from_scenario, channel=self.channel, fade=0)
        elif self._type == 1:
            if sys.platform == "win32":
                volume = int(fvolume * 1000)
                mciSendStringW = ctypes.windll.winmm.mciSendStringW
                if from_scenario:
                    name = "cwsnd1_" + str(self.channel)
                else:
                    name = "cwsnd2"
                mciSendStringW("setaudio %s volume to %s" % (name, fvolume), 0, 0, 0)
        elif self._type == 2:
            assert isinstance(self._sound, pygame.mixer.Sound)
            self._sound.set_volume(fvolume)


# ------------------------------------------------------------------------------
# 汎用関数
# ------------------------------------------------------------------------------

def init(size_noscale: Optional[Tuple[int, int]] = None, title: str = "", fullscreen: bool = False,
         soundfonts: Optional[List[Tuple[str, bool, int]]] = None, fullscreensize: Tuple[int, int] = (0, 0),
         sdlmixer_enabled: bool = False) -> Tuple[pygame.surface.Surface, pygame.surface.Surface,
                                                  Optional[pygame.surface.Surface], pygame.time.Clock]:
    """pygame初期化。"""
    if sys.platform == "win32":
        # FIXME: SDLがWindowsの言語設定に勝手にUSキーボード設定を追加してしまうので
        #        キーボードレイアウトが増えていた場合に限り除去
        #        おそらくSDL2では発生しないので、更新した時には以下のコードを取り除けるはず
        _ = win32api.GetKeyboardLayout(0)
        hkls = set()
        for hkl in win32api.GetKeyboardLayoutList():
            hkls.add(hkl)

        pygame.display.init()

        for hkl in win32api.GetKeyboardLayoutList():
            if hkl not in hkls:
                p = ctypes.c_void_p(hkl)
                ctypes.windll.user32.UnloadKeyboardLayout(p)
    else:
        pygame.display.init()

    pygame.font.init()
    # pygame.joystick.init()
    flags = 0
    size = cw.s(size_noscale)
    if fullscreen:
        scr_fullscreen: Optional[pygame.surface.Surface] = pygame.display.set_mode(fullscreensize, flags)
        scr = pygame.surface.Surface(size).convert()
        scr_draw = scr
    else:
        scr_fullscreen = None
        scr = pygame.display.set_mode(cw.wins(size_noscale), flags)
        if cw.UP_WIN == cw.UP_SCR:
            scr_draw = scr
        else:
            scr_draw = pygame.surface.Surface(size).convert()
    clock = pygame.time.Clock()

    if title:
        pygame.display.set_caption(title)

    pygame.event.set_blocked(None)
    pygame.event.set_allowed([KEYDOWN, KEYUP, MOUSEBUTTONDOWN, MOUSEBUTTONUP, USEREVENT, cw.FORCE_USEREVENT])

    # BASS Audioを初期化(使用できない事もある)
    if soundfonts is None:
        soundfonts = [(cw.DEFAULT_SOUNDFONT, True, 100)]
    soundfonts2 = [(sfont[0], sfont[2] / 100.0) for sfont in soundfonts if sfont[1]]
    if not cw.bassplayer.init_bass(soundfonts2):
        if sdlmixer_enabled:
            # BASS Audioが使用できない場合に限りpygame.mixerを初期化
            # (BASSとpygame.mixerを同時に初期化した場合、
            # 環境によっては音が出なくなるなどの不具合が出る)
            sdlmixer_init()

    return scr, scr_draw, scr_fullscreen, clock


def sdlmixer_init() -> None:
    try:
        pygame.mixer.init(44100, -16, 2, 1024)
        pygame.mixer.set_num_channels(2)
    except Exception:
        cw.util.print_ex(file=sys.stderr)


def convert_maskpos(maskpos: Union[Tuple[int, int], str], width: int, height: int) -> Tuple[int, int]:
    """maskposが座標ではなくキーワード"center"または"right"
    であった場合、それぞれ画像の中央、右上の座標を返す。
    """
    if isinstance(maskpos, str):
        if maskpos == "center":
            maskpos = (width // 2, height // 2)
        elif maskpos == "right":
            maskpos = (width - 1, 0)
        else:
            raise Exception("Invalid maskpos: %s" % (maskpos))
    return maskpos


def get_scaledimagepaths(path: str, can_loaded_scaledimage: bool) -> List[Tuple[str, int]]:
    """(スケーリングされたファイル名, スケール)のlistを返す。
    listには1倍スケールを示す(path, 1)が必ず含まれる。
    """
    seq = [(path, 1)]
    if can_loaded_scaledimage:
        spext = cw.util.splitext(path)
        for scale in cw.SCALE_LIST:
            fname = "%s.x%d%s" % (spext[0], scale, spext[1])
            seq.append((fname, scale))
    return seq


def copy_scaledimagepaths(frompath: str, topath: str, can_loaded_scaledimage: bool) -> None:
    """frompathをtopathへコピーする。
    その後、ファイル名に".xN"をつけたイメージを探し、
    実際に存在するファイルであればコピーする。
    """
    shutil.copy2(frompath, topath)
    fromspext = cw.util.splitext(frompath)
    if can_loaded_scaledimage and fromspext[1].lower() in cw.EXTS_IMG:
        tospext = cw.util.splitext(topath)
        for scale in cw.SCALE_LIST:
            fname = "%s.x%d%s" % (fromspext[0], scale, fromspext[1])
            fname = cw.cwpy.rsrc.get_filepath(fname)
            if os.path.isfile(fname):
                fname2 = "%s.x%d%s" % (tospext[0], scale, tospext[1])
                shutil.copy2(fname, fname2)


def remove_scaledimagepaths(fpath: str, can_loaded_scaledimage: bool, trashbox: bool = False) -> None:
    """fpathと共にfpathがスケーリングされたイメージファイルを全て削除する。
    """
    if not os.path.isfile(fpath):
        return
    remove(fpath, trashbox=trashbox)
    fpathext = cw.util.splitext(fpath)
    if can_loaded_scaledimage and fpathext[1].lower() in cw.EXTS_IMG:
        for scale in cw.SCALE_LIST:
            fname = "%s.x%d%s" % (fpathext[0], scale, fpathext[1])
            fname = cw.cwpy.rsrc.get_filepath(fname)
            if fname and os.path.isfile(fname):
                remove(fname, trashbox=trashbox)


def find_scaledimagepath(path: str, up_scr: float, can_loaded_scaledimage: bool, noscale: bool) -> Tuple[str, int]:
    """ファイル名に".xN"をつけたイメージを探して(ファイル名, スケール値)を返す。
    例えば"file.bmp"に対する"file.x2.bmp"を探す。
    """
    scale = 1

    if cw.binary.image.path_is_code(path):
        return path, scale

    path = cw.util.join_paths(path)
    if not noscale and (can_loaded_scaledimage or
                        path.startswith(cw.util.join_paths(cw.tempdir, "ScenarioLog/TempFile") + "/") or
                        path.startswith(cw.util.join_paths(cw.cwpy.skindir, "Table") + "/")):
        scale = int(math.pow(2, int(math.log(up_scr, 2))))
        spext = cw.util.splitext(path)
        while 2 <= scale:
            fname = "%s.x%d%s" % (spext[0], scale, spext[1])
            fname = cw.cwpy.rsrc.get_filepath(fname)
            if os.path.isfile(fname):
                path = fname
                break
            scale //= 2
    return path, scale


def find_noscalepath(path: str) -> str:
    """pathが"file.x2.bmp"のようなスケール付きイメージのものであれば
    ".xN"の部分を取り除いて返す。
    ただし取り除いた後のファイルが実在しない場合はそのまま返す。
    """
    scales = "|".join([str(s) for s in cw.SCALE_LIST])
    exts = "|".join([s.replace(".", "\\.") for s in cw.EXTS_IMG])
    result = re.match("\\A(.+)\\.x(%s)(%s)\\Z" % (scales, exts), path, re.IGNORECASE)
    if result:
        fpath = result.group(1) + result.group(3)
        if os.path.isfile(fpath):
            path = fpath
    return path


def load_image(path: str, mask: bool = False, maskpos: Tuple[int, int] = (0, 0), f: Optional[BinaryIO] = None,
               retry: bool = True, isback: bool = False, can_loaded_scaledimage: bool = True,
               noscale: bool = False, up_scr: Optional[Union[int, float]] = None,
               use_excache: bool = False) -> pygame.surface.Surface:
    """pygame.surface.Surface(読み込めなかった場合はNone)を返す。
    path: 画像ファイルのパス。
    mask: True時、(0,0)のカラーを透過色に設定する。透過画像の場合は無視される。
    """
    # assert threading.currentThread() == cw.cwpy
    if cw.cwpy.rsrc:
        path = cw.cwpy.rsrc.get_filepath(path)

    if up_scr is None:
        up_scr = cw.UP_SCR

    if use_excache and path:
        # JPDC撮影等で更新されたイメージは正式に差し替えが完了するのがイベント終了後となる
        # それまではキャッシュを使用する
        npath = get_keypath(get_symlinktarget(path))
        if cw.cwpy.sdata and npath in cw.cwpy.sdata.ex_cache:
            caches = cw.cwpy.sdata.ex_cache[npath]
            data = b""
            up_scr2 = 1
            for i, scale in enumerate(itertools.chain((1,), cw.SCALE_LIST)):
                data2 = caches[i]
                if data2:
                    assert isinstance(data2, bytes)
                    data = data2
                    up_scr2 = scale
                if scale == up_scr:
                    break
            up_scr = up_scr2
            if data:
                f = io.BytesIO(data)

    if not f:
        path, up_scr = find_scaledimagepath(path, up_scr, can_loaded_scaledimage, noscale)

    bmpdepth = 0
    try:
        if f or cw.binary.image.path_is_code(path):
            if f:
                data = f.read()
            else:
                data = cw.binary.image.code_to_data(path)
            ext = get_imageext(data)
            isbmp = ext == ".bmp"
            ispng = ext == ".png"
            isgif = ext == ".gif"
            isjpg = ext == ".jpg"
            if ext == ".bmp":
                data = cw.image.patch_rle4bitmap(data)
                bmpdepth = cw.image.get_bmpdepth(data)
            f2: BinaryIO
            with io.BytesIO(data) as f2:
                image = pygame.image.load(f2)
                f2.close()
            if ext == ".bmp":
                image = cw.imageretouch.patch_alphadata(image, ext, data)
        else:
            if not os.path.isfile(path):
                return pygame.surface.Surface((0, 0)).convert()
            ext = cw.util.splitext(path)[1].lower()
            isbmp = ext == ".bmp"
            ispng = ext == ".png"
            isgif = ext == ".gif"
            isjpg = ext in (".jpg", ".jpeg")
            if ext == ".bmp":
                with open(path, "rb") as f2:
                    data = f2.read()
                    f2.close()
                bmpdepth = cw.image.get_bmpdepth(data)
                data = cw.image.patch_rle4bitmap(data)
                data, _ok = cw.image.fix_cwnext32bitbitmap(data)
                with io.BytesIO(data) as f2:
                    image = pygame.image.load(f2)
                    f2.close()
            else:
                with open(path, "rb") as f2:
                    data = f2.read()
                    f2.close()
                with io.BytesIO(data) as f2:
                    image = pygame.image.load(f2)
                    f2.close()
            if ext == ".bmp":
                image = cw.imageretouch.patch_alphadata(image, ext, data)
    except Exception:
        print_ex()
        # print u"画像が読み込めません(load_image)。リトライします", path
        if retry:
            try:
                if f:
                    f.seek(0)
                    data = f.read()
                elif cw.binary.image.path_is_code(path):
                    data = cw.binary.image.code_to_data(path)
                else:
                    if not os.path.isfile(path):
                        return pygame.surface.Surface((0, 0)).convert()
                    with open(path, "rb") as f2:
                        data = f2.read()
                        f2.close()
                if not ispng:
                    bmpdepth = cw.image.get_bmpdepth(data)
                data, _ok = cw.image.fix_cwnext32bitbitmap(data)
                data, _ok = cw.image.fix_cwnext16bitbitmap(data)
                with io.BytesIO(data) as f2:
                    r = load_image(path, mask, maskpos, f2, False, isback=isback,
                                   can_loaded_scaledimage=can_loaded_scaledimage,
                                   noscale=noscale, up_scr=up_scr, use_excache=use_excache)
                    f2.close()
                return r
            except Exception:
                print_ex()
                # print u"画像が読み込めません(リトライ後)", path
        return pygame.surface.Surface((0, 0)).convert()

    # アルファチャンネルを持った透過画像を読み込んだ場合は
    # SRCALPHA(0x00010000)のフラグがONになっている
    if (bmpdepth in (0, 32)) and (image.get_flags() & pygame.SRCALPHA):
        image = image.convert_alpha()
    else:
        imageb = image
        if image.get_bitsize() <= 8 and image.get_colorkey() and not isgif and isback:
            # BUG: 環境によってイメージセルのマスク処理を行うと透過色が壊れる issue #723
            mask = False
        if isjpg and isback and not (cw.cwpy and cw.cwpy.sdata and
                                     cw.cwpy.sct.lessthan("1.30", cw.cwpy.sdata.get_versionhint())):
            # BUG: JPEGイメージのマスク指定が無視される
            #      CardWirth 1.50
            mask = False
        # BUG: パレット使用時にconvert()を行うと同一色が全て透過されてしまう
        #      CardWirth 1.50
        if not (bmpdepth == 16 and isbmp):  # BUG: 16-bitビットマップでマスク色が有効にならない pygame 1.9.4
            image = image.convert()

        # カード画像がPNGの場合はマスクカラーを無視する(CardWirth 1.50の実装)
        if image.get_colorkey() and ispng and not isback:
            image.set_colorkey(None)

        if mask and image.get_colorkey() and isgif:
            # 256GIFでは強制的に左上マスク色が有効になる
            if imageb.get_bitsize() <= 8:
                maskpos = convert_maskpos(maskpos, image.get_width(), image.get_height())
                image.set_colorkey(image.get_at(maskpos), pygame.RLEACCEL)
        elif mask and not image.get_colorkey():  # PNGなどですでにマスクカラーが指定されている場合は除外
            maskpos = convert_maskpos(maskpos, image.get_width(), image.get_height())
            image.set_colorkey(image.get_at(maskpos), pygame.RLEACCEL)

    if not ispng and bmpdepth == 1 and mask and not isback or up_scr != 1:
        image = Depth1Surface(image, up_scr, bmpdepth)
    return image


class Depth1Surface(pygame.surface.Surface):
    def __init__(self, surface: pygame.surface.Surface, scr_scale: float, bmpdepth: int = 24) -> None:
        pygame.surface.Surface.__init__(self, surface.get_size(), surface.get_flags(), surface.get_bitsize(),
                                        surface.get_masks())
        self.blit(surface, (0, 0), special_flags=pygame.BLEND_RGBA_ADD)
        colorkey = surface.get_colorkey()
        if colorkey:
            self.set_colorkey(colorkey, pygame.RLEACCEL)
        if isinstance(surface, Depth1Surface) and hasattr(surface, "bmpdepthis1"):
            self.bmpdepthis1: bool = surface.bmpdepthis1
        else:
            self.bmpdepthis1 = bmpdepth == 1
        self.scr_scale: float = scr_scale

    def copy(self) -> pygame.surface.Surface:
        if hasattr(self, "scr_scale"):
            bmp = Depth1Surface(pygame.surface.Surface.copy(self), self.scr_scale)
            bmp.bmpdepthis1 = self.bmpdepthis1
            return bmp
        else:
            return pygame.surface.Surface.copy(self)

    def convert_alpha(self, surface: Optional[pygame.surface.Surface] = None) -> pygame.surface.Surface:
        if hasattr(self, "scr_scale"):
            if surface:
                bmp = Depth1Surface(pygame.surface.Surface.convert_alpha(self, surface), self.scr_scale, bmpdepth=32)
            else:
                bmp = Depth1Surface(pygame.surface.Surface.convert_alpha(self), self.scr_scale, bmpdepth=32)
            bmp.bmpdepthis1 = False
            return bmp
        else:
            if surface:
                return pygame.surface.Surface.convert_alpha(self, surface)
            else:
                return pygame.surface.Surface.convert_alpha(self)


def calc_imagesize(image: pygame.surface.Surface) -> int:
    """imageのデータサイズを概算する。
    結果は正確ではない。
    """
    result: int = image.get_bitsize() * image.get_width() * image.get_height() // 8
    return result


def calc_wxbmpsize(wxbmp: wx.Bitmap) -> int:
    """wx.Bitmapのデータサイズを概算する。
    結果は正確ではない。
    """
    result: int = wxbmp.GetDepth() * wxbmp.GetWidth() * wxbmp.GetHeight() // 8
    return result


def put_number(image: pygame.surface.Surface, num: int) -> pygame.surface.Surface:
    """アイコンサイズの画像imageの上に
    numの値を表示する。
    """
    image = image.convert_alpha()
    s = str(num)
    if len(s) == 1:
        font = cw.cwpy.rsrc.fonts["statusimg1"]
    elif len(s) == 2:
        font = cw.cwpy.rsrc.fonts["statusimg2"]
    else:
        font = cw.cwpy.rsrc.fonts["statusimg3"]
    h = font.get_height()
    w = (h + 1) // 2
    subimg = pygame.surface.Surface((len(s) * w, h)).convert_alpha()
    subimg.fill((0, 0, 0, 0))
    x = image.get_width() - subimg.get_width() - cw.s(1)
    y = image.get_height() - subimg.get_height()
    pos = (x, y)
    for i, c in enumerate(s):
        cimg = font.render(c, 2.0 <= cw.UP_SCR, (0, 0, 0))
        image.blit(cimg, (pos[0] + 1 + i * w, pos[1] + 1))
        image.blit(cimg, (pos[0] + 1 + i * w, pos[1] - 1))
        image.blit(cimg, (pos[0] - 1 + i * w, pos[1] + 1))
        image.blit(cimg, (pos[0] - 1 + i * w, pos[1] - 1))
        image.blit(cimg, (pos[0] + 1 + i * w, pos[1]))
        image.blit(cimg, (pos[0] - 1 + i * w, pos[1]))
        image.blit(cimg, (pos[0] + i * w, pos[1] + 1))
        image.blit(cimg, (pos[0] + i * w, pos[1] - 1))
        cimg = font.render(c, 2.0 <= cw.UP_SCR, (255, 255, 255))
        image.blit(cimg, (pos[0] + i * w, pos[1]))
    return image


def get_imageext(b: bytes) -> str:
    """dataが画像であれば対応する拡張子を返す。"""
    if 22 < len(b) and ord('B') == b[0] and ord('M') == b[1]:
        return ".bmp"
    if 25 <= len(b) and 0x89 == b[0] and ord('P') == b[1] and ord('N') == b[2] and ord('G') == b[3]:
        return ".png"
    if 10 <= len(b) and ord('G') == b[0] and ord('I') == b[1] and ord('F') == b[2]:
        return ".gif"
    if 6 <= len(b) and 0xFF == b[0] and 0xD8 == b[1]:
        return ".jpg"
    if 10 <= len(b):
        if ord('M') == b[0] and ord('M') == b[1] and 42 == b[3]:
            return ".tiff"
        elif ord('I') == b[0] and ord('I') == b[1] and 42 == b[2]:
            return ".tiff"
    return ""


def get_facepaths(sexcoupon: str, agecoupon: str,
                  adddefaults: bool = True) -> Dict[Optional[Tuple[int, str, str]], List[List["cw.image.ImageInfo"]]]:
    """sexとageに対応したFaceディレクトリ内の画像パスを辞書で返す。
    辞書の内容は、(ソートキー, ディレクトリ, ディレクトリ表示名)をキーにした
    当該ディレクトリ内のファイルパスのlistとなる。
    sexcoupon: 性別クーポン。
    agecoupon: 年代クーポン。
    adddefaults: 1件もなかった場合、Resource/Image/Cardにある
                 FATHERまたはMOTHERを使用する。
    """
    imgpaths: Dict[Optional[Tuple[int, str, str]], List[List[cw.image.ImageInfo]]] = {}

    sex = ""
    for fsex in cw.cwpy.setting.sexes:
        if sexcoupon == "＿" + fsex.name:
            sex = fsex.subname

    age = ""
    for fperiods in cw.cwpy.setting.periods:
        if agecoupon == "＿" + fperiods.name:
            age = fperiods.abbr

    dpaths: List[Tuple[int, str, str]] = []  # (実際のパス, 表示するパス)
    facedir1 = cw.util.join_paths(cw.cwpy.skindir, "Face")  # スキン付属
    facedir2 = "Data/Face"  # 全スキン共通

    for i, facedir in enumerate((facedir1, facedir2)):
        def add(weight: int, dpath1: str) -> None:
            dpath = join_paths(facedir, dpath1)
            if i == 0:
                name = cw.cwpy.setting.skinname
            else:
                name = cw.cwpy.msgs["common"]
            dpaths.append((i * 10 + weight, "<%s> %s" % (name, dpath1), dpath))

        # 性別・年代限定
        if sex and age:
            add(0, sex + "-" + age)
        # 性別限定
        if sex:
            add(1, sex)
        # 年代限定
        if age:
            add(2, "Common-" + age)
        # 汎用
        add(3, "Common")

    passed: Set[str] = set()
    _get_facepaths(facedir, imgpaths, dpaths, passed)
    if not imgpaths and adddefaults:
        seq = []
        dpath = join_paths(cw.cwpy.skindir, "Resource/Image/Card")
        for fsex in cw.cwpy.setting.sexes:
            if "＿" + fsex.name == sexcoupon:
                if fsex.father:
                    fpath = join_paths(dpath, "FATHER")
                    fpath = find_resource(fpath, cw.M_IMG)
                    seq.append(cw.dialog.create.path_to_imageinfo(fpath))
                if fsex.mother:
                    fpath = join_paths(dpath, "MOTHER")
                    fpath = find_resource(fpath, cw.M_IMG)
                    seq.append(cw.dialog.create.path_to_imageinfo(fpath))
                break
        if seq:
            imgpaths[(0, dpath, "Resource/Image/Card")] = seq
    return imgpaths


def _get_facepaths(facedir: str, imgpaths: Dict[Optional[Tuple[int, str, str]], List[List["cw.image.ImageInfo"]]],
                   dpaths: Iterable[Tuple[int, str, str]], passed: Set[str]) -> None:
    for sortkey, showdpath, dpath in dpaths:
        if not os.path.isdir(dpath):
            continue
        abspath = get_keypath(get_symlinktarget(dpath))
        if abspath in passed:
            continue
        passed.add(abspath)

        dpaths2 = []
        seq = []
        scales = "|".join([str(s) for s in cw.SCALE_LIST])
        re_xn = re.compile("\\A.+\\.x(%s)\\Z" % (scales), re.IGNORECASE)
        for fname in os.listdir(dpath):
            path1 = join_paths(dpath, fname)
            path = get_linktarget(path1)
            if os.path.isfile(path):
                spext = cw.util.splitext(path)
                ext = spext[1].lower()
                if ext in cw.EXTS_IMG and not re_xn.match(spext[0]):
                    seq.append(cw.dialog.create.path_to_imageinfo(path))
            elif os.path.isdir(path):
                showpath = join_paths(showdpath, fname)
                if sys.platform == "win32" and path1 != path and showpath.lower().endswith(".lnk"):
                    showpath = cw.util.splitext(showpath)[0]
                dpaths2.append((sortkey, showpath, path))

        if seq:
            p = join_paths(relpath(dpath, facedir))
            if p.startswith("../"):
                p = dpath
            imgpaths[(sortkey, showdpath, join_paths(p))] = seq
        if dpaths2:
            _get_facepaths(facedir, imgpaths, dpaths2, passed)


def load_bgm(path: str) -> int:
    """Pathの音楽ファイルをBGMとして読み込む。
    リピートして鳴らす場合は、cw.audio.MusicInterface参照。
    pygame.mixer.music.load()が成功した場合は0、
    winmm.dllを利用して再生する場合は1(Windowsのみ)、
    bass.dllを利用して再生する場合は2、
    失敗した場合は-1を返す。
    path: 音楽ファイルのパス。
    """
    if threading.currentThread() != cw.cwpy:
        raise Exception()

    if cw.cwpy.rsrc:
        path = cw.cwpy.rsrc.get_filepath(path)

    if not os.path.isfile(path) or (not (cw.cwpy.setting.sdlmixer_enabled and
                                         pygame.mixer.get_init()) and
                                    not cw.bassplayer.is_alivablewithpath(path)):
        return -1

    if cw.util.splitext(path)[1].lower() in (".mpg", ".mpeg"):
        return 1

    if cw.bassplayer.is_alivablewithpath(path):
        return 2

    if cw.cwpy.setting.sdlmixer_enabled:
        if not pygame.mixer.get_init():
            return -1

        path = get_soundfilepath("Bgm", path)

        try:
            assert threading.currentThread() == cw.cwpy
            # ファイルパスを渡して読込
            pygame.mixer.music.load(path)
            return 0
        except Exception:
            cw.util.print_ex()
            print("BGMが読み込めません", path)
    return -1


def load_sound(path: str) -> SoundInterface:
    """効果音ファイルを読み込み、SoundInterfaceを返す。
    読み込めなかった場合は、無音で再生するSoundInterfaceを返す。
    path: 効果音ファイルのパス。
    """
    if threading.currentThread() != cw.cwpy:
        raise Exception()

    if cw.cwpy.rsrc:
        path = cw.cwpy.rsrc.get_filepath(path)

    if not os.path.isfile(path) or (not (cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init()) and
                                    not cw.bassplayer.is_alivablewithpath(path)):
        return SoundInterface()

    if cw.cwpy.is_playingscenario() and path in cw.cwpy.sdata.resource_cache:
        obj = cw.cwpy.sdata.resource_cache[path]
        assert isinstance(obj, SoundInterface)
        return obj.copy()

    try:
        assert threading.currentThread() == cw.cwpy
        if cw.bassplayer.is_alivablewithpath(path):
            # BASSが使用できる場合
            sound = SoundInterface(path, path, is_midi=is_midi(path))
        elif sys.platform == "win32" and (path.lower().endswith(".wav") or
                                          path.lower().endswith(".mp3")):
            # WinMMを使用する事でSDL_mixerの問題を避ける
            # FIXME: mp3効果音をWindows環境でしか再生できない
            sound = SoundInterface(path, path, is_midi=is_midi(path))
        elif cw.cwpy.setting.sdlmixer_enabled and pygame.mixer.get_init():
            with open(path, "rb") as f:
                mixer_sound = pygame.mixer.Sound(f)
                f.close()
            sound = SoundInterface(mixer_sound, path, is_midi=is_midi(path))
        else:
            return SoundInterface()
    except Exception:
        print("サウンドが読み込めません", path)
        return SoundInterface()

    if cw.cwpy.is_playingscenario():
        cw.cwpy.sdata.sweep_resourcecache(os.path.getsize(path) if os.path.isfile(path) else 0)
        cw.cwpy.sdata.resource_cache[path] = sound

    return sound.copy()


def is_midi(path: str) -> bool:
    """pathがMIDIファイルか判定する。"""
    try:
        if cw.fsync.is_waiting(path):
            cw.fsync.sync()
        if os.path.isfile(path) and 4 <= os.path.getsize(path):
            with open(path, "rb") as f:
                return f.read(4) == b"MThd"
    except Exception:
        pass
    return cw.util.splitext(path)[1].lower() in (".mid", ".midi")


def get_soundfilepath(basedir: str, path: Union[str]) -> str:
    """宿のフォルダにある場合は問題が出るため、
    再生用のコピーを生成する。
    """
    if path and cw.cwpy.ydata and (path.startswith(cw.cwpy.ydata.yadodir) or
                                   path.startswith(cw.cwpy.ydata.tempdir)):
        dpath = join_paths(cw.tempdir, "Playing", basedir)
        fpath = os.path.basename(path)
        fpath = join_paths(dpath, fpath)
        fpath = cw.binary.util.check_duplicate(fpath)
        if not os.path.isdir(dpath):
            os.makedirs(dpath)
        shutil.copyfile(path, fpath)
        path = fpath
    return path


def remove_soundtempfile(basedir: str) -> None:
    """再生用のコピーを削除する。
    """
    cw.fsync.sync()
    dpath = join_paths(cw.tempdir, "Playing", basedir)
    if os.path.isdir(dpath):
        remove(dpath)
        if not os.listdir(join_paths(cw.tempdir, "Playing")):
            remove(dpath)


class Comparable(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def __lt__(self, other: typing.Any) -> bool: ...


_SortableByAttrT = TypeVar("_SortableByAttrT")
_SortableT = TypeVar("_SortableT", Comparable, str, int, float)


def _sorted_by_attr_impl(d: bool, seq: Iterable[_SortableByAttrT], *attr: str,
                         cmpfunc: Optional[Callable[[_SortableT, _SortableT], int]] = None) -> List[_SortableByAttrT]:
    if attr:
        key_attr = operator.attrgetter(*attr)

        def get_keyattr(a: _SortableByAttrT) -> _SortableT:
            return typing.cast(_SortableT, key_attr(a))
        get = get_keyattr
    else:
        def ret(a: _SortableByAttrT) -> _SortableT:
            return typing.cast(_SortableT, a)

        get = ret

    str_table: Dict[str, LogicalStr] = {}

    def logical_cmp_str(a: _SortableT, b: _SortableT) -> int:
        if not (isinstance(a, str) and isinstance(b, str)):
            return cmp(a, b)
        if a in str_table:
            al = str_table[a]
        else:
            al = LogicalStr(a)
            str_table[a] = al
        if b in str_table:
            bl = str_table[b]
        else:
            bl = LogicalStr(b)
            str_table[b] = bl
        return cmp(al, bl)

    def logical_cmp_impl(a: _SortableT, b: _SortableT) -> int:
        if (isinstance(a, tuple) and isinstance(b, tuple)) or \
                (isinstance(a, list) and isinstance(b, list)):
            r = 0
            for i in range(max(len(a), len(b))):
                if len(a) <= i:
                    return -1
                if len(b) <= i:
                    return 1
                aval = a[i]
                bval = b[i]
                r = logical_cmp_impl(aval, bval)
                if r != 0:
                    break
            return r
        else:
            r = logical_cmp_str(a, b)
            return r

    def logical_cmp(aobj: _SortableByAttrT, bobj: _SortableByAttrT) -> int:
        a = get(aobj)
        b = get(bobj)
        if cmpfunc:
            return cmpfunc(a, b)
        else:
            return logical_cmp_impl(a, b)

    key = functools.cmp_to_key(logical_cmp)
    if d:
        assert isinstance(seq, list)
        seq.sort(key=key)
        return seq
    else:
        return sorted(seq, key=key)


_re_num = re.compile("([0-9]+)")


class LogicalStr(Comparable):
    def __init__(self, s: str) -> None:
        self.seq: List[Union[str, Tuple[int, str]]] = []
        self.s = s
        if not s:
            return
        pos = 0
        while s != "":
            m = _re_num.search(s, pos=pos)
            if m is None:
                self.seq.append(s[pos:].lower())
                break
            si = m.start()
            ei = m.end()
            # 末尾に'0'をつける事で0より小さな文字コードの文字が前に来るようにする
            # 以下のケースでは、'0'をつけなければ"cw 1"が先頭に来てしまう
            #  cw 1 -> ['cw ', (1, '1')]
            #  cw ! -> ['cw !']
            #  cw a -> ['cw a']
            # '0'をつける事で以下のようにASCII順で並ぶ
            #  cw ! -> ['cw !']
            #  cw 1 -> ['cw 0', (1, '1')]
            #  cw a -> ['cw a']
            self.seq.append(s[pos:si].lower() + '0')
            ss = s[si:ei]
            self.seq.append((int(ss), ss))
            pos = ei

    def __lt__(self, other: object) -> bool:
        assert isinstance(other, LogicalStr)
        if self.seq < other.seq:
            return True
        elif other.seq < self.seq:
            return False
        return self.s < other.s

    def __eq__(self, other: object) -> bool:
        assert isinstance(other, LogicalStr)
        return self.seq == other.seq and self.s == other.s


assert LogicalStr("a1234b") > LogicalStr("a12b")
assert LogicalStr("a12b") < LogicalStr("a1234b")
assert LogicalStr("a12b") != LogicalStr("a1234b")
assert LogicalStr("a12b") < LogicalStr("ab")
assert LogicalStr("cw 1") < LogicalStr("cw a")
assert LogicalStr("cw 1") > LogicalStr("cw !")
assert LogicalStr("cw 1") > LogicalStr("cw ")
assert LogicalStr("cw 1") > LogicalStr("cw  ")
assert LogicalStr("cw 1") < LogicalStr("cw 2")
assert LogicalStr("a0b") < LogicalStr("a1b")
assert LogicalStr("a0 b") < LogicalStr("a1b")
assert LogicalStr("a2 b") > LogicalStr("a1b")
assert LogicalStr("a899999999999999999999999999999999999999999999999999999999999999999999999999999999999999999b") >\
       LogicalStr("a9b")
assert LogicalStr("a999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999b") >\
       LogicalStr("a8b")
assert LogicalStr("a999999999999999999999999999999999999999999999999999999999999999999999999999999999999999998b") <\
       LogicalStr("a999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999b")


def cmp(a: Optional[_SortableT], b: Optional[_SortableT]) -> int:
    if a is None and b is None:
        return 0
    elif a is None:
        return -1
    elif b is None:
        return 1
    elif type(a) is type(b):
        if a < b:
            return -1
        elif b < a:
            return 1
    else:
        if type(a) is int:
            return -1
        else:
            return 1
    return 0


def sorted_by_attr(seq: Iterable[_SortableByAttrT], *attr: str) -> List[_SortableByAttrT]:
    """非破壊的にオブジェクトの属性でソートする。
    seq: リスト
    attr: 属性名
    """
    return _sorted_by_attr_impl(False, seq, *attr)


def sort_by_attr(seq: List[_SortableByAttrT], *attr: str) -> List[_SortableByAttrT]:
    """破壊的にオブジェクトの属性でソートする。
    seq: リスト
    attr: 属性名
    """
    return _sorted_by_attr_impl(True, seq, *attr)


assert sort_by_attr(["a1234b", "a12b", "a1234b"]) == ["a12b", "a1234b", "a1234b"]
assert sort_by_attr(["a12b", "a1234b", "a1b", "a9b", "a01234b", "a1234b", "a-."]) == ["a-.", "a1b", "a9b", "a12b",
                                                                                      "a01234b", "a1234b", "a1234b"]
assert sort_by_attr([(1, "a"), None, (0, "b"), (0, "c")]) == [None, (0, "b"), (0, "c"), (1, "a")]

if sys.platform == "win32":
    _shlwapi = ctypes.windll.LoadLibrary("shlwapi.dll")
    if _shlwapi:
        _shlwapi.StrCmpLogicalW.argtypes = [ctypes.wintypes.LPCWSTR, ctypes.wintypes.LPCWSTR]
        _shlwapi.StrCmpLogicalW.restype = ctypes.wintypes.INT


def sort_by_filename(seq: List[_SortableByAttrT], *attr: str) -> List[_SortableByAttrT]:
    if sys.platform == "win32" and _shlwapi:
        def cmp_fname(a: str, b: str) -> int:
            result: int = _shlwapi.StrCmpLogicalW(ctypes.wintypes.LPCWSTR(a), ctypes.wintypes.LPCWSTR(b))
            return result

        seq = _sorted_by_attr_impl(True, seq, *attr, cmpfunc=cmp_fname)
    else:
        seq = _sorted_by_attr_impl(True, seq, *attr)
    return seq


def new_order(seq: Union[Iterable["cw.header.AdventurerHeader"],
                         Iterable["cw.header.CardHeader"],
                         Iterable["cw.header.PartyHeader"]],
              mode: int = 1) -> int:
    """order属性を持つアイテムのlistを
    走査して新しいorderを返す。
    必要であれば、seq内のorderを振り直す。
    mode: 0=最大order。1=最小order。orderの振り直しが発生する
    """
    if mode == 0:
        order = -1
        for item in seq:
            order = max(item.order, order)
        return order + 1
    else:
        for item in seq:
            item.order += 1
        return 0


def join_paths(*paths: str) -> str:
    """パス結合。ディレクトリの区切り文字はプラットホームに関わらず"/"固定。
    セキュリティ上の問題を避けるため、あえて絶対パスは取り扱わない。
    *paths: パス結合する文字列
    """
    return "/".join([a for a in paths if a]).replace("\\", "/").rstrip("/")


# FIXME: パスによって以下のような警告が標準エラー出力に出るようだが、詳細が分からない。
#        ***\ntpath.py:533: UnicodeWarning: Unicode unequal comparison failed to convert both arguments to
#        Unicode - interpreting them as being unequal
#        おそらく実際的な問題は発生しないので、とりあえず警告を無効化する。
warnings.filterwarnings("ignore", category=UnicodeWarning)


def relpath(path: str, start: str) -> str:
    if len(start) < len(path) and path.startswith(start) and start != "":
        path2 = path[len(start):]
        if path2[0] == '/' or (sys.platform == "win32" and path2[0] == '\\'):
            return path2[1:]
    try:
        path = os.path.abspath(path)
        return os.path.relpath(path, start)
    except Exception:
        return path


assert relpath("Data/abc", "Data") == "abc"
assert relpath("Data/abc/def", "Data").replace("\\", "/") == "abc/def"
assert relpath("Data/abc/def", "Data/abc/").replace("\\", "/") == "def"
assert relpath("Data/abc/def", "Data/abc") == os.path.relpath("Data/abc/def", "Data/abc")
assert relpath("Data/abc/def", "..").replace("\\", "/") == os.path.relpath("Data/abc/def", "..").replace("\\", "/")
assert relpath(".", "..").replace("\\", "/") == os.path.relpath(".", "..").replace("\\", "/")
assert relpath("/a", "..").replace("\\", "/") == os.path.relpath("/a", "..").replace("\\", "/")
assert relpath("a", "../bcde").replace("\\", "/") == os.path.relpath("a", "../bcde").replace("\\", "/")
assert relpath("../a", "../bcde").replace("\\", "/") == os.path.relpath("../a", "../bcde").replace("\\", "/")
assert relpath("../a", "../").replace("\\", "/") == os.path.relpath("../a", "../").replace("\\", "/")


@typing.overload
def validate_filepath(fpath: Optional[str]) -> str: ...


@typing.overload
def validate_filepath(fpath: List[Optional[str]]) -> List[str]: ...


def validate_filepath(fpath: Optional[Union[str, List[Optional[str]]]]) -> Union[List[str], str]:
    """
    fpathが絶対パスまたは外部ディレクトリを指定する
    相対パスであれば空文字列に置換する。
    """
    if isinstance(fpath, list):
        seq = []
        for f in fpath:
            f = validate_filepath(f)
            if f:
                seq.append(f)
        return seq
    else:
        if not fpath:
            return ""
        from cw.binary.image import path_is_code
        if path_is_code(fpath):
            return fpath
        if os.path.isabs(fpath):
            return ""
        else:
            n = join_paths(os.path.normpath(fpath))
            if n == ".." or n.startswith("../"):
                return ""
        return fpath


assert validate_filepath(["/test/abc", None, "test/../test", "test/../../abc", "../abc"]) == \
       ["test/../test"]


def is_descendant(path: str, start: str) -> str:
    """
    pathはstartのサブディレクトリにあるか。
    ある場合は相対パスを返す。
    """
    if not path or not start:
        return ""
    rel = join_paths(relpath(path, start))
    if os.path.isabs(rel):
        return ""
    if rel.startswith("../"):
        return ""
    return rel


def splitext(fname: str) -> Tuple[str, str]:
    """パスの拡張子以外の部分と拡張子部分の分割。
    os.path.splitext()との違いは、".ext"のような
    拡張子部分だけのパスの時、(".ext", "")ではなく
    ("", ".ext")を返す事である。
    """
    p = os.path.splitext(fname)
    if p[0].startswith(".") and not p[1]:
        return (p[1], p[0])
    return p


def str2bool(s: str) -> bool:
    """特定の文字列をbool値にして返す。
    s: bool値に変換する文字列(true, false, 1, 0など)。
    """
    if isinstance(s, bool):
        return s
    else:
        s = s.lower()

        if s == "true":
            return True
        elif s == "false":
            return False
        elif s == "1":
            return True
        elif s == "0":
            return False
        else:
            raise ValueError("%s is incorrect value!" % (s))


@typing.overload
def numwrap(n: int, nmin: int, nmax: int) -> int: ...


@typing.overload
def numwrap(n: float, nmin: float, nmax: float) -> float: ...


def numwrap(n: Union[int, float], nmin: Union[int, float], nmax: Union[int, float]) -> Union[int, float]:
    """最小値、最大値の範囲内でnの値を返す。
    n: 範囲内で調整される値。
    nmin: 最小値。
    nmax: 最大値。
    """
    if n < nmin:
        n = nmin
    elif n > nmax:
        n = nmax

    return n


def div_vocation(value: int) -> int:
    """能力判定のために能力値を2で割る。
    0以上の場合とマイナス値の場合で式が異なる。
    """
    if value < 0:
        return (value + 2) // 2
    else:
        return (value + 1) // 2


def get_truetypefontname(path: str) -> str:
    """引数のTrueTypeFontファイルを読み込んで、フォントネームを返す。
    ref http://mail.python.org/pipermail/python-list/2008-September/508476.html
    path: TrueTypeFontファイルのパス。
    """
    # customize path
    with open(path, "rb") as f:

        # header
        shead = struct.Struct(">IHHHH")
        fhead = f.read(shead.size)
        dhead = shead.unpack_from(fhead, 0)

        # font directory
        stable = struct.Struct(">4sIII")
        ftable = f.read(stable.size * dhead[1])
        for i in range(dhead[1]):  # directory records
            dtable = stable.unpack_from(ftable, i * stable.size)
            if dtable[0] == b"name":
                break
        assert dtable[0] == b"name"

        # name table
        f.seek(dtable[2])  # at offset
        fnametable = f.read(dtable[3])  # length
        snamehead = struct.Struct(">HHH")  # name table head
        dnamehead = snamehead.unpack_from(fnametable, 0)

        sname = struct.Struct(">HHHHHH")
        fontname = b""

        for i in range(dnamehead[1]):  # name table records
            dname = sname.unpack_from(fnametable, snamehead.size + i * sname.size)

            if dname[3] == 4:  # key == 4: "full name of font"
                s = struct.unpack_from('%is' % dname[4], fnametable,
                                       dnamehead[2] + dname[5])[0]
                if dname[:3] == (1, 0, 0):
                    fontname = s
                elif dname[:3] == (3, 1, 1033):
                    s = s.split(b"\x00")
                    fontname = b"".join(s)
        f.close()

    return str(fontname, "ascii")


def get_md5(path: str) -> str:
    """MD5を使ったハッシュ値を返す。
    path: ハッシュ値を求めるファイルのパス。
    """
    m = hashlib.md5()
    with open(path, "rb") as f:

        while True:
            data = f.read(32768)

            if not data:
                break

            m.update(data)
        f.close()

    return m.hexdigest()


def get_md5_from_data(data: bytes) -> str:
    """MD5を使ったハッシュ値を返す。
    data: ハッシュ値を求めるデータ。
    """
    m = hashlib.md5()
    m.update(data)
    return m.hexdigest()


def number_normalization(value: int, fromvalue: int, tovalue: int) -> int:
    """数値を範囲内の値に正規化する。
    value: 正規化対象の数値。
    fromvalue: 範囲の最小値。
    tovalue: 範囲の最大値+1。
    """
    if 0 == tovalue:
        return value
    if tovalue <= value or value < fromvalue:
        value -= (value // tovalue) * tovalue
    if value < fromvalue:
        value += tovalue
    return value


def print_ex(file: Optional[TextIO] = None) -> None:
    """例外の内容を標準出力に書き足す。
    """
    if file is None:
        file = sys.stdout
    if file:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback, file=file)
        file.write("\n")


def screenshot_title(titledic: Dict[str, str]) -> str:
    """スクリーンショットタイトルの書き出し。
    """
    title = format_title(cw.cwpy.setting.ssinfoformat, titledic)
    return title


def screenshot_header(title: str, w: int) -> Tuple[pygame.surface.Surface, pygame.surface.Surface, int, int]:
    """スクリーンショット情報の書き出し。
    """
    fore = cw.cwpy.setting.ssinfofontcolor
    back = cw.cwpy.setting.ssinfobackcolor
    font = cw.cwpy.rsrc.fonts["screenshot"]
    fh = font.size("#")[1]
    lh = fh + 2
    imgs = []
    for color in (fore, back):
        subimg = font.render(title, True, color)
        swmax = w - cw.s(5) * 2
        if swmax < subimg.get_width():
            size = (swmax, subimg.get_height())
            subimg = cw.image.smoothscale(subimg, size)
        imgs.append(subimg)
    imgs[1].fill((255, 255, 255, 80), special_flags=pygame.BLEND_RGBA_MULT)
    return imgs[0], imgs[1], fh, lh


def screenshot() -> None:
    """スクリーンショットをファイルへ書き出す。
    """
    cw.cwpy.play_sound("screenshot")
    t = cw.cwpy.get_titledic(with_datetime=True, for_fname=True)
    assert isinstance(t, tuple)
    titledic, titledicfn = t
    filename = create_screenshotfilename(titledicfn)
    try:
        dpath = os.path.dirname(filename)
        if os.path.isdir(dpath):
            filename = dupcheck_plus(filename, yado=False)
        else:
            os.makedirs(dpath)
        bmp, y = create_screenshot(titledic)
        pygame.image.save(bmp, filename)
    except Exception:
        cw.util.print_ex()
        s = "スクリーンショットの保存に失敗しました。\n%s" % (filename)
        cw.cwpy.call_modaldlg("ERROR", text=s)


def create_screenshotfilename(titledic: Dict[str, str]) -> str:
    """スクリーンショット用のファイルパスを作成する。
    """
    fpath = format_title(cw.cwpy.setting.ssfnameformat, titledic)
    if not cw.util.splitext(fpath)[1].lower() in cw.EXTS_IMG:
        fpath += ".png"
    return fpath


def create_screenshot(titledic: Dict[str, str]) -> Tuple[pygame.surface.Surface, int]:
    """スクリーンショットを作成する。
    """
    title = screenshot_title(titledic)
    scr = pygame.surface.Surface(cw.cwpy.scr_draw.get_size()).convert()
    cw.cwpy.draw_to(scr, False)
    if title:
        back = cw.cwpy.setting.ssinfobackcolor
        w = cw.s(cw.SIZE_GAME[0])
        subimg, subimg2, fh, lh = screenshot_header(title, w)
        if cw.cwpy.setting.sswithstatusbar:
            h = cw.s(cw.SIZE_GAME[1]) + lh
        else:
            h = cw.s(cw.SIZE_AREA[1]) + lh
        bmp = pygame.surface.Surface((w, h)).convert()
        bmp.fill(back, rect=pygame.rect.Rect(cw.s(0), cw.s(0), w, lh))
        if cw.cwpy.setting.ssinfobackimage and os.path.isfile(cw.cwpy.setting.ssinfobackimage):
            subimg3 = load_image(cw.cwpy.setting.ssinfobackimage, False)
            fill_image(bmp, cw.s(subimg3), (w, lh))
        else:
            fpath = cw.util.find_resource(cw.util.join_paths(cw.cwpy.skindir,
                                                             "Resource/Image/Other/SCREENSHOT_HEADER"),
                                          cw.M_IMG)
            if fpath:
                subimg3 = load_image(fpath, False)
                fill_image(bmp, cw.s(subimg3), (w, lh))
        bmp.blit(scr, (cw.s(0), lh))
        x = cw.s(5)
        y = (lh - fh) // 2
        for xx in range(-1, 1 + 1):
            for yy in range(-1, 1 + 1):
                if xx != x or yy != y:
                    bmp.blit(subimg2, (x + xx, y + yy))
        bmp.blit(subimg, (x, y))
        y = lh
    else:
        if cw.cwpy.setting.sswithstatusbar:
            bmp = scr
        else:
            bmp = scr.subsurface((cw.s((0, 0)), cw.s(cw.SIZE_AREA)))
        y = cw.s(0)

    return bmp, y


def card_screenshot() -> None:
    """ パーティー所持カードのスクリーンショットをファイルへ書き出す。
    """
    if cw.cwpy.ydata:
        if cw.cwpy.ydata.party:
            cw.cwpy.play_sound("screenshot")
            t = cw.cwpy.get_titledic(with_datetime=True, for_fname=True)
            assert isinstance(t, tuple)
            titledic, titledicfn = t
            filename = create_cardscreenshotfilename(titledicfn)
            try:
                dpath = os.path.dirname(filename)
                if os.path.isdir(dpath):
                    filename = dupcheck_plus(filename, yado=False)
                else:
                    os.makedirs(dpath)
                bmp = create_cardscreenshot(titledic)
                pygame.image.save(bmp, filename)
            except Exception:
                cw.util.print_ex()
                s = "スクリーンショットの保存に失敗しました。\n%s" % (filename)
                cw.cwpy.call_modaldlg("ERROR", text=s)


def create_cardscreenshotfilename(titledic: Dict[str, str]) -> str:
    """パーティー所持カードスクリーンショット用のファイルパスを作成する。
    """
    fpath = format_title(cw.cwpy.setting.cardssfnameformat, titledic)
    if not cw.util.splitext(fpath)[1].lower() in cw.EXTS_IMG:
        fpath += ".png"
    return fpath


def create_cardscreenshot(titledic: Dict[str, str]) -> pygame.surface.Surface:
    """パーティー所持カードスクリーンショットを作成する。
    """

    pcards = [i for i in cw.cwpy.get_pcards()]
    if pcards:
        max_card = [2, 2, 2, 0]
        margin = 2
        # 背景のタイル色
        # タイトルバーに馴染む色にする
        back = [[min(255, max(0, n // 2 + 88)) for n in cw.cwpy.setting.ssinfobackcolor],
                [min(255, max(0, n // 2 + 40)) for n in cw.cwpy.setting.ssinfobackcolor]]

        # カード数によってタイルのサイズを決定
        for pcard in pcards:
            for index in (cw.POCKET_SKILL, cw.POCKET_ITEM, cw.POCKET_BEAST):
                max_card[index] = max(len(pcard.cardpocket[index]), max_card[index])
            if cw.cwpy.setting.show_personal_cards:
                max_card[3] = max(len(pcard.personal_pocket), max_card[3])

        if 0 < max_card[3]:
            # 私物が1枚でも存在するのであれば、最小で2枚分の横幅を確保する
            max_card[3] = max(max_card[3], 2)

        w = cw.s(95 + 80 * sum(max_card) + margin * (5 + sum(max_card)))
        if max_card[3]:
            w += cw.s(margin)
        h = cw.s((130 + 2 * margin) * len(pcards))
        title = screenshot_title(titledic)
        if title:
            subimg, subimg2, fh, lh = screenshot_header(title, w)
            h += lh
        bmp = pygame.surface.Surface((w, h)).convert()
        bmp.fill(cw.cwpy.setting.ssinfobackcolor, rect=pygame.rect.Rect(cw.s(0), cw.s(0), w, h))

        # 背景画像
        if title:
            if cw.cwpy.setting.ssinfobackimage and os.path.isfile(cw.cwpy.setting.ssinfobackimage):
                subimg3 = load_image(cw.cwpy.setting.ssinfobackimage, False)
                fill_image(bmp, cw.s(subimg3), (w, lh))
            else:
                fpath = cw.util.find_resource(cw.util.join_paths(cw.cwpy.skindir,
                                                                 "Resource/Image/Other/SCREENSHOT_HEADER"),
                                              cw.M_IMG)
                if fpath:
                    subimg3 = load_image(fpath, False)
                    fill_image(bmp, cw.s(subimg3), (w, lh))

        # イメージの作成
        sy = cw.s(0)
        if title:
            x, y = cw.s(5), (lh - fh) // 2
            for xx in range(-1, 1 + 1):
                for yy in range(-1, 1 + 1):
                    if xx != x or yy != y:
                        bmp.blit(subimg2, (x + xx, y + yy))
            bmp.blit(subimg, (x, y))
            sy += lh

        for i in range(len(pcards)):
            backindex = (1 + i) % 2
            bmp.fill(back[backindex], rect=pygame.rect.Rect(cw.s(0), sy, cw.s(95 + 2 * margin), cw.s(130 + 2 * margin)))
            bmp.blit(pcards[i].cardimg.image, (cw.s(margin), sy + cw.s(margin)))

            def blit_card(headers: Iterable[cw.header.CardHeader], x: int, sy: int) -> None:
                for header in headers:
                    if header.negaflag:
                        header.negaflag = False
                    bmp.blit(header.cardimg.get_cardimg(header), (cw.s(x), sy + cw.s(10 + margin)))
                    x += 80 + margin

            current_x = 95 + 2 * margin
            next_x = 0
            for index in (cw.POCKET_SKILL, cw.POCKET_ITEM, cw.POCKET_BEAST, cw.POCKET_PERSONAL):
                if max_card[index] == 0:
                    continue
                current_x += next_x
                next_x = 80 * max_card[index] + margin * (max_card[index] + 1)
                backindex = (index + i) % 2
                bmp.fill(back[backindex], rect=pygame.rect.Rect(cw.s(current_x), sy, cw.s(next_x),
                                                                cw.s(130 + 2 * margin)))
                if index == cw.POCKET_PERSONAL:
                    adjust_x = (max_card[index] - len(pcards[i].personal_pocket))
                else:
                    adjust_x = (max_card[index] - len(pcards[i].cardpocket[index]))
                x = current_x + adjust_x * 40 + margin * (2 + adjust_x) // 2
                if index == cw.POCKET_PERSONAL:
                    blit_card(pcards[i].personal_pocket, x, sy)
                else:
                    blit_card(pcards[i].cardpocket[index], x, sy)

            sy += cw.s(130 + 2 * margin)

    else:
        raise Exception()

    return bmp


def to_clipboard(s: str) -> None:
    """テキストsをクリップボードへ転写する。"""
    tdo = wx.TextDataObject()
    tdo.SetText(s)
    if wx.TheClipboard.Open():
        wx.TheClipboard.SetData(tdo)
        wx.TheClipboard.Close()
        wx.TheClipboard.Flush()


# ------------------------------------------------------------------------------
# ファイル操作関連
# ------------------------------------------------------------------------------

def dupcheck_plus(path: str, yado: bool = True) -> str:
    """パスの重複チェック。引数のパスをチェックし、重複していたら、
    ファイル・フォルダ名の後ろに"(n)"を付加して重複を回避する。
    宿のファイルパスの場合は、"Data/Temp/Yado"ディレクトリの重複もチェックする。
    """
    cw.fsync.sync()

    tempyado = cw.util.join_paths(cw.tempdir, "Yado")
    dpath, basename = os.path.split(path)
    fname, ext = cw.util.splitext(basename)
    fname = cw.binary.util.check_filename(fname.strip())
    ext = ext.strip()
    basename = fname + ext
    path = join_paths(dpath, basename)

    if yado:
        if path.startswith("Yado"):
            temppath = path.replace("Yado", tempyado, 1)
        elif path.startswith(tempyado):
            temppath = path.replace(tempyado, "Yado", 1)
        else:
            print("宿パスの重複チェック失敗", path)
            temppath = ""

    else:
        temppath = ""

    count = 2

    while os.path.exists(path) or os.path.exists(temppath):
        basename = "%s(%d)%s" % (fname, count, ext)
        path = join_paths(dpath, basename)

        if yado:
            if path.startswith("Yado"):
                temppath = path.replace("Yado", tempyado, 1)
            elif path.startswith(tempyado):
                temppath = path.replace(tempyado, "Yado", 1)
            else:
                print("宿パスの重複チェック失敗", path)
                temppath = ""

        count += 1

    return join_paths(dpath, basename)


def repl_dischar(fname: str) -> str:
    """
    ファイル名使用不可文字を代替文字に置換し、
    両端に空白があった場合は削除する。
    """
    d = {'\\': '￥', '/': '／', ':': '：', ',': '，', ';': '；',
         '*': '＊', '?': '？', '"': '”', '<': '＜', '>': '＞',
         '|': '｜'}

    for key, value in d.items():
        fname = fname.replace(key, value)

    fname = fname.strip()
    if fname == "":
        fname = "noname"
    return fname


def check_dischar(s: str) -> bool:
    """
    ファイル名使用不可文字を含んでいるかチェックする。
    """
    seq = ('\\', '/', ':', ',', ';', '*', '?', '"', '<', '>', '|', '"')

    for i in seq:
        if s.find(i) >= 0:
            return True

    return False


def join_yadodir(path: str) -> str:
    """
    引数のpathを現在読み込んでいる宿ディレクトリと結合させる。
    "Data/Temp/Yado"にパスが存在すれば、そちらを優先させる。
    """
    temppath = join_paths(cw.cwpy.tempdir, path)
    yadopath = join_paths(cw.cwpy.yadodir, path)

    if temppath and cw.fsync.is_waiting(temppath):
        cw.fsync.sync()
    elif yadopath and cw.fsync.is_waiting(yadopath):
        cw.fsync.sync()

    if os.path.exists(temppath):
        return temppath
    else:
        return yadopath


def get_yadofilepath(path: str) -> str:
    """"Data/Yado"もしくは"Data/Temp/Yado"のファイルパスの存在チェックをかけ、
    存在しているパスを返す。存在していない場合は""を返す。
    "Data/Temp/Yado"にパス優先。
    """
    if not cw.cwpy.ydata:
        return ""
    elif path.startswith(cw.cwpy.tempdir):
        temppath = path
        yadopath = path.replace(cw.cwpy.tempdir, cw.cwpy.yadodir, 1)
    elif path.startswith(cw.cwpy.yadodir):
        temppath = path.replace(cw.cwpy.yadodir, cw.cwpy.tempdir, 1)
        yadopath = path
    else:
        return ""

    if temppath and cw.fsync.is_waiting(temppath):
        cw.fsync.sync()
    elif yadopath and cw.fsync.is_waiting(yadopath):
        cw.fsync.sync()

    if yadopath in cw.cwpy.ydata.deletedpaths:
        return ""
    elif os.path.isfile(temppath):
        return temppath
    elif os.path.isfile(yadopath):
        return yadopath
    else:
        return ""


def find_resource(path: str, mtype: int) -> str:
    """pathとmtypeに該当する素材を拡張子の優先順に沿って探す。"""
    cw.fsync.sync()
    if mtype == cw.M_IMG:
        t = (".png", ".bmp", ".gif", ".jpg")
    elif mtype == cw.M_MSC:
        t = (".ogg", ".mp3", ".mid", ".wav")
    elif mtype == cw.M_SND:
        t = (".wav", ".ogg", ".mp3", ".mid")
    else:
        assert False, mtype

    if os.path.normcase("A") != os.path.normcase("a"):
        seq = []
        for t2 in t[:]:
            seq.append(t2)
            seq.append(t2.upper())
        seq2: Iterable[str] = seq
    else:
        seq2 = t

    for ext in seq2:
        path2 = path + ext
        if cw.cwpy:
            path2 = cw.cwpy.rsrc.get_filepath(path2)
        if os.path.isfile(path2):
            return path2

    if is_descendant(path, cw.cwpy.skindir):
        path = join_paths("Data/SkinBase", relpath(path, cw.cwpy.skindir))
        return find_resource(path, mtype)

    return ""


def get_inusecardmaterialpath(path: str, mtype: int, inusecard: Optional["cw.header.CardHeader"] = None,
                              findskin: bool = True) -> str:
    """pathが宿からシナリオへ持ち込んだカードの
    素材を指していればそのパスを返す。
    そうでない場合は空文字列を返す。"""
    imgpath = ""
    cw.fsync.sync()
    if cw.cwpy.event.in_inusecardevent:
        if inusecard or (cw.cwpy.is_runningevent() and cw.cwpy.event.get_inusecard()):
            if not inusecard:
                inusecard = cw.cwpy.event.get_inusecard()
            assert inusecard
            assert inusecard.carddata is not None
            if not inusecard.carddata.getbool(".", "scenariocard", False) or \
                    inusecard.carddata.gettext("Property/Materials", ""):
                imgpath = cw.util.join_yadodir(path)
                imgpath = get_materialpathfromskin(imgpath, mtype, findskin=findskin)
    return imgpath


def get_materialpath(path: str, mtype: int, scedir: str = "", system: bool = False, findskin: bool = True) -> str:
    """pathが指す素材を、シナリオプレイ中はシナリオ内から探し、
    プレイ中でない場合や存在しない場合はスキンから探す。
    path: 素材の相対パス。
    type: 素材のタイプ。cw.M_IMG, cw.M_MSC, cw.M_SNDのいずれか。
    """
    cw.fsync.sync()
    if mtype == cw.M_IMG and cw.binary.image.path_is_code(path):
        return path

    orig_scedir = scedir
    cachekey = (path, mtype, system, findskin)
    if orig_scedir == "":
        if cachekey in cw.cwpy.sdata.path_cache:
            return cw.cwpy.sdata.path_cache[cachekey]

    if not system and (cw.cwpy.is_playingscenario() or scedir):
        tpath = cw.util.join_paths(cw.tempdir, "ScenarioLog/TempFile", path)
        tpath = cw.cwpy.rsrc.get_filepath(tpath)
        if os.path.isfile(tpath):
            path = tpath
        else:
            if not scedir:
                scedir = cw.cwpy.sdata.scedir
            path = cw.util.join_paths(scedir, path)
            path = cw.cwpy.rsrc.get_filepath(path)
    else:
        path = cw.cwpy.rsrc.get_filepath(path)
        if not os.path.isfile(path):
            path = cw.util.join_paths(cw.cwpy.skindir, path)

    path = get_materialpathfromskin(path, mtype, findskin=findskin)
    if orig_scedir == "":
        cw.cwpy.sdata.path_cache[cachekey] = path
    return path


def get_materialpathfromskin(path: str, mtype: int, findskin: bool = True) -> str:
    cw.fsync.sync()
    if not os.path.isfile(path):
        if not findskin:
            path = ""
        elif path.startswith(cw.cwpy.skindir):
            fname = cw.util.splitext(path)[0]
            if mtype == cw.M_IMG:
                path2 = cw.util.find_resource(fname, cw.cwpy.rsrc.ext_img)
            elif mtype == cw.M_MSC:
                path2 = cw.util.find_resource(fname, cw.cwpy.rsrc.ext_bgm)
            elif mtype == cw.M_SND:
                path2 = cw.util.find_resource(fname, cw.cwpy.rsrc.ext_snd)
            else:
                assert False
            if path2:
                return path2

        fname = os.path.basename(path)
        lfname = fname.lower()
        eb = lfname.endswith(".jpy1") or lfname.endswith(".jptx") or lfname.endswith(".jpdc")
        if not eb:
            fname = cw.util.splitext(fname)[0]
        dpaths = [cw.cwpy.skindir]
        if os.path.isdir("Data/Materials"):
            dpaths.extend([cw.util.join_paths("Data/Materials", d) for d in os.listdir("Data/Materials")])
        for dpath in dpaths:
            if eb:
                # エフェクトブースターのファイルは他の拡張子への付替を行わない
                path = cw.cwpy.rsrc.get_filepath(cw.util.join_paths(dpath, "Table", fname))
            elif mtype == cw.M_IMG:
                path = cw.util.find_resource(cw.util.join_paths(dpath, "Table", fname), cw.cwpy.rsrc.ext_img)
            elif mtype == cw.M_MSC:
                path = cw.util.find_resource(cw.util.join_paths(dpath, "Bgm", fname), cw.cwpy.rsrc.ext_bgm)
                if not path:
                    path = cw.util.find_resource(cw.util.join_paths(dpath, "BgmAndSound", fname),
                                                 cw.cwpy.rsrc.ext_bgm)
            elif mtype == cw.M_SND:
                path = cw.util.find_resource(cw.util.join_paths(dpath, "Sound", fname), cw.cwpy.rsrc.ext_snd)
                if not path:
                    path = cw.util.find_resource(cw.util.join_paths(dpath, "BgmAndSound", fname),
                                                 cw.cwpy.rsrc.ext_snd)

            if path:
                break

    return path


class FileSync(threading.Thread):
    """ファイルをfsyncしながら出力する。"""

    def __init__(self) -> None:
        threading.Thread.__init__(self)
        self._quit = False
        self._files: List[Tuple[str, Union[bytes, str], str, Optional[str]]] = []
        self._mutex = threading.Lock()

    def quit(self) -> None:
        """全てのファイル出力が完了してからスレッドを終了する。"""
        self._quit = True

    def push(self, file: str, data: Union[bytes, str], mode: str = "wb", encoding: Optional[str] = None) -> None:
        """出力対象を追加する。"""
        with self._mutex:
            self._files.append((file, data, mode, encoding))

        if self._quit:
            try:
                self.join()
            except Exception:
                cw.util.print_ex()
            self._write_files()

    def sync(self) -> None:
        """全てのファイル出力が完了するまで待ち合わせる。"""
        while not self._quit:
            with self._mutex:
                if not self._files:
                    break
            time.sleep(0.001)

    def is_waiting(self, file: str) -> bool:
        """fileの書き込みを待ち合わせ中か。"""
        # FIXME: get_keypath()とget_symlinktarget()のパフォーマンスが非常に悪いので
        #        常にTrueを返してcw.fsync.sync()を呼び出させる。
        # file = get_keypath(get_symlinktarget(file))
        # with self._mutex:
        #     return file in map(lambda t: get_keypath(get_symlinktarget(t[0])), self._files)
        return True

    def _write_files(self) -> None:
        while True:
            with self._mutex:
                if self._files:
                    file, data, mode, encoding = self._files[0]
                else:
                    break
            i = 0
            while True:
                i += 1
                if i == 1:
                    tmp = file + ".cardwirthpy_temp"
                else:
                    tmp = file + ".cardwirthpy_temp(%s)" % i
                if not os.path.exists(tmp):
                    break
            try:
                with open(tmp, mode, encoding=encoding) as f:
                    f.write(data)
                    f.flush()
                    os.fsync(f.fileno())
                    f.close()
                os.replace(tmp, file)
            except Exception:
                print_ex(file=sys.stderr)
            with self._mutex:
                self._files.pop(0)

    def run(self) -> None:
        while not self._quit:
            self._write_files()
            time.sleep(0.001)
        self._write_files()


def write_file(file: str, data: Union[str, bytes], fsync: FileSync, mode: str = "wb",
               encoding: Optional[str] = None) -> None:
    """ファイルを出力する。"""
    if fsync:
        fsync.push(file, data, mode=mode, encoding=encoding)
    else:
        with open(file, mode, encoding=encoding) as f:
            f.write(data)
            f.flush()
            f.close()


def write_textfile(file: str, data: str, fsync: FileSync) -> None:
    """UTF-8のテキストファイルを出力する。"""
    write_file(file, data, fsync, mode="w", encoding="utf-8")


def remove_temp() -> None:
    """
    一時ディレクトリを空にする。
    """
    cw.fsync.sync()
    dpath = cw.tempdir

    if not os.path.exists(dpath):
        os.makedirs(dpath)

    removeall = True
    for name in os.listdir(dpath):
        if name in ("Scenario", "LockFiles"):
            removeall = False
        else:
            path = join_paths(dpath, name)
            try:
                remove(path)
            except Exception:
                print_ex()
                remove_treefiles(path)

    if removeall and cw.tempdir != cw.tempdir_init:
        try:
            remove(cw.tempdir)
        except Exception:
            print_ex()
            remove_treefiles(cw.tempdir)

    try:
        remove("Data/Temp/Global/Deleted")
    except Exception:
        pass


def remove(path: str, trashbox: bool = False) -> None:
    cw.fsync.sync()
    if os.path.isfile(path):
        remove_file(path, trashbox=trashbox)
    elif os.path.isdir(path):
        if join_paths(path).lower().startswith("data/temp/") and not trashbox:
            # Tempフォルダは、フォルダの内容さえ消えていれば
            # 空フォルダが残っていてもほとんど無害
            try:
                remove_treefiles(path, trashbox=trashbox)
                remove_tree(path, noretry=True, trashbox=trashbox)
            except Exception:
                # まれにフォルダ削除に失敗する環境がある
                # print_ex(file=sys.stderr)
                print_ex()
                remove_treefiles(path, trashbox=trashbox)
        else:
            if trashbox:
                try:
                    remove_tree(path, trashbox=trashbox)
                except Exception:
                    print_ex()
                    remove_treefiles(path, trashbox=trashbox)
                    remove_tree(path, trashbox=trashbox)
            else:
                remove_treefiles(path, trashbox=trashbox)
                remove_tree(path, trashbox=trashbox)


def remove_file(path: str, retry: int = 0, trashbox: bool = False) -> None:
    cw.fsync.sync()
    try:
        if trashbox:
            send_trashbox(path)
        else:
            os.remove(path)
    except OSError as err:
        if err.errno == 13 and retry < 5:
            os.chmod(path, stat.S_IWRITE | stat.S_IREAD)
            remove_file(path, retry + 1, trashbox=trashbox)
        elif retry < 5:
            time.sleep(1)
            remove_tree(path, retry + 1, trashbox=trashbox)
        else:
            raise err


def add_winauth(file: str) -> None:
    if os.path.isfile(file) and sys.platform == "win32":
        os.chmod(file, stat.S_IWRITE | stat.S_IREAD)


def remove_tree(treepath: str, retry: int = 0, noretry: bool = False, trashbox: bool = False) -> None:
    cw.fsync.sync()
    try:
        if trashbox:
            send_trashbox(treepath)
        else:
            shutil.rmtree(treepath)
    except OSError as err:
        if err.errno == 13 and retry < 5 and not noretry:
            for dpath, dnames, fnames in os.walk(treepath):
                for dname in dnames:
                    path = join_paths(dpath, dname)
                    if os.path.isdir(path):
                        try:
                            os.chmod(path, stat.S_IWRITE | stat.S_IREAD)
                        except OSError:
                            print_ex()
                            time.sleep(1)
                            remove_tree2(treepath, trashbox=trashbox)
                            return

                for fname in fnames:
                    path = join_paths(dpath, fname)
                    if os.path.isfile(path):
                        try:
                            os.chmod(path, stat.S_IWRITE | stat.S_IREAD)
                        except OSError:
                            print_ex()
                            time.sleep(1)
                            remove_tree2(treepath, trashbox=trashbox)
                            return

            remove_tree(treepath, retry + 1, trashbox=trashbox)
        elif retry < 5 and not noretry:
            time.sleep(1)
            remove_tree(treepath, retry + 1, trashbox=trashbox)
        else:
            remove_tree2(treepath, trashbox=trashbox)


def remove_tree2(treepath: str, trashbox: bool = False) -> None:
    # shutil.rmtree()で権限付与時にエラーになる事があるので
    # 削除方法を変えてみる
    cw.fsync.sync()
    for dpath, dnames, fnames in os.walk(treepath, topdown=False):
        for dname in dnames:
            path = join_paths(dpath, dname)
            if os.path.isdir(path):
                os.rmdir(path)
        for fname in fnames:
            path = join_paths(dpath, fname)
            if os.path.isfile(path):
                if trashbox:
                    send_trashbox(path)
                else:
                    os.remove(path)
    os.rmdir(treepath)


def remove_treefiles(treepath: str, trashbox: bool = False) -> None:
    # remove_tree2()でもたまにエラーになる環境があるらしいので、
    # せめてディレクトリだけでなくファイルだけでも削除を試みる
    cw.fsync.sync()
    for dpath, dnames, fnames in os.walk(treepath, topdown=False):
        for fname in fnames:
            path = join_paths(dpath, fname)
            if os.path.isfile(path):
                add_winauth(path)
                if trashbox:
                    send_trashbox(path)
                else:
                    os.remove(path)


def rename_file(path: str, dstpath: str, trashbox: bool = False) -> None:
    """pathをdstpathへ移動する。
    すでにdstpathがある場合は上書きされる。
    """
    cw.fsync.sync()
    if not os.path.isdir(os.path.dirname(dstpath)):
        os.makedirs(os.path.dirname(dstpath))
    if os.path.isfile(dstpath):
        remove_file(dstpath, trashbox=trashbox)
    try:
        shutil.move(path, dstpath)
    except OSError:
        # ファイルシステムが異なっていると失敗する
        # 可能性があるのでコピー&削除を試みる
        print_ex()
        with open(path, "rb") as f1:
            with open(dstpath, "wb") as f2:
                f2.write(f1.read())
                f2.flush()
                os.fsync(f2.fileno())
                f2.close()
            f1.close()
        remove_file(path, trashbox=trashbox)


def send_trashbox(path: str) -> None:
    """
    可能であればpathをゴミ箱へ送る。
    """
    cw.fsync.sync()
    if sys.platform == "win32":
        path = os.path.normpath(os.path.abspath(path))
        ope = win32com.shell.shellcon.FO_DELETE
        flags = (
                win32com.shell.shellcon.FOF_NOCONFIRMATION |
                win32com.shell.shellcon.FOF_ALLOWUNDO |
                win32com.shell.shellcon.FOF_SILENT
        )
        _ = win32com.shell.shell.SHFileOperation((None, ope, path + '\0\0', None, flags, None, None))
    elif os.path.isfile(path):
        os.remove(path)
    elif os.path.isdir(path):
        shutil.rmtree(path)


def send_trashbox2(paths: Iterable[str]) -> None:
    """
    可能であればpathsをゴミ箱へ送る。
    """
    cw.fsync.sync()
    if sys.platform == "win32":
        ope = win32com.shell.shellcon.FO_DELETE
        flags = (
                win32com.shell.shellcon.FOF_NOCONFIRMATION |
                win32com.shell.shellcon.FOF_ALLOWUNDO |
                win32com.shell.shellcon.FOF_SILENT
        )
        paths = "\0".join(map(lambda path: os.path.normpath(os.path.abspath(path)), paths)) + '\0\0'
        win32com.shell.shell.SHFileOperation((None, ope, paths, None, flags, None, None))
    else:
        for path in paths:
            if os.path.isfile(path):
                os.remove(path)
            elif os.path.isdir(path):
                shutil.rmtree(path)


def remove_emptydir(dpath: str) -> None:
    """
    dpathが中身の無いディレクトリであれば削除する。
    """
    cw.fsync.sync()
    if os.path.isdir(dpath):
        for dpath2, dnames, fnames in os.walk(dpath):
            if len(fnames):
                # 中身が存在する
                return
        remove(dpath)


OVERWRITE_ALWAYS = 0
NO_OVERWRITE = 1
OVERWRITE_WITH_LATEST_FILES = 2


def copytree_overwrite(src: str, dst: str, files_overwrite: int = OVERWRITE_ALWAYS) -> None:
    """
    ディレクトリを上書きコピーないし統合する。
    files_overwrite=Falseの時は同一のファイルを上書きしない。
    """
    cw.fsync.sync()
    if not os.path.isdir(dst):
        os.makedirs(dst)
    for dpath, dnames, fnames in os.walk(src):
        rel = relpath(dpath, src)
        for dname in dnames:
            src2 = join_paths(dpath, dname)
            dst2 = join_paths(dst, rel, dname)
            copytree_overwrite(src2, dst2, files_overwrite=files_overwrite)
        for fname in fnames:
            src2 = join_paths(dpath, fname)
            dst2 = join_paths(dst, rel, fname)
            if files_overwrite == OVERWRITE_ALWAYS:
                shutil.copy2(src2, dst2)
            else:
                if not os.path.isfile(dst2):
                    shutil.copy2(src2, dst2)
                elif files_overwrite == NO_OVERWRITE:
                    pass
                elif files_overwrite == OVERWRITE_WITH_LATEST_FILES:
                    msrc = os.path.getmtime(src2)
                    mdst = os.path.getmtime(dst2)
                    if mdst < msrc:
                        shutil.copy2(src2, dst2)
                else:
                    shutil.copy2(src2, dst2)


# ------------------------------------------------------------------------------
# スレッド関係
# ------------------------------------------------------------------------------

_SyncLock = TypeVar("_SyncLock", bound=Callable[..., typing.Any])


def synclock(lock: threading.Lock) -> Callable[[_SyncLock], _SyncLock]:
    """
    @synclock(_lock)
    def function():
        ...
    のように、ロックオブジェクトを指定して
    特定関数・メソッドの排他制御を行う。
    """

    def synclock(f: _SyncLock) -> _SyncLock:
        def acquire(*args: typing.Any, **kwargs: typing.Any) -> typing.Any:
            lock.acquire()
            try:
                return f(*args, **kwargs)
            finally:
                lock.release()

        # FIXME: error: Incompatible return value type (got "Callable[[VarArg(Any), KwArg(Any)], Any]",
        #        expected "_SyncLock")
        # return acquire
        return typing.cast(_SyncLock, acquire)

    return synclock


# ------------------------------------------------------------------------------
# ZIPファイル関連
# ------------------------------------------------------------------------------

class _LhafileWrapper(lhafile.Lhafile):
    f: BinaryIO

    def __init__(self, path: str, mode: str) -> None:
        # 十六進数のファイルサイズを表す文字列+Windows改行コードが
        # 冒頭に入っていることがある。
        # その場合は末尾にも余計なデータもあるため、冒頭で指定された
        # サイズにファイルを切り詰めなくてはならない。
        f: BinaryIO = open(path, "rb")
        b = f.read(1)
        seq = []
        while b in (b"0123456789abcdefABCDEF"):
            seq.append(b)
            b = f.read(1)
        if seq and b == b'\r' and f.read(1) == b'\n':
            strnum = b"".join(seq)
            num = int(strnum, 16)
            data = f.read(num)
            f.close()
            f = io.BytesIO(data)
            lhafile.Lhafile.__init__(self, f)
            self.f = f
        else:
            f.seek(0)
            lhafile.Lhafile.__init__(self, f)
            self.f = f

    def __enter__(self) -> "_LhafileWrapper":
        return self

    def __exit__(self, exc_type: Optional[Type[BaseException]], exc_value: Optional[BaseException],
                 traceback: Optional[types.TracebackType]) -> Literal[False]:
        self.close()
        return False

    def close(self) -> None:
        self.f.close()


_zip_mutex = threading.Lock()


@synclock(_zip_mutex)
def zip_file(path: str, mode: Union[Literal['r'], Literal['w'], Literal['x'], Literal['a']]) ->\
        Union[zipfile.ZipFile, _LhafileWrapper]:
    """zipfile.ZipFileのインスタンスを生成する。
    FIXME: Python 2.7のzipfile.ZipFileはアーカイブ内の
    ファイル名にあるディレクトリセパレータを'/'に置換してしまうため、
    「ソ」などのいわゆるShift JISの0x5C問題に引っかかって
    正しいファイル名が得られなくなってしまう。
    まったくスレッドセーフではない悪い方法だが、
    それを回避するには一時的にos.sepを'/'にして凌ぐしかない。"""
    if path.lower().endswith(".lzh"):
        if lhafile:
            return _LhafileWrapper(path, mode)
        else:
            raise Exception("Module lhafile is not found.")
    else:
        sep = os.sep
        os.sep = "/"
        try:
            return zipfile.ZipFile(path, mode)
        finally:
            os.sep = sep


@synclock(_zip_mutex)
def compress_zip(path: str, zpath: str, unicodefilename: bool = False) -> str:
    """pathのデータをzpathで指定したzipファイルに圧縮する。
    path: 圧縮するディレクトリパス
    """
    cw.fsync.sync()
    dpath = os.path.dirname(zpath)

    if dpath and not os.path.isdir(dpath):
        os.makedirs(dpath)

    z = zipfile.ZipFile(zpath, "w", zipfile.ZIP_DEFLATED)
    rpl_dir = path + "/"

    for dpath, dnames, fnames in os.walk(str(path)):
        for dname in dnames:
            fpath = join_paths(dpath, dname)
            if os.path.isdir(fpath):
                mtime = time.localtime(os.path.getmtime(fpath))[:6]
                zname = fpath.replace(rpl_dir, "", 1) + "/"
                zinfo = zipfile.ZipInfo(zname, mtime)
                if unicodefilename:
                    zinfo.flag_bits |= 0x800
                z.writestr(zinfo, "")

        for fname in fnames:
            fpath = join_paths(dpath, fname)
            if os.path.isfile(fpath):
                zname = fpath.replace(rpl_dir, "", 1)
                if unicodefilename:
                    z.write(fpath, zname)
                else:
                    raise ValueError("Unsupported not Unicode File Name.")

    z.close()
    return zpath


def decompress_zip(path: str, dstdir: str, dname: str = "", startup: Optional[Callable[[int], None]] = None,
                   progress: Optional[Callable[[int], bool]] = None, overwrite: bool = False,
                   z: Optional[zipfile.ZipFile] = None) -> str:
    """zipファイルをdstdirに解凍する。
    解凍したディレクトリのpathを返す。
    """
    cw.fsync.sync()
    if not z:
        try:
            z = zip_file(path, "r")
        except Exception:
            print_ex()
            return ""
    assert z

    if not dname:
        dname = splitext(os.path.basename(path))[0]

    if overwrite:
        paths = set()
    else:
        dstdir = join_paths(dstdir, dname)
        dstdir = dupcheck_plus(dstdir, False)

    seq = z.infolist()
    if startup:
        startup(len(seq))
    for i, (zname, info) in enumerate(zip(z.namelist(), z.infolist())):
        if progress and i % 10 == 0:
            if progress(i):
                if overwrite:
                    break
                else:
                    z.close()
                    remove(dstdir)
                    return ""
        name = decode_zipfilename(zname, info)
        normpath = os.path.normpath(name)
        if os.path.isabs(normpath):
            continue
        if normpath == ".." or normpath.startswith(".." + os.path.sep):
            continue

        if name.endswith("/"):
            name = name.rstrip("/")
            dpath = join_paths(dstdir, name)

            if dpath and not os.path.isdir(dpath):
                os.makedirs(dpath)

        else:
            fpath = join_paths(dstdir, name)
            dpath = os.path.dirname(fpath)

            if dpath and not os.path.isdir(dpath):
                os.makedirs(dpath)

            if isinstance(info.date_time, datetime.datetime):
                mtime = info.date_time.timestamp()
            else:
                mtime = datetime.datetime(info.date_time[0], info.date_time[1], info.date_time[2],
                                          info.date_time[3], info.date_time[4], info.date_time[5]).timestamp()

            if overwrite:
                # 上書き展開時は一部ファイルでエラーが出た場合に
                # 上書き先を改名して対処する
                # (再生中のBGMが上書きできない場合など)
                paths.add(get_keypath(fpath))
                if not os.path.isfile(fpath) or os.path.getmtime(fpath) != mtime:
                    data = z.read(zname)
                    try:
                        with open(fpath, "wb") as f:
                            f.write(data)
                            f.flush()
                            os.fsync(f.fileno())
                            f.close()
                    except Exception:
                        # 改名してリトライ
                        if os.path.isfile(fpath):
                            dst = join_paths("Data/Temp/Global/Deleted", os.path.basename(fpath))
                            dst = dupcheck_plus(dst, False)
                            if not os.path.isdir("Data/Temp/Global/Deleted"):
                                os.makedirs("Data/Temp/Global/Deleted")
                            rename_file(fpath, dst)
                        with open(fpath, "wb") as f:
                            f.write(data)
                            f.flush()
                            os.fsync(f.fileno())
                            f.close()
                else:
                    continue
            else:
                try:
                    data = z.read(zname)
                    with open(fpath, "wb") as f:
                        f.write(data)
                        f.flush()
                        f.close()
                except Exception:
                    if isinstance(info, lhafile.LhaInfo) and info.compress_type == b"-lhd-":
                        continue
                    raise

            os.utime(fpath, (os.path.getatime(fpath), mtime))

    z.close()

    if overwrite:
        for dpath, _dnames, fnames in os.walk(dstdir):
            for fname in fnames:
                path = join_paths(dpath, fname)
                path = get_keypath(path)
                if path not in paths:
                    remove(path)

    if progress:
        progress(len(seq))

    return dstdir


def decode_zipfilename(zname: str, info: Union[zipfile.ZipInfo, lhafile.LhaInfo]) -> str:
    """ZipFileないしLhaFileのファイル名をデコードする。"""
    if isinstance(info, zipfile.ZipInfo):
        if not (info.flag_bits & 0x800):
            return decode_zipname(zname.encode("cp437"))
        else:
            return zname
    else:
        return decode_zipname(zname.encode("ISO-8859-1"))


def decode_zipname(name: Union[str, bytes]) -> str:
    if not isinstance(name, str):
        try:
            r_name = str(name, "utf_8_sig")
        except UnicodeDecodeError:
            try:
                r_name = str(name, cw.MBCS)
            except UnicodeDecodeError:
                try:
                    r_name = str(name, "euc-jp")
                except UnicodeDecodeError:
                    try:
                        r_name = str(name, "utf-8")
                    except UnicodeDecodeError:
                        try:
                            r_name = str(name, "utf-16")
                        except UnicodeDecodeError:
                            try:
                                r_name = str(name, "utf-32")
                            except UnicodeDecodeError:
                                r_name = str(name, "cp437")
        return r_name

    return name


def decode_text(name: bytes) -> str:
    try:
        r_name = str(name, "utf_8_sig")
    except UnicodeDecodeError:
        try:
            r_name = str(name, "utf-8")
        except UnicodeDecodeError:
            try:
                r_name = str(name, "shift_jis")
            except UnicodeDecodeError:
                try:
                    r_name = str(name, "utf-16")
                except UnicodeDecodeError:
                    try:
                        r_name = str(name, "utf-32")
                    except UnicodeDecodeError:
                        try:
                            r_name = str(name, cw.MBCS)
                        except UnicodeDecodeError:
                            try:
                                r_name = str(name, "euc-jp")
                            except UnicodeDecodeError:
                                r_name = str(name, "cp437")
    return r_name


def read_zipdata(zfile: zipfile.ZipFile, name: str) -> bytes:
    try:
        data = zfile.read(name)
    except KeyError:
        print_ex()
        data = b""
    return data


def get_elementfromzip(zpath: str, name: str, tag: str = "") -> "cw.data.CWPyElement":
    with zip_file(zpath, "r") as z:
        data = read_zipdata(z, name)
        z.close()
    f = io.BytesIO(data)
    try:
        element = cw.data.xml2element(name, tag, stream=f)
    finally:
        f.close()
    return element


def decompress_cab(path: str, dstdir: str, dname: str = "", startup: Optional[Callable[[int], None]] = None,
                   progress: Optional[Callable[[int], bool]] = None,
                   overwrite: bool = False) -> str:
    """cabファイルをdstdirに解凍する。
    解凍したディレクトリのpathを返す。
    """
    cw.fsync.sync()
    if not dname:
        dname = splitext(os.path.basename(path))[0]

    if not overwrite:
        dstdir = join_paths(dstdir, dname)
        dstdir = dupcheck_plus(dstdir, False)

    if overwrite and os.path.isdir(dstdir):
        # 強制的に全てのファイルを展開するため、
        # 元々あったファイルを削除するか、削除予定地へ転送する
        for dpath, _dnames, fnames in os.walk(dstdir):
            for fname in fnames:
                fpath = join_paths(dpath, fname)
                dst = join_paths("Data/Temp/Global/Deleted", fname)
                if not os.path.isdir("Data/Temp/Global/Deleted"):
                    os.makedirs("Data/Temp/Global/Deleted")
                dst = dupcheck_plus(dst)
                rename_file(fpath, dst)
                remove(dst)

    if startup or progress:
        filenum = cab_filenum(path)

    if startup:
        startup(filenum)

    try:
        if not os.path.isdir(dstdir):
            os.makedirs(dstdir)
        ss = []
        if sys.platform == "win32" and sys.getwindowsversion().major <= 5:
            # バージョン5以前の`expand.exe`は`-f:*`でディレクトリ構造を無視してしまう
            for dname in cab_dpaths(path):
                if not dname:
                    continue
                dstdir2 = cw.util.join_paths(dstdir, dname)
                if not os.path.isdir(dstdir2):
                    os.makedirs(dstdir2)
                ss.append("expand \"%s\" -f:\"%s\\*\" \"%s\"" % (path, dname, dstdir2))
            ss.append("expand \"%s\" -f:\"*\" \"%s\"" % (path, dstdir))
        else:
            ss.append("expand \"%s\" -f:* \"%s\"" % (path, dstdir))
        if progress:
            class Progress(object):
                def __init__(self) -> None:
                    self.result: Optional[str] = None
                    self.cancel = False

                def run(self) -> None:
                    for s in ss:
                        p = subprocess.Popen(s, shell=True, close_fds=True)
                        r = p.poll()
                        while r is None:
                            if self.cancel:
                                p.kill()
                            time.sleep(0.001)
                            r = p.poll()
                        if r != 0:
                            return  # 失敗
                    self.result = dstdir

            prog = Progress()
            thr = threading.Thread(target=prog.run)
            thr.start()
            count = 0
            while thr.is_alive():
                # ファイル数カウント
                last_count = count
                count = 0
                for dpath, _dnames, fnames in os.walk(dstdir):
                    count += len(fnames)
                if last_count != count:
                    if progress(count):
                        prog.cancel = True
                p = time.process_time() + 0.1
                while thr.is_alive() and time.process_time() < p:
                    time.sleep(0.001)
            if prog.cancel and not overwrite:
                remove(dstdir)
                return ""
        else:
            for s in ss:
                if subprocess.call(s, shell=True, close_fds=True) != 0:
                    return ""
    except Exception:
        cw.util.print_ex()
        return ""

    if progress:
        progress(filenum)

    return dstdir


def cab_filenum(cab: str) -> int:
    """CABアーカイブに含まれるファイル数を返す。"""
    word = struct.Struct("<h")
    try:
        with io.BufferedReader(io.FileIO(cab, "rb")) as f:
            # ヘッダ
            buf = f.read(36)
            f.close()
            if buf[:4] != b"MSCF":
                return 0

            cfiles: int = word.unpack(buf[28:30])[0]
            return cfiles
    except Exception:
        cw.util.print_ex()
    return 0


def cab_hasfile(cab: str, fname: Union[str, Iterable[str]]) -> str:
    """CABアーカイブに指定された名前のファイルが含まれているか判定する。"""
    if not os.path.isfile(cab):
        return ""

    dword = struct.Struct("<l")
    word = struct.Struct("<h")
    if isinstance(fname, str):
        fname = os.path.normcase(fname)
    else:
        s = set()
        for name in fname:
            s.add(os.path.normcase(name))
        fname = s

    encoding = "cp932"
    try:
        with io.BufferedReader(io.FileIO(cab, "rb")) as f:
            # ヘッダ
            buf = f.read(36)
            if buf[:4] != b"MSCF":
                f.close()
                return ""

            cofffiles = dword.unpack(buf[16:20])[0]
            cfiles = word.unpack(buf[28:30])[0]
            f.seek(cofffiles)

            for _i in range(cfiles):
                buf = f.read(16)
                attribs = word.unpack(buf[14:16])[0]
                seq = []
                while True:
                    c = f.read(1)
                    if c == b'\0':
                        break
                    seq.append(c)
                b_name = b"".join(seq)
                _A_NAME_IS_UTF = 0x80
                if attribs & _A_NAME_IS_UTF:
                    name = str(b_name, "utf-8")
                else:
                    name = str(b_name, encoding)
                if isinstance(fname, str):
                    if fname == os.path.normcase(os.path.basename(name)):
                        f.close()
                        return name
                else:
                    if os.path.normcase(os.path.basename(name)) in fname:
                        f.close()
                        return name
            f.close()
    except Exception:
        cw.util.print_ex()
    return ""


def cab_dpaths(cab: str) -> Set[str]:
    """CABアーカイブ内のディレクトリのsetを返す。"""
    r: Set[str] = set()
    if not os.path.isfile(cab):
        return r

    dword = struct.Struct("<l")
    word = struct.Struct("<h")

    encoding = "cp932"
    try:
        with io.BufferedReader(io.FileIO(cab, "rb")) as f:
            # ヘッダ
            buf = f.read(36)
            if buf[:4] != b"MSCF":
                f.close()
                return r

            cofffiles = dword.unpack(buf[16:20])[0]
            cfiles = word.unpack(buf[28:30])[0]
            f.seek(cofffiles)

            for _i in range(cfiles):
                buf = f.read(16)
                attribs = word.unpack(buf[14:16])[0]
                seq = []
                while True:
                    c = f.read(1)
                    if c == b'\0':
                        break
                    seq.append(c)
                b_name = b"".join(seq)
                _A_NAME_IS_UTF = 0x80
                if attribs & _A_NAME_IS_UTF:
                    name = str(b_name, "utf-8")
                else:
                    name = str(b_name, encoding)
                i = name.rfind("\\")
                if i == -1:
                    r.add("")
                else:
                    dname = name[:i]
                    r.add(dname)
            f.close()
    except Exception:
        cw.util.print_ex()
    return r


def cab_scdir(cab: str) -> str:
    """CABアーカイブ内でSummary.wsmまたは
    Summary.xmlが含まれるフォルダを返す。
    """
    fpath = cab_hasfile(cab, ("Summary.xml", "Summary.wsm"))
    return os.path.dirname(fpath)


# ------------------------------------------------------------------------------
# テキスト操作関連
# ------------------------------------------------------------------------------

def encodewrap(s: str) -> str:
    """改行コードを\nに置換する。"""
    r = []
    if not s:
        return ""
    for c in s:
        if c == '\\':
            r.append("\\\\")
        elif c == '\n':
            r.append("\\n")
        elif c == '\r':
            pass
        else:
            r.append(c)
    return "".join(r)


def decodewrap(s: str, code: str = "\n") -> str:
    """\nを改行コードに戻す。"""
    if not s:
        return ""
    r = []
    bs = False
    for c in s:
        if bs:
            if c == 'n':
                r.append(code)
            elif c == '\\':
                r.append('\\')
            else:
                r.append(c)
            bs = False
        elif c == '\\':
            bs = True
        else:
            r.append(c)
    return "".join(r)


def encodetextlist(arr: List[str]) -> str:
    """arrを\n区切りの文字列にする。"""
    return encodewrap("\n".join(arr))


def decodetextlist(s: str) -> List[str]:
    """\n区切りの文字列を文字配列にする。"""
    if not s:
        return []
    return decodewrap(s).split("\n")


def _is_hw(c: str) -> bool:
    """unichrが半角文字であればTrueを返す。"""
    return not unicodedata.east_asian_width(c) in ('F', 'W', 'A')


def get_strlen(s: str) -> int:
    def ln(c: str) -> int:
        if not c or unicodedata.combining(c):
            return 0
        elif _is_hw(c):
            return 1
        else:
            return 2
    return reduce(lambda a, b: a + b, [ln(c) for c in s])


assert get_strlen("#") == 1
assert get_strlen("##") == 2
assert get_strlen("#あ") == 3
assert get_strlen("#あ゙") == 3


def slice_str(s: str, width: int, get_width: Optional[Callable[[str], int]] = None) -> Tuple[str, str]:
    """
    sをwidthの位置でスライスし、2つの文字列にして返す。
    """
    s = str(s)
    if not get_width:
        get_width = get_strlen
    left = []
    leftlen = 0
    for c in s:
        clen = get_width(c)
        if width < leftlen + clen:
            break
        left.append(c)
        leftlen += clen
    return "".join(left), s[len(left):]


assert slice_str("ABC", 2) == ("AB", "C")
assert slice_str("ABCあ", 4) == ("ABC", "あ")


def rjustify(s: str, length: int, c: str) -> str:
    slen = cw.util.get_strlen(s)
    if slen < length:
        s += c * (length - slen)
    return s


def ljustify(s: str, length: int, c: str) -> str:
    slen = cw.util.get_strlen(s)
    if slen < length:
        s = (c * (length - slen)) + s
    return s


WRAPS_CHARS = "｡|､|，|、|。|．|）|」|』|〕|｝|】"


def txtwrap(s: str, mode: int, width: int = 30, wrapschars: str = "", encodedtext: bool = True,
            spcharinfo: Optional[Set[int]] = None) -> str:
    """引数の文字列を任意の文字数で改行する(全角は2文字として数える)。
    mode=1: カード解説。
    mode=2: 画像付きメッセージ(台詞)用。
    mode=3: 画像なしメッセージ用。
    mode=4: キャラクタ情報ダイアログの解説文・張り紙説明用。
    mode=5: 素質解説文用。
    mode=6: メッセージダイアログ用。
    """
    if mode == 1:
        wrapschars = WRAPS_CHARS
        width = 37
    elif mode == 2:
        wrapschars = ""
        width = 32
    elif mode == 3:
        wrapschars = ""
        width = 42
    elif mode == 4:
        wrapschars = WRAPS_CHARS
        width = 37
    elif mode == 5:
        wrapschars = WRAPS_CHARS
        width = 24
    elif mode == 6:
        wrapschars = WRAPS_CHARS
        width = 48

    if encodedtext:
        # \\nを改行コードに戻す
        s = cw.util.decodewrap(s)
    # 行頭禁止文字集合
    r_wchar = re.compile(wrapschars) if mode not in (2, 3) and wrapschars else None
    # 特殊文字記号集合
    re_color = "&[\x20-\x7E]"
    r_spchar = re.compile("#.|" + re_color) if mode in (2, 3) else None
    if spcharinfo is not None:
        spcharinfo2: List[int] = []
    cnt = 0
    asciicnt = 0
    wraped = False
    skip = False
    spchar = False
    defspchar = False
    wrapafter = False
    seq: List[str] = []
    seqlen = 0
    skipchars = ""

    def seq_insert(index: int, char: str) -> None:
        if index < 0:
            index = len(seq) + index
        seq.insert(index, char)
        if spcharinfo is not None:
            for i in reversed(range(len(spcharinfo2))):
                spi = spcharinfo2[i]
                if spi < index:
                    break
                else:
                    spcharinfo2[i] += len(char)

    def insert_wrap(index: int) -> None:
        # 折り返しを追加
        seq_insert(index, '\n')
        if spcharinfo is not None:
            # 折り返しが追加された位置を記憶しておく
            if index < 0:
                index = len(seq) + index
            if index == len(seq):
                spcharinfo2.append(seqlen)
            else:
                spcharinfo2.append(reduce(lambda l, s: l + len(s), seq[:index], 0))

    index = 0
    for char in graphemes(s):
        spchar2 = spchar
        spchar = False
        width2 = width
        wrapafter2 = wrapafter
        defspchar2 = defspchar
        defspchar = False

        if r_spchar and not defspchar2:
            if skip:
                if spcharinfo is not None and index in spcharinfo:
                    spcharinfo2.append(seqlen)
                seq.append(char)
                seqlen += len(char)
                skip = False
                if skipchars.startswith("#"):
                    cnt += len(char)
                    asciicnt = 0
                    if width + 1 < cnt:
                        if not wrapafter:
                            insert_wrap(len(seq))
                            seqlen += len("\n")
                        cnt = 0
                        asciicnt = 0
                        wraped = False
                        wrapafter = True
                index += len(char)
                continue

            chars = char + get_char(s, index + 1)

            if r_spchar.match(chars.lower()):
                if spcharinfo is not None and index in spcharinfo:
                    spcharinfo2.append(seqlen)
                    if not chars.startswith("#") or \
                            not chars[:2].lower() in cw.cwpy.rsrc.specialchars or \
                            width < cnt:
                        if width < cnt and chars.startswith("#"):
                            if not wrapafter:
                                insert_wrap(len(seq))
                                seqlen += len("\n")
                            cnt = 0
                            asciicnt = 0
                        seq.append(char)
                        seqlen += len(char)
                        skip = True
                        if chars.startswith("#"):
                            cnt += len(char)
                        skipchars = chars
                        index += len(char)
                        continue
                    spchar = True
                    if not chars.startswith("&"):
                        wrapafter = False
                        defspchar = True

        # 行頭禁止文字
        if cnt == 0 and not wraped and r_wchar and r_wchar.match(char):
            seq_insert(-1, char)
            seqlen += len(char)
            asciicnt = 0
            wraped = True
        # 改行記号
        elif char == "\n":
            if not wrapafter:
                seq.append(char)
                seqlen += len(char)
            cnt = 0
            asciicnt = 0
            wraped = False
            wrapafter = False
        # 半角文字
        elif get_strlen(char) == 1:
            seq.append(char)
            seqlen += len(char)
            cnt += 1
            if not (mode in (2, 3) or (mode in (1, 4) and char == ' ')) and \
                    not (mode == 1 and index + 1 < len(s) and not get_strlen(s[index + 1]) == 1):
                asciicnt += 1
            if spchar2 or not (mode in (2, 3) or (mode in (1, 4) and char == ' ')) or \
                    len(s) <= index + 1 or get_strlen(s[index + 1]) == 1:
                width2 += 1
            wrapafter = False

        # 行頭禁止文字・改行記号・半角文字以外
        else:
            seq.append(char)
            seqlen += len(char)
            cnt += 2
            asciicnt = 0
            wrapafter = False
            if mode in (1, 2, 3) and index + 1 < len(s) and get_strlen(s[index + 1]) == 1:
                width2 += 1

        # 互換動作: 1.28以降は行末に半角スペースがあると折り返し位置が変わる
        #           (イベントによるメッセージのみ)
        if mode in (3, 4) or cw.cwpy.sdata and not cw.cwpy.sct.lessthan("1.20", cw.cwpy.sdata.get_versionhint()):
            if not wrapafter2 and index + 1 < len(s) and s[index + 1] == " " and mode in (1, 2, 3, 4):
                width2 += 1
                asciicnt = 0

        # 行折り返し処理
        if not spchar and cnt > width2:
            if defspchar2 and width2 + 1 < cnt:
                index = -(cnt - (width + 1))
                if seq[-index] != "\n":
                    insert_wrap(index)
                    seqlen += len("\n")
                cnt = 1
            elif width2 >= asciicnt > 0 and not defspchar2:
                if not get_char(s, index + 1) == "\n" and seq[-asciicnt] != "\n":
                    insert_wrap(-asciicnt)
                    seqlen += len("\n")
                cnt = asciicnt
            elif index + 1 <= len(s) or not get_char(s, index + 1) == "\n":
                if index + 2 <= len(s) or not get_char(s, index + 2) == "\n":
                    insert_wrap(len(seq))
                    seqlen += len("\n")
                    wrapafter = True
                cnt = 0
                asciicnt = 0
                wraped = False
        index += len(char)

    if spcharinfo is not None:
        spcharinfo.clear()
        spcharinfo.update(spcharinfo2)

    return "".join(seq).rstrip()


def graphemes(s: str) -> Sequence[str]:
    arr: List[str] = []
    for chars in grapheme.graphemes(s):
        if len(chars) == 2 and chars[1] in ('ﾞ', 'ﾟ'):
            arr.append(chars[0])
            arr.append(chars[1])
        else:
            arr.append(chars)
    return arr


def _wordwrap_impl(s: str, width: int, get_width: Optional[Callable[[str], int]], open_chars: str, close_chars: str,
                   startindex: int, resultindex: int, spcharinfo: Optional[Set[int]],
                   spcharinfo2: Optional[List[int]]) -> str:
    """
    sをwidthの幅で折り返す。
    テキストの長さをは計る時にget_width(s)を使用する。
    """
    s = str(s)
    if not get_width:
        get_width = get_strlen

    iterwords: List[str] = regex.findall(r"(?>[a-z0-9_]\p{M}*)+|(?>[ａ-ｚＡ-Ｚ０-９＿]\p{M}*)+|.\p{M}*", s,
                                         regex.IGNORECASE)
    if spcharinfo is not None:
        # 特殊文字と単語を分離しておく
        iter2: List[str] = []
        index = startindex
        spc = None
        for word in iterwords:
            if spc:
                iter2.append(spc + word[0])
                if 1 < len(word):
                    iter2.append(word[1:])
                spc = None
            elif index in spcharinfo is not None:
                spc = word
            else:
                iter2.append(word)
            index += len(word)
        assert spc is None
        iterwords = iter2

    lines = []
    buf: List[Tuple[str, bool]] = []
    buflen = 0
    hw = get_width("#")
    index = startindex
    for word in iterwords:
        # 特殊文字か？
        is_spchar = spcharinfo is not None and index in spcharinfo

        wordlen = get_width(word)
        if width < buflen + wordlen:
            def match_op(buf: Tuple[str, bool]) -> bool:
                return not buf[1] and open_chars.find(buf[0]) != -1

            def match_cl(buf: Tuple[str, bool]) -> bool:
                return not buf[1] and close_chars.find(buf[0]) != -1

            def match_last(bufs: Sequence[Tuple[str, bool]], matcher: Callable[[Tuple[str, bool]], bool]) -> bool:
                for i in range(len(bufs)):
                    buf = bufs[-(1 + i)]
                    if buf[1] and buf[0][0] == '&':
                        continue
                    return matcher(buf)
                return False

            def match_op_last(bufs: Sequence[Tuple[str, bool]]) -> bool:
                # bufsの末尾部分がopen_charsに該当する文字ならTrue
                # ただし色変更の特殊文字は無視する
                return match_last(bufs, match_op)

            def match_cl_last(bufs: Sequence[Tuple[str, bool]]) -> bool:
                # bufsの末尾部分がclose_charsに該当する文字ならTrue
                # ただし色変更の特殊文字は無視する
                return match_last(bufs, match_cl)

            assert match_op_last([("[", False), ("&R", True), ("&R", True)])
            assert match_op_last([("[", False)])
            assert not match_op_last([("[", False), ("&R", False), ("&R", True)])

            def append_word_wrap(buf: List[Tuple[str, bool]], buflen: int,
                                 word: str) -> Tuple[List[Tuple[str, bool]], int, str]:
                # wordを強制的に折り返しながら行に加える
                assert get_width
                if is_spchar:
                    return buf, buflen, word
                while width < buflen + get_width(word):
                    word2, word3 = slice_str(word, width - buflen, get_width)
                    if word2:
                        word2 += "-"
                    buf.append((word2, False))
                    word = word3
                    lines.append(buf)
                    buf = []
                    buflen = 0
                return [], 0, word

            def break_before_openchar(buf2: Sequence[Tuple[str, bool]], buf: List[Tuple[str, bool]], buflen: int,
                                      word: str) -> Tuple[List[Tuple[str, bool]], int, str]:
                # 行末禁止文字の位置まで遡って折り返す
                assert get_width
                while buf2 and match_op_last(buf2):
                    buf2 = buf2[:-1]
                if buf2:
                    i = len(buf2)
                    lines.append(buf[:i])
                    buf = buf[i:]
                    buflen = sum([get_width(s[0]) for s in buf])
                    return buf, buflen, word
                else:
                    return append_word_wrap(buf, buflen, word)

            if 1 <= len(buf) and match_op_last(buf) and not match_cl_last(buf):
                # 末尾に行末禁止文字があるので折り返し可能な位置まで遡って折り返す
                buf, buflen, word = break_before_openchar(buf, buf, buflen, word)
                wordlen = get_width(word)
            elif not str.isspace(word):
                # 空白文字は行末にいくつでも連ねるのでそれ以外の文字を処理
                if match_cl((word, is_spchar)):
                    if width < buflen or (width == buflen and hw < wordlen):
                        # 行頭禁止文字は1文字まではぶら下げるが、それ以上ある場合は
                        # 折り返し可能な位置まで遡って折り返す
                        buf2 = buf
                        while buf2 and match_cl_last(buf2):
                            buf2 = buf2[:-1]
                        if not buf2 or (len(buf2) == 1 and not match_op_last(buf2)):
                            # 折り返し可能な位置が無かった
                            lines.append(buf)
                            buf = []
                            buflen = 0
                        elif 2 <= len(buf2) and not match_op_last(buf2[:-1]):
                            # 折り返し可能な位置が見つかった(折り返した箇所に行末禁止文字が無い)
                            i = len(buf2) - 1
                            lines.append(buf[:i])
                            buf = buf[i:]
                            buflen = sum([get_width(s[0]) for s in buf])
                        else:
                            # 折り返し可能な位置は行末禁止文字だった
                            buf, buflen, word = break_before_openchar(buf2, buf, buflen, word)
                            wordlen = get_width(word)
                else:
                    # 普通に折り返す
                    if buf:
                        lines.append(buf)
                        buf = []
                        buflen = 0
                    buf, buflen, word = append_word_wrap(buf, buflen, word)
                    wordlen = get_width(word)
        if word:
            buf.append((word, is_spchar))
            if not is_spchar or word[0] != '&':
                buflen += wordlen
        index += len(word)

    if buf:
        lines.append(buf)

    if spcharinfo2 is None:
        return "\n".join(["".join([w[0] for w in buf]) for buf in lines])
    else:
        seq = []
        for i, buf in enumerate(lines):
            line = []
            for word, is_spchar in buf:
                if is_spchar:
                    spcharinfo2.append(resultindex)
                line.append(word)
                resultindex += len(word)
            seq.append("".join(line))
            if i + 1 < len(lines):
                spcharinfo2.append(resultindex)  # 折り返し位置を記録
            resultindex += len("\n")
        return "\n".join(seq)


def wordwrap(s: str, width: int, get_width: Optional[Callable[[str], int]] = None,
             open_chars: str = "\"'(<[`{‘“〈《≪「『【〔（＜［｛｢",
             close_chars: str = "!\"'),.:;>?]`}゜’”′″、。々＞》≫」』】〕〟゛°ゝゞヽヾ〻！），．：；＞？］｝｡｣､ﾞﾟ"
                                "ぁぃぅぇぉァィゥェォｧｨｩｪｫヵっッｯゃゅょャュョｬｭｮゎヮㇵㇶㇷㇸㇹㇺ…―ーｰ",
             spcharinfo: Optional[Set[int]] = None) -> str:
    if spcharinfo is not None:
        spcharinfo2: Optional[List[int]] = []
    else:
        spcharinfo2 = None
    lines = []
    index = 0
    resultindex = 0
    for line in s.splitlines():
        wrapped = _wordwrap_impl(line, width, get_width, open_chars, close_chars, index, resultindex, spcharinfo,
                                 spcharinfo2)
        lines.append(wrapped)
        index += len(line) + len("\n")
        resultindex += len(wrapped) + len("\n")

    if spcharinfo is not None:
        assert spcharinfo2 is not None
        spcharinfo.clear()
        spcharinfo.update(spcharinfo2)

    return "\n".join(lines)


assert wordwrap("ABC.DEFG.H,IKLM?", 3) == "ABC.\nDEF-\nG.H,\nIKL-\nM?"
assert wordwrap("[abc..]\ndefg", 3) == "[ab-\nc..]\ndef-\ng"
assert wordwrap("abc..\ndefghij", 3) == "abc.\n.\ndef-\nghi-\nj"
assert wordwrap("a bc..", 4) == "a \nbc.."
assert wordwrap("a bc....],.\ndef", 4) == "a \nbc...\n.],.\ndef"
assert wordwrap("[def]", 4) == "[def]"
assert wordwrap("def[ghi]]", 4) == "def\n[ghi\n]]"
assert wordwrap("あいうえお。かきくけこ", 11) == "あいうえお。\nかきくけこ"
assert wordwrap("あいうえAA。かきくけこ", 9) == "あいうえ\nAA。かき\nくけこ"
assert wordwrap("[[[[a", 4) == "[[[[\na"
assert wordwrap("\"Let's it go!!\"", 4) == "\"Let'\ns it \ngo!!\""
assert wordwrap("あいうえおA.かきくけこ", 11) == "あいうえおA.\nかきくけこ"
assert wordwrap("あいうえおA。かきくけこ", 11) == "あいうえお\nA。かきくけ\nこ"
assert wordwrap("ｐｑｒ pqr ＰＱＲ", 6) == "ｐｑｒ \npqr \nＰＱＲ"
assert wordwrap("あ゙い゙ゔえ゙お゙か゚き゚く゚け゚こ゚", 6) == "あ゙い゙ゔ\nえ゙お゙か゚\nき゚く゚け゚\nこ゚"


def _test_wordwrap(s: str, width: int, spcharinfo: Set[int]) -> Tuple[str, Set[int]]:
    return wordwrap(s, width, spcharinfo=spcharinfo), spcharinfo


assert _test_wordwrap("CARD #WIRTH SPECIA&L\nCHA&RACTER #TEST!", 8, spcharinfo={5, 18, 24, 32}) == \
       ("CARD #W\nIRTH \nSPECIA&L\nCHA&RACTER \n#TEST!", {5, 7, 13, 20, 26, 34, 35})
assert _test_wordwrap("wordwrap", 4, spcharinfo=set()) == \
       ("word-\nwrap", {5})

assert wordwrap("[&Rabc..]", 3, spcharinfo={1}) == "[&Rab-\nc..]"
assert wordwrap("ab...", 3) == "ab..\n."
assert _test_wordwrap("ab..&R.", 3, spcharinfo={4}) == ("ab..\n&R.", {4, 5})


def get_char(s: str, index: int) -> str:
    try:
        if 0 <= index and index < len(s):
            return s[index]
        return ""
    except Exception:
        return ""


def format_title(fmt: str, d: Dict[str, str], use_lf: bool = False) -> str:
    """foobar2000の任意フォーマット文字列のような形式で
    文字列の構築を行う。
     * %%で囲われた文字列は変数となり、辞書dから得られる値に置換される。
     * []で囲われた文字列は、その内側で使用された変数がなければ丸ごと無視される。
     * \\の次の文字列は常に通常文字となる。ただし\\nは例外の場合がある。
     * use_lf=Trueの時は、\\n=改行コードとなる。

    例えば次のようになる:
        d = { "application":"CardWirthPy", "skin":"スキン名", "yado":"宿名" }
        s = format_title("%application% %skin%[ - %yado%[ %scenario%]]", d)
        assert s == "CardWirthPy スキン名 - 宿名"
    """

    class _FormatPart(object):
        """フォーマット内の変数。"""
        def __init__(self, name: str) -> None:
            self.name = name

    # BUG: undefined name '_List' Pyflakes (2.2.0)
    # class _List(object):
    #     def __init__(self, seq: List[Union[str, _FormatPart, "_List"]]) -> None:
    #         self.seq = seq

    class _AbsList(object):
        def __init__(self) -> None:
            self.seq: List[Union[str, _FormatPart, _AbsList]] = []

    class _List(_AbsList):
        def __init__(self, seq: List[Union[str, _FormatPart, _AbsList]]) -> None:
            self.seq = seq

    def eat_parts(fmt: str, subsection: bool) -> Tuple[str, List[Union[str, _FormatPart, _AbsList]]]:
        """formatを文字列とFormatPartのリストに分解。
        []で囲われた部分はサブリストとする。
        """
        seq: List[Union[str, _FormatPart, _AbsList]] = []
        bs = False
        while fmt:
            c = fmt[0]
            fmt = fmt[1:]
            if bs:
                if c == 'n' and use_lf:
                    seq.append("\n")
                else:
                    seq.append(c)
                bs = False
            elif c == "\\":
                bs = True
            elif c == "]" and subsection:
                return fmt, seq
            elif c == "%":
                ci = fmt.find("%")
                if ci != -1:
                    seq.append(_FormatPart(fmt[:ci]))
                    fmt = fmt[ci + 1:]
            elif c == "[":
                fmt, list2 = eat_parts(fmt, True)
                seq.append(_List(list2))
            else:
                seq.append(c)
        return fmt, seq

    fmt, sl = eat_parts(fmt, False)
    assert not fmt

    def do_format(secs: Iterable[Union[str, _FormatPart, _AbsList]]) -> Tuple[str, bool]:
        """フォーマットを実行する。"""
        seq = []
        use = False
        for sec in secs:
            if isinstance(sec, _FormatPart):
                name = d.get(sec.name, "")
                if name:
                    use = True
                    seq.append(name)
            elif isinstance(sec, _AbsList):
                text, use2 = do_format(sec.seq)
                if use2:
                    seq.append(text)
                    use = True
            else:
                seq.append(sec)
        return "".join(seq), use

    return do_format(sl)[0]


assert format_title("%application% %skin%[ - %yado%[ %scenario%]]",
                    {"application": "CardWirthPy", "skin": "スキン名", "yado": "宿名"}) == \
       "CardWirthPy スキン名 - 宿名"
assert format_title("\\%\\[\\]\\\\", {}) == "%[]\\"
assert format_title("1\\%2\\[3\\]4\\\\", {}) == "1%2[3]4\\"


# ------------------------------------------------------------------------------
# wx汎用関数
# ------------------------------------------------------------------------------

def load_wxbmp(name: str = "", mask: bool = False, image: wx.Image = None,
               maskpos: Union[Tuple[int, int], str] = (0, 0), f: Optional[BinaryIO] = None, retry: bool = True,
               can_loaded_scaledimage: bool = True,
               noscale: bool = False, up_scr: Optional[Union[int, float]] = None) -> wx.Bitmap:
    """pos(0,0)にある色でマスクしたwxBitmapを返す。"""
    if sys.platform != "win32":
        assert threading.currentThread() != cw.cwpy

    def masked_empty_bitmap() -> wx.Bitmap:
        image = wx.Image(1, 1)
        if image.HasAlpha():
            image.ClearAlpha()
        r = image.GetRed(0, 0)
        g = image.GetGreen(0, 0)
        b = image.GetBlue(0, 0)
        image.SetMaskColour(r, g, b)
        return image.ConvertToBitmap()

    if cw.cwpy and cw.cwpy.rsrc:
        name = cw.cwpy.rsrc.get_filepath(name)

    if not f and (not cw.binary.image.code_to_data(name) and not os.path.isfile(name)) and not image:
        return masked_empty_bitmap()

    if up_scr is None:
        up_scr = cw.UP_SCR  # ゲーム画面と合わせるため、ダイアログなどでも描画サイズのイメージを使用する
    name, up_scr = find_scaledimagepath(name, up_scr, can_loaded_scaledimage, noscale)

    haspngalpha = False
    bmpdepth = 0
    maskcolour = None
    if mask:
        if not image:
            try:
                if f:
                    data = f.read()
                elif cw.binary.image.path_is_code(name):
                    data = cw.binary.image.code_to_data(name)
                else:
                    if not os.path.isfile(name):
                        return masked_empty_bitmap()
                    with open(name, "rb") as f2:
                        data = f2.read()
                        f2.close()

                if not data:
                    return masked_empty_bitmap()

                ext = get_imageext(data)
                if ext == ".png":
                    haspngalpha = cw.image.has_pngalpha(data)
                bmpdepth = cw.image.get_bmpdepth(data)
                data, ok1 = cw.image.fix_cwnext32bitbitmap(data)
                data, ok2 = cw.image.fix_cwnext16bitbitmap(data)
                data, ok3 = cw.image.patch_rle8bitmap(data)
                if name and ok1 and ok2 and ok3 and not cw.binary.image.path_is_code(name):
                    # BUG: io.BytesIO()を用いてのwx.ImageFromStream()は、
                    #      二重にファイルを読む処理よりなお10倍も遅い
                    image = wx.Image(name)
                else:
                    with io.BytesIO(data) as f3:
                        image = wx.Image(f3, wx.BITMAP_TYPE_ANY, -1)
                        f3.close()
            except Exception:
                print_ex()
                print("画像が読み込めません(load_wxbmp)", name)
                return masked_empty_bitmap()

        def set_mask(image: wx.Image, maskpos: Union[Tuple[int, int], str]) -> Tuple[int, int, int]:
            if image.HasAlpha():
                r = image.GetMaskRed()
                g = image.GetMaskGreen()
                b = image.GetMaskBlue()
            else:
                maskpos = convert_maskpos(maskpos, image.Width, image.Height)
                r = image.GetRed(maskpos[0], maskpos[1])
                g = image.GetGreen(maskpos[0], maskpos[1])
                b = image.GetBlue(maskpos[0], maskpos[1])
            image.SetMaskColour(r, g, b)
            return (r, g, b)

        if not image.IsOk():
            return masked_empty_bitmap()

        if not haspngalpha and not image.HasAlpha() and mask:
            maskcolour = set_mask(image, maskpos)

        wxbmp = image.ConvertToBitmap()

        # 255色GIFなどでパレットに存在しない色が
        # マスク色に設定されている事があるので、
        # その場合は通常通り左上の色をマスク色とする
        # 将来、もしこの処理の結果問題が起きた場合は
        # このif文以降の処理を削除する必要がある
        if mask and image.HasMask() and wxbmp.GetDepth() <= 8:
            palette = wxbmp.GetPalette()
            if palette is not None:
                maskcolor = (image.GetMaskRed(), image.GetMaskGreen(), image.GetMaskBlue())
                maskok = False
                for pixel in range(palette.GetColoursCount()):
                    if palette.GetRGB(pixel) == maskcolor:
                        maskok = True
                        break
                if not maskok:
                    maskcolour = set_mask(image, maskpos)
                    wxbmp = image.ConvertToBitmap()

    elif image:
        wxbmp = image.ConvertToBitmap()
    else:
        try:
            wxbmp = wx.Bitmap(name)
        except Exception:
            print("画像が読み込めません(load_wxbmp)", name)
            return masked_empty_bitmap()

    if bmpdepth == 1 and mask:
        wxbmp.bmpdepthis1 = True
    if maskcolour:
        wxbmp.maskcolour = maskcolour

    wxbmp.scr_scale = up_scr

    return wxbmp


def empty_bitmap(w: int, h: int) -> wx.Bitmap:
    """空のビットマップを返す。"""
    if w <= 0:
        w = 1
    if h <= 0:
        h = 1
    return wx.Bitmap(w, h, depth=24)


def empty_bitmap_rgba(w: int, h: int) -> wx.Bitmap:
    """空のビットマップ(アルファ値あり)を返す。"""
    return wx.Bitmap(w, h, depth=32)


def copy_wxbmp(bmp: wx.Bitmap, usebuffer: bool = False) -> wx.Bitmap:
    """wx.Bitmapのコピーを生成する。"""
    w = bmp.GetWidth()
    h = bmp.GetHeight()
    if usebuffer:
        buf = wxbmp_to_buffer(bmp)
        return wx.Bitmap.FromBuffer(w, h, buf)
    else:
        return bmp.GetSubBitmap((0, 0, w, h))


def convert_to_image(bmp: wx.Bitmap) -> wx.Image:
    """wx.Bitmapをwx.Imageに変換する。
    FIXME: 直接bmp.ConvertToImage()を使用すると
           画像が化ける事がある
    """
    w = bmp.GetWidth()
    h = bmp.GetHeight()
    if w <= 0 or h <= 0:
        img = wx.Image(w, h)
    buf = wxbmp_to_buffer(bmp)
    try:
        img = wx.ImageFromBuffer(w, h, buf)
    except Exception:
        img = bmp.ConvertToImage()
    if hasattr(bmp, "bmpdepthis1"):
        img.bmpdepthis1 = bmp.bmpdepthis1
    if hasattr(bmp, "maskcolour"):
        r, g, b = bmp.maskcolour
        img.maskcolour = bmp.maskcolour
        img.SetMaskColour(r, g, b)
    return img


def wxbmp_to_buffer(bmp: wx.Bitmap) -> Sequence[int]:
    """wx.BitmapをRGBのバイト配列へ変換する。"""
    w, h = bmp.GetSize()
    buf: array.ArrayType[int] = array.array('B', [0] * (w * h * 3))
    bmp.CopyToBuffer(buf)
    return buf


def fill_image(img: pygame.surface.Surface, surface: pygame.surface.Surface, csize: Tuple[int, int],
               ctrlpos: Tuple[int, int] = (0, 0), cpos: Tuple[int, int] = (0, 0)) -> None:
    """引数のsurfaceをimg上に敷き詰める。"""
    imgsize = surface.get_size()
    w, h = imgsize

    startx = -(ctrlpos[0] % w)
    starty = -(ctrlpos[1] % h)

    x = startx
    while x < csize[0]:
        y = starty
        while y < csize[1]:
            img.blit(surface, (x + cpos[0], y + cpos[1]))
            y += h
        x += w


def fill_bitmap(dc: wx.DC, bmp: wx.Bitmap, csize: Union[Tuple[int, int], wx.Size],
                ctrlpos: Union[Tuple[int, int], wx.Point] = (0, 0),
                cpos: Union[Tuple[float, float], Tuple[int, int]] = (0, 0)) -> None:
    """引数のbmpを敷き詰める。"""
    imgsize = bmp.GetSize()
    w, h = imgsize

    startx = -(ctrlpos[0] % w)
    starty = -(ctrlpos[1] % h)

    x = startx
    while x < csize[0]:
        y = starty
        while y < csize[1]:
            dc.DrawBitmap(bmp, x + cpos[0], y + cpos[1], False)
            y += h
        x += w


def get_centerposition(size: wx.Size, targetpos: Tuple[int, int],
                       targetsize: Tuple[int, int] = (1, 1)) -> Tuple[int, int]:
    """中央取りのpositionを計算して返す。"""
    top, left = targetsize[0] // 2, targetsize[1] // 2
    top, left = targetpos[0] + top, targetpos[1] + left
    top, left = top - size[0] // 2, left - size[1] // 2
    return (top, left)


def draw_center(dc: wx.DC, target: Union[str, wx.Bitmap], pos: Tuple[int, int], mask: bool = True) -> None:
    """指定した座標にBitmap・テキストの中央を合わせて描画。
    target: wx.Bitmapかstrかunicode
    """
    if isinstance(target, str):
        size = dc.GetTextExtent(target)
        pos = get_centerposition(size, pos)
        dc.DrawText(target, pos[0], pos[1])
    elif isinstance(target, wx.Bitmap):
        size = target.GetSize()
        pos = get_centerposition(size, pos)
        dc.DrawBitmap(target, pos[0], pos[1], mask)


def draw_height(dc: wx.DC, target: Union[str, wx.Bitmap], height: int, mask: bool = True) -> None:
    """高さのみ指定して、横幅は背景の中央に合わせてBitmap・テキストを描画。
    target: wx.Bitmapかstrかunicode
    """
    if isinstance(target, str):
        width = (dc.GetSize()[0] - dc.GetTextExtent(target)[0]) // 2
        dc.DrawText(target, width, height)
    elif isinstance(target, wx.Bitmap):
        width = (dc.GetSize()[0] - target.GetSize()[0]) // 2
        dc.DrawBitmap(target, width, height, mask)


def draw_box(dc: wx.DC, pos: Tuple[int, int], size: Tuple[int, int]) -> None:
    """dcでStaticBoxの囲いを描画する。"""
    # ハイライト
    colour = wx.SystemSettings.GetColour(wx.SYS_COLOUR_3DHIGHLIGHT)
    dc.SetPen(wx.Pen(colour, 1, wx.SOLID))
    box = get_boxpointlist((pos[0] + 1, pos[1] + 1), size)
    dc.DrawLineList(box)
    # 主線
    colour = wx.SystemSettings.GetColour(wx.SYS_COLOUR_3DSHADOW)
    dc.SetPen(wx.Pen(colour, 1, wx.SOLID))
    box = get_boxpointlist(pos, size)
    dc.DrawLineList(box)


def draw_witharound_simple(dc: wx.DC, s: str, x: int, y: int, aroundcolor: wx.Colour) -> None:
    """テキストsを縁取りしながら描画する。"""
    oldcolor = dc.GetTextForeground()
    dc.SetTextForeground(aroundcolor)
    for xx in range(x - 1, x + 2):
        for yy in range(y - 1, y + 2):
            if xx != x or yy != y:
                dc.DrawText(s, xx, yy)
    dc.SetTextForeground(oldcolor)
    dc.DrawText(s, x, y)


def draw_witharound(dc: wx.DC, s: str, x: int, y: int, maxwidth: int = 0, align: int = wx.ALIGN_LEFT,
                    aroundcolor: Optional[wx.Colour] = None) -> None:
    """テキストsを縁取りしながら描画する。
    フォントのスムージングを行う。
    """
    if aroundcolor:
        white = aroundcolor
    else:
        white = False
    draw_antialiasedtext(dc, s, x, y, white, maxwidth, 0, scaledown=False, bordering=True, align=align)


def draw_adjusted(dc: wx.MemoryDC, s: str, x: int, y: int, maxwidth: int, align: int = wx.ALIGN_LEFT) -> None:
    """テキストをmaxwidthの幅に収まるように描画する。"""
    w = dc.GetTextExtent(s)[0]
    if w <= maxwidth:
        if align == wx.ALIGN_CENTER:
            x += (maxwidth - w) // 2
        elif align == wx.ALIGN_RIGHT:
            x += maxwidth - w
        dc.DrawText(s, x, y)
    else:
        quality = wx.IMAGE_QUALITY_HIGH
        draw_antialiasedtext(dc, s, x, y, dc.GetTextForeground(), maxwidth, 0,
                             quality=quality, scaledown=False, bordering=False, align=align)


def draw_antialiasedtext(dc: wx.DC, text: str, x: int, y: int, white: bool, maxwidth: int, padding: int,
                         quality: Optional[int] = None, scaledown: bool = True, alpha: int = 64,
                         bordering: bool = False, width_coeff: float = 1.0, align: int = wx.ALIGN_LEFT) -> None:
    if not text:
        return
    w = dc.GetTextExtent(text)[0]
    if w <= maxwidth:
        if align == wx.ALIGN_CENTER:
            x += (maxwidth - w) // 2
        elif align == wx.ALIGN_RIGHT:
            x += maxwidth - w
    if bordering:
        subimg = cw.util.render_antialiasedtext(dc, text, not white, maxwidth, padding,
                                                scaledown=scaledown, quality=quality, alpha=alpha,
                                                width_coeff=width_coeff)
        for xx in range(x - 1, x + 2):
            for yy in range(y - 1, y + 2):
                if xx != x or yy != y:
                    dc.DrawBitmap(subimg, xx, yy)
    subimg = cw.util.render_antialiasedtext(dc, text, white, maxwidth, padding,
                                            scaledown=scaledown, quality=quality,
                                            width_coeff=width_coeff)
    dc.DrawBitmap(subimg, x, y)


def render_antialiasedtext(basedc: wx.DC, text: str, white: bool, maxwidth: int, padding: int,
                           quality: Optional[int] = None, scaledown: bool = True, alpha: int = 255,
                           width_coeff: float = 1.0) -> wx.Bitmap:
    """スムージングが施された、背景が透明なテキストを描画して返す。"""
    if quality is None:
        quality = wx.IMAGE_QUALITY_BICUBIC
    w, h = basedc.GetTextExtent(text)
    basew, baseh = w, h
    if w <= 0 or h <= 0:
        return empty_bitmap(w, h)
    font = basedc.GetFont()
    upfont = 0 < maxwidth and maxwidth < w and not scaledown
    if upfont:
        scaledown = True
        pixelsize = font.GetPixelSize()[1]
        family = font.GetFamily()
        style = font.GetStyle()
        weight = font.GetWeight()
        underline = font.GetUnderlined()
        facename = font.GetFaceName()
        encoding = font.GetEncoding()
        font = wx.Font(wx.Size(0, pixelsize * 2), family, style, weight, underline, facename, encoding)
        basedc.SetFont(font)
        w, h = basedc.GetTextExtent(text)
    else:
        basew = w // 2
        baseh = h // 2
    if w <= 0 or h <= 0:
        return empty_bitmap(w, h)
    wxbmp = empty_bitmap(w, h)
    dc = wx.MemoryDC(wxbmp)
    dc.SetFont(font)
    dc.SetBrush(wx.BLACK_BRUSH)
    dc.SetPen(wx.BLACK_PEN)
    dc.DrawRectangle(-1, -1, w + 2, h + 2)
    dc.SetTextForeground(wx.WHITE)
    dc.DrawText(text, 0, 0)
    dc.SelectObject(wx.NullBitmap)
    dc.Destroy()
    redbuf = bytearray(wxbmp_to_buffer(wxbmp))[::3]
    if isinstance(white, wx.Colour):
        brush = wx.Brush(white)
        pen = wx.Pen(white)
    elif white:
        brush = wx.WHITE_BRUSH
        pen = wx.WHITE_PEN
    else:
        brush = wx.BLACK_BRUSH
        pen = wx.BLACK_PEN
    wxbmp = empty_bitmap_rgba(w, h)
    dc = wx.MemoryDC(wxbmp)
    dc.SetFont(font)
    dc.SetBrush(brush)
    dc.SetPen(pen)
    dc.DrawRectangle(-1, -1, w + 2, h + 2)
    dc.SelectObject(wx.NullBitmap)
    dc.Destroy()
    subimg = convert_to_image(wxbmp)

    # BUG: wxGTK 4.0.1でランダムに背景が真っ白になる問題への対策
    if sys.platform != "win32":
        redbuf = bytearray(redbuf)
        if sys.platform == "darwin":
            from . import _imageretouch_mac
            _imageretouch_mac.mul_alphaonly(redbuf, alpha)
        elif sys.maxsize == 0x7fffffff:
            from . import _imageretouch32
            _imageretouch32.mul_alphaonly(redbuf, alpha)
        elif sys.maxsize == 0x7fffffffffffffff:
            from . import _imageretouch64
            _imageretouch64.mul_alphaonly(redbuf, alpha)
    subimg.SetAlphaBuffer(redbuf)

    if scaledown:
        if 0 < maxwidth and basew + padding * 2 > maxwidth:
            size = (maxwidth - padding * 2, baseh)
            subimg = subimg.Rescale(int(size[0] * width_coeff), baseh, quality=quality)
        else:
            subimg = subimg.Rescale(int((basew) * width_coeff), baseh, quality=quality)
    else:
        if 0 < maxwidth and w + padding * 2 > maxwidth:
            size = (maxwidth - padding * 2, h)
            subimg = subimg.Rescale(int(size[0] * width_coeff), h, quality=quality)
        elif width_coeff != 1.0:
            w, h = subimg.GetSize()
            subimg = subimg.Rescale(int(w * width_coeff), h, quality=quality)

    # BUG: wxGTK 4.0.1でランダムに背景が真っ白になる問題への対策
    if sys.platform == "win32":
        if alpha != 255:
            cw.imageretouch.mul_wxalpha(subimg, alpha)

    if upfont:
        font = wx.Font(wx.Size(0, pixelsize), family, style, weight, 0, facename, encoding)
        basedc.SetFont(font)

    subimg = subimg.ConvertToBitmap()
    return subimg


def get_boxpointlist(pos: Tuple[int, int], size: Tuple[int, int]) -> List[Tuple[int, int, int, int]]:
    """StaticBoxの囲い描画用のposlistを返す。"""
    x, y = pos
    width, height = size
    poslist = [][:]
    poslist.append((x, y, x + width, y))
    poslist.append((x, y, x, y + height))
    poslist.append((x + width, y, x + width, y + height))
    poslist.append((x, y + height, x + width, y + height))
    return poslist


def create_fileselection(parent: wx.TopLevelWindow, target: Optional[wx.TextCtrl], message: str, wildcard: str = "*.*",
                         seldir: bool = False, getbasedir: Optional[Callable[[], str]] = None,
                         callback: Optional[Union[Callable[[Sequence[str]], None], Callable[[str], None]]] = None,
                         winsize: bool = False, multiple: bool = False) -> wx.Button:
    """ファイルまたはディレクトリを選択する
    ダイアログを表示するボタンを生成する。
    parent: ボタンの親パネル。
    target: 選択結果を格納するコントロール。
    message: 選択時に表示されるメッセージ。
    wildcard: 選択対象の定義。
    seldir: Trueの場合はディレクトリの選択を行う。
    getbasedir: 相対パスを扱う場合は基準となるパスを返す関数。
    """

    def OnOpen(event: wx.CommandEvent) -> None:
        if target is None:
            fpath = ""
        else:
            fpath = target.GetValue()
        dpath = fpath
        if getbasedir and not os.path.isabs(dpath):
            dpath = os.path.join(getbasedir(), dpath)
        if seldir:
            dlg = wx.DirDialog(parent.TopLevelParent, message, dpath, wx.DD_DIR_MUST_EXIST)
            if dlg.ShowModal() == wx.ID_OK:
                dpath = dlg.GetPath()
                if getbasedir:
                    base = getbasedir()
                    dpath2 = cw.util.relpath(dpath, base)
                    if not dpath2.startswith(".." + os.path.sep):
                        dpath = dpath2
                if target is not None:
                    target.SetValue(dpath)
                if callback:
                    callback(dpath)
            dlg.Destroy()
        else:
            dpath = os.path.dirname(fpath)
            fpath = os.path.basename(fpath)
            flags = wx.FD_OPEN
            if multiple:
                flags |= wx.FD_MULTIPLE
            dlg = wx.FileDialog(parent.TopLevelParent, message, dpath, fpath, wildcard, flags)
            if dlg.ShowModal() == wx.ID_OK:
                files = dlg.GetFilenames()
                seq: List[str] = []
                fnames = ""
                for fname in files:
                    fpath = os.path.join(dlg.GetDirectory(), fname)
                    if getbasedir:
                        base = getbasedir()
                        fpath2 = cw.util.relpath(fpath, base)
                        if not fpath2.startswith(".." + os.path.sep):
                            fpath = fpath2
                    if target is not None:
                        if fnames != "":
                            fnames += "; "
                        fnames += fpath
                    seq.append(fpath)
                if callback:
                    if multiple:
                        # BUG: 以下のコードはなぜか実行されない(Python 3.8.6)
                        # callback2: Callable[..., bool] = callable
                        # callback2(seq)
                        callback(typing.cast(str, seq))
                    else:
                        callback(seq[0])
                if target is not None:
                    target.SetValue(fnames)
            dlg.Destroy()

    if winsize:
        size = (cw.wins(20), -1)
    else:
        size = (cw.ppis(20), -1)
    button = wx.Button(parent, size=size, label="...")
    parent.Bind(wx.EVT_BUTTON, OnOpen, button)
    return button


def adjust_position(frame: wx.TopLevelWindow) -> None:
    """frameの位置がいずれかのモニタ内に収まるように調節する。
    サイズ変更は行わない。
    """
    win = wx.Display.GetFromWindow(frame)
    if win == wx.NOT_FOUND:
        win = 0
    cax, cay, caw, cah = wx.Display(win).GetClientArea()
    caw += cax
    cah += cay
    x, y, w, h = frame.GetRect()
    if caw <= x + w:
        x = caw - w
    if cah <= y + h:
        y = cah - h
    if x < cax:
        x = cax
    if y < cay:
        y = cay
    frame.SetPosition((x, y))


class CWPyStaticBitmap(wx.Panel):
    """wx.StaticBitmapはアルファチャンネル付きの画像を
    正しく表示できない場合があるので代替する。
    複数重ねての表示にも対応。
    """
    def __init__(self, parent: wx.Panel, cid: int, bmps: List[wx.Bitmap], bmps_bmpdepthkey: List[wx.Bitmap],
                 size: Optional[Tuple[int, int]] = None, infos: Optional[List["cw.image.ImageInfo"]] = None,
                 ss: Optional[Callable[["cw.Scalable"], "cw.Scalable"]] = None) -> None:
        if not ss:
            ss = cw.ppis
        if not size and bmps:
            w = 0
            h = 0
            for bmp in bmps:
                s = bmp.GetSize()
                w = max(w, s[0])
                h = max(h, s[1])
            size = (w, h)
        wx.Panel.__init__(self, parent, cid, size=size)
        self.bmps = bmps
        self.bmps_bmpdepthkey = bmps_bmpdepthkey
        self.infos = infos
        # BUG: self._ss: Callable[[cw.Scalable], cw.Scalable] にすると謎の警告が発生する(mypy 0.782)
        self._ss: Callable[..., typing.Any] = ss
        self._bind()

    def _bind(self) -> None:
        self.Bind(wx.EVT_PAINT, self.OnPaint)

    def OnPaint(self, event: wx.PaintEvent) -> None:
        dest = wx.Bitmap(self.GetClientSize())
        dc = wx.MemoryDC(dest)
        clear_background(dc, self)

        for i, (bmp, bmpdepthkey) in enumerate(zip(self.bmps, self.bmps_bmpdepthkey)):
            if self.infos:
                info = self.infos[i]
                w, h = bmpdepthkey.GetSize()
                scr_scale = bmpdepthkey.scr_scale if hasattr(bmpdepthkey, "scr_scale") else 1
                w //= scr_scale
                h //= scr_scale
                baserect = info.calc_basecardposition_wx((w, h), noscale=True,
                                                         basecardtype="LargeCard",
                                                         cardpostype="NotCard")
                baserect = self._ss(baserect)
                x, y = baserect.x, baserect.y
            else:
                x, y = 0, 0
            cw.imageretouch.wxblit_2bitbmp_to_card(dc, dest, bmp, x, y, True, bitsizekey=bmpdepthkey)

        dc = wx.PaintDC(self)
        dc.DrawBitmap(dest, cw.wins(0), cw.wins(0))

    def SetBitmap(self, bmps: List[wx.Bitmap], bmps_bmpdepthkey: List[wx.Bitmap],
                  infos: Optional[List["cw.image.ImageInfo"]] = None) -> None:
        self.bmps = bmps
        self.bmps_bmpdepthkey = bmps_bmpdepthkey
        self.infos = infos
        self.Refresh()

    def GetBitmap(self) -> List[wx.Bitmap]:
        return self.bmps


def clear_background(dc: wx.DC, window: wx.Control) -> None:
    """windowの背景色によって塗り潰す。"""
    if sys.platform == "win32":
        colour = get_backgroundcolour(window)
        b = dc.GetBrush()
        p = dc.GetPen()
        dc.SetBrush(wx.Brush(colour))
        dc.SetPen(wx.Pen(colour))
        w, h = window.GetClientSize()
        dc.DrawRectangle(0, 0, w, h)
        dc.SetBrush(b)
        dc.SetPen(p)
    else:
        cdc = wx.ClientDC(window)
        w, h = window.GetClientSize()
        dc.Blit(0, 0, w, h, cdc, 0, 0)
        del cdc


def get_backgroundcolour(window: Union[wx.Control]) -> wx.Colour:
    """windowの実際の背景色を返す。"""
    notebook = None
    window2 = window
    while window.HasTransparentBackground() or isinstance(window, wx.Panel):
        window = window.GetParent()
        if isinstance(window, wx.Notebook):
            notebook = window
            colour = notebook.GetThemeBackgroundColour()
            if colour.IsOk():
                return colour
            break

    while window2.HasTransparentBackground():
        window2 = window2.GetParent()

    return window2.GetBackgroundColour()


def abbr_longstr(dc: wx.DC, text: str, w: int) -> str:
    """wx.DCを使って長い文字列を省略して末尾に三点リーダを付ける。
    dc: 幅計算用のwx.DC。
    text: 編集対象の文字列。
    w: 目標文字列長(pixel)。
    """
    if w <= 0 and text:
        if dc.GetTextExtent(text)[0] <= dc.GetTextExtent("...")[0]:
            return text
        else:
            return "..."
    width = dc.GetTextExtent(text)[0]
    if width > w:
        while text and dc.GetTextExtent(text + "...")[0] > w:
            text = text[:-1]
        text += "..."
    return text


def abbr_longstr_with_count(text: str, w: int) -> str:
    """文字数によって省略を行う。
    text: 編集対象の文字列。
    w: 目標文字列長(半角文字数)。
    """
    width = get_strlen(text)
    if w <= 0 and text:
        if width <= len("..."):
            return text
        else:
            return "..."
    if width > w:
        while text and width + len("...") > w:
            width -= get_strlen(text[-1])
            text = text[:-1]
        text += "..."
    return text


class CheckableListCtrl(wx.ListCtrl,
                        wx.lib.mixins.listctrl.ListCtrlAutoWidthMixin):
    """チェックボックス付きのリスト。"""

    def __init__(self, parent: wx.Dialog, cid: int, size: Tuple[int, int], style: int, colpos: int = 0,
                 system: bool = True) -> None:
        wx.ListCtrl.__init__(self, parent=parent, id=cid, size=size, style=style | wx.LC_NO_HEADER)
        wx.lib.mixins.listctrl.ListCtrlAutoWidthMixin.__init__(self)
        self.EnableCheckBoxes(True)
        for i in range(colpos + 1):
            self.InsertColumn(i, "")

        self.InsertItem(0, "", 0)
        rect = self.GetItemRect(0, wx.LIST_RECT_LABEL)
        self.SetColumnWidth(0, rect.x)
        self.DeleteAllItems()

        self.resizeLastColumn(0)

        self._system = system
        self._checking = False

        self.Bind(wx.EVT_LIST_ITEM_CHECKED, self.OnListItemChecked)
        self.Bind(wx.EVT_LIST_ITEM_UNCHECKED, self.OnListItemChecked)

    def OnListItemChecked(self, event: wx.ListEvent) -> None:
        # チェック時に音を鳴らし、選択中のアイテムだった場合は
        # 他の選択中のアイテムにもチェックを反映
        if not self.TopLevelParent.IsShown():
            return
        if self._checking:
            return
        self._checking = True
        if not self._system:
            cw.cwpy.play_sound("page")
        index = event.Index
        flag = self.IsItemChecked(index)
        i = self.GetNextItem(index - 1, wx.LIST_NEXT_ALL, wx.LIST_STATE_SELECTED)
        if index == i:
            index = -1
            while True:
                index = self.GetNextItem(index, wx.LIST_NEXT_ALL, wx.LIST_STATE_SELECTED)
                if index < 0:
                    break
                if index != i:
                    self.CheckItem(index, flag)
        self._checking = False


class CWBackCheckBox(wx.CheckBox):
    def __init__(self, parent: wx.Dialog, wid: int, text: str) -> None:
        """CAUTIONリソースを背景とするチェックボックス。"""
        wx.CheckBox.__init__(self, parent, wid, text)
        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)

        dc = wx.ClientDC(self)
        dc.SetFont(cw.cwpy.rsrc.get_wxfont("paneltitle", pixelsize=cw.wins(15)))
        w, h = dc.GetTextExtent(text)
        bmp = cw.cwpy.rsrc.debugs_wx["NOCHECK"]
        w += cw.wins(4) + bmp.GetWidth()
        h = max(h, bmp.GetHeight())
        self.SetMinSize((w, h))
        self.SetSize((w, h))

        self._nocheck = bmp
        self._check = cw.cwpy.rsrc.debugs_wx["CHECK"]

        self.background = cw.cwpy.rsrc.dialogs["CAUTION"]

        self._bind()

    def _bind(self) -> None:
        self.Bind(wx.EVT_PAINT, self.OnPaint)

    def set_background(self, bmp: wx.Bitmap) -> None:
        self.background = bmp
        self.Refresh()

    def OnPaint(self, event: wx.PaintEvent) -> None:
        size = self.GetSize()
        basebmp = empty_bitmap(size[0], size[1])
        dc = wx.MemoryDC(basebmp)
        # background
        bmp = self.background
        csize = self.GetClientSize()
        fill_bitmap(dc, bmp, csize, ctrlpos=self.GetPosition())
        # checkbox
        if self.GetValue():
            bmp = self._check
        else:
            bmp = self._nocheck
        dc.DrawBitmap(bmp, cw.wins(2), (csize[1] - bmp.GetHeight()) // 2, True)
        # text
        dc.SetTextForeground(wx.BLACK)
        dc.SetFont(cw.cwpy.rsrc.get_wxfont("paneltitle", pixelsize=cw.wins(15)))
        s = self.GetLabel()
        tsize = dc.GetTextExtent(s)
        dc.DrawText(s, bmp.GetWidth() + cw.wins(4), (csize[1] - tsize[1]) // 2)
        dc.SelectObject(wx.NullBitmap)

        dc = wx.PaintDC(self)
        dc.DrawBitmap(basebmp, 0, 0)


def add_sideclickhandlers(toppanel: wx.Panel, leftbtn: wx.BitmapButton, rightbtn: wx.BitmapButton) -> None:
    """toppanelの左右の領域をクリックすると
    leftbtnまたはrightbtnのイベントが実行されるように
    イベントへのバインドを行う。
    """

    def _is_cursorinleft() -> bool:
        rect = toppanel.GetClientRect()
        x, _y = toppanel.ScreenToClient(wx.GetMousePosition())
        enabled: bool = leftbtn.IsEnabled()
        return x < rect.x + rect.width // 4 and enabled

    def _is_cursorinright() -> bool:
        rect = toppanel.GetClientRect()
        x, _y = toppanel.ScreenToClient(wx.GetMousePosition())
        enabled: bool = rightbtn.IsEnabled()
        return rect.x + rect.width // 4 * 3 < x and enabled

    def _update_mousepos() -> None:
        if _is_cursorinleft():
            toppanel.SetCursor(cw.cwpy.rsrc.cursors["CURSOR_BACK"])
        elif _is_cursorinright():
            toppanel.SetCursor(cw.cwpy.rsrc.cursors["CURSOR_FORE"])
        else:
            toppanel.SetCursor(cw.cwpy.rsrc.cursors["CURSOR_ARROW"])

    def OnMotion(evt: wx.MouseEvent) -> None:
        _update_mousepos()

    def OnLeftUp(evt: wx.MouseEvent) -> None:
        if _is_cursorinleft():
            btnevent = wx.PyCommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, leftbtn.GetId())
            leftbtn.ProcessEvent(btnevent)
        elif _is_cursorinright():
            btnevent = wx.PyCommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, rightbtn.GetId())
            rightbtn.ProcessEvent(btnevent)

    _update_mousepos()
    toppanel.Bind(wx.EVT_MOTION, OnMotion)
    toppanel.Bind(wx.EVT_LEFT_UP, OnLeftUp)


def set_acceleratortable(panel: wx.Window, seq: List[Tuple[int, int, int]],
                         ignoreleftrightkeys: Tuple[wx.Control, ...] = (wx.TextCtrl, wx.Dialog, wx.Panel)) -> None:
    """panelにseqから生成したAcceleratorTableを設定する。
    """
    # テキスト入力欄に限り左右キーを取り除く
    seq2 = []
    for accel in seq:
        if not (accel[0] == wx.ACCEL_NORMAL and accel[1] in (wx.WXK_LEFT, wx.WXK_RIGHT)):
            seq2.append(accel)

    accel1 = wx.AcceleratorTable(seq)
    accel2 = wx.AcceleratorTable(seq2)

    def recurse(widget: wx.Control) -> None:
        if isinstance(widget, ignoreleftrightkeys):
            widget.SetAcceleratorTable(accel2)
        else:
            widget.SetAcceleratorTable(accel1)
        for child in widget.GetChildren():
            recurse(child)

    recurse(panel)


def adjust_dropdownwidth(choice: wx.ComboBox) -> None:
    """wx.Choiceまたはwx.ComboBoxのドロップダウンリストの
    横幅を内容に合わせて広げる。
    """
    if sys.platform == "win32":
        # スクロールバーの幅
        scwidth = win32api.GetSystemMetrics(win32con.SM_CXVSCROLL)
        w = win32api.SendMessage(choice.GetHandle(), win32con.CB_GETDROPPEDWIDTH, 0, 0)

        # 項目ごとに幅を計算
        dc = wx.ClientDC(choice)
        for s in choice.GetItems():
            w = max(w, dc.GetTextExtent(s)[0] + cw.ppis(5) + scwidth)
        dc.SetFont(choice.GetFont())

        # モニタの横幅よりは大きくしない
        d = wx.Display.GetFromWindow(choice)
        if d == wx.NOT_FOUND:
            d = 0
        drect = wx.Display(d).GetClientArea()
        w = min(w, drect[2])

        # 幅を設定
        win32api.SendMessage(choice.GetHandle(), win32con.CB_SETDROPPEDWIDTH, w, 0)


def has_modalchild(frame: wx.TopLevelWindow) -> bool:
    """frame.TopLevelParentにモーダル表示中のサブウィンドウがあればTrue。"""
    for child in frame.TopLevelParent.GetChildren():
        if isinstance(child, wx.Dialog) and child.IsShown() and child.IsModal():
            return True
    if frame is cw.cwpy.frame and cw.cwpy.frame.debugger and has_modalchild(cw.cwpy.frame.debugger):
        return True
    return False


class CWPyRichTextCtrl(wx.richtext.RichTextCtrl):
    _search_engines: Optional[List[Tuple[str, str, int]]] = None

    def __init__(self, parent: wx.Panel, wid: int, text: str = "", size: Tuple[int, int] = (-1, -1),
                 style: int = 0, searchmenu: bool = False) -> None:
        wx.richtext.RichTextCtrl.__init__(self, parent, wid, text, size=size, style=style)

        # popup menu
        self.popup_menu = wx.Menu()
        self.mi_copy = wx.MenuItem(self.popup_menu, wx.ID_COPY, "コピー(&C)")
        self.mi_selectall = wx.MenuItem(self.popup_menu, wx.ID_SELECTALL, "すべて選択(&A)")
        self.popup_menu.Append(self.mi_copy)
        self.popup_menu.Append(self.mi_selectall)

        self.Bind(wx.EVT_TEXT_URL, self.OnURL)
        self.Bind(wx.EVT_CONTEXT_MENU, self.OnContextMenu)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.Bind(wx.EVT_MOTION, self.OnMotion)
        self.Bind(wx.EVT_MENU, self.OnCopy, id=wx.ID_COPY)
        self.Bind(wx.EVT_MENU, self.OnSelectAll, id=wx.ID_SELECTALL)

        self.search_engines = []
        if searchmenu:
            if os.path.isfile("Data/SearchEngines.xml"):
                try:
                    if CWPyRichTextCtrl._search_engines is None:
                        CWPyRichTextCtrl._search_engines = []
                        data = cw.data.xml2element("Data/SearchEngines.xml")
                        for e in data:
                            if e.tag == "SearchEngine":
                                url = e.getattr(".", "url", "")
                                name = e.text
                                if url and name:
                                    menuid = wx.NewId()
                                    CWPyRichTextCtrl._search_engines.append((url, name, menuid))

                    class SearchEngine(object):
                        def __init__(self, parent: CWPyRichTextCtrl, url: str, name: str, menuid: int) -> None:
                            self.parent = parent
                            self.url = url
                            self.mi = wx.MenuItem(self.parent.popup_menu, menuid, name)
                            self.parent.popup_menu.Append(self.mi)
                            self.parent.Bind(wx.EVT_MENU, self.OnSearch, id=menuid)

                        def OnSearch(self, event: wx.CommandEvent) -> None:
                            try:
                                self.parent.go_url(self.url % self.parent.GetStringSelection())
                            except Exception:
                                cw.util.print_ex(file=sys.stderr)

                    separator = False
                    for url, name, menuid in CWPyRichTextCtrl._search_engines:
                        if not separator:
                            separator = True
                            self.popup_menu.AppendSeparator()
                        self.search_engines.append(SearchEngine(self, url, name, menuid))
                except Exception:
                    cw.util.print_ex(file=sys.stderr)

    def set_text(self, value: bytes, linkurl: bool = False) -> None:
        # ZIPアーカイブのファイルエンコーディングと
        # 読み込むテキストファイルのエンコーディングが異なる場合、
        # エラーが出るので
        value2 = cw.util.decode_text(value)

        # FIXME: URLクリック等でキャレットがURL上にある場合に
        # テキストを削除すると、URLリンク設定が以降追加された
        # 全テキストに適用される。
        # そのため、末尾がURLではない事を前提に、キャレットを
        # テキスト末尾へ移動してからクリアを行う。
        self.MoveEnd()
        self.Clear()

        # URLを検索して取り出し、テキストをリストに分割
        def get_urls(text: str) -> List[Tuple[str, bool]]:
            prog = re.compile(r"http(s)?://([\w\-]+\.)+[\w]+(/[\w\-./?%&=~#!]*)?")
            seq = []

            url = prog.search(text)

            while url:
                if url.start() > 0:
                    seq.append((text[:url.start()], False))
                seq.append((url.group(0), True))
                text = text[url.end():]

                url = prog.search(text)

            if len(text) > 0:
                seq.append((text, False))

            return seq

        if linkurl:
            for v, url_flag in get_urls(value2):
                if url_flag:
                    self.BeginTextColour((255, 132, 0))
                    self.BeginUnderline()
                    self.BeginURL(v)
                else:
                    self.BeginTextColour(wx.WHITE)

                self.WriteText(v)

                if url_flag:
                    self.EndURL()
                    self.EndUnderline()
                self.EndTextColour()

            self.EndTextColour()
            if len(self.GetValue()) and not self.GetValue()[-1] in ("\n", "\r"):
                # 末尾が改行でない時は改行を加える
                # 前記した全文URL化バグへの対策でもある
                self.WriteText("\n")

        self.ShowPosition(0)

    def OnMouseWheel(self, event: wx.MouseEvent) -> None:
        if has_modalchild(self):
            return

        if sys.platform == "win32":
            import win32gui
            SPI_GETWHEELSCROLLLINES = 104
            value = win32gui.SystemParametersInfo(SPI_GETWHEELSCROLLLINES)
            value = cw.wins(value * 4)
        else:
            value = cw.wins(4 * 4)

        if get_wheelrotation(event) > 0:
            self.ScrollLines(-value)
        else:
            self.ScrollLines(value)

    def OnMotion(self, event: wx.MouseEvent) -> None:
        # 画面外へのドラッグによるスクロール処理
        mousey = event.GetPosition()[1]
        if mousey < cw.wins(0):
            self.ScrollLines(-cw.wins(4))
        elif mousey > self.GetSize()[1]:
            self.ScrollLines(cw.wins(4))

        event.Skip()

    def OnContextMenu(self, event: wx.ContextMenuEvent) -> None:
        self.mi_copy.Enable(self.HasSelection())
        for searchengine in self.search_engines:
            searchengine.mi.Enable(self.HasSelection())
        self.PopupMenu(self.popup_menu)

    def OnCopy(self, event: wx.MenuEvent) -> None:
        self.Copy()

    def OnSelectAll(self, event: wx.MenuEvent) -> None:
        self.SelectAll()

    def go_url(self, url: str) -> None:
        open_url(self, url)

    def OnURL(self, event: wx.TextUrlEvent) -> None:
        # 文字列選択中はブラウザ起動しない
        if not self.HasSelection():
            self.go_url(event.GetString())


def open_url(parentdlg: wx.TopLevelWindow, url: str) -> None:
    """urlをWebブラウザで開く。"""
    if threading.currentThread() is cw.cwpy:
        cw.cwpy.frame.exec_func(open_url, parentdlg, url)
        return

    try:
        webbrowser.open(url)
    except Exception:
        s = "「%s」が開けませんでした。インターネットブラウザが正常に関連付けされているか確認して下さい。" % url
        dlg = cw.dialog.message.ErrorMessage(parentdlg, s)
        cw.cwpy.frame.move_dlg(dlg)
        dlg.ShowModal()
        dlg.Destroy()


def get_wheelrotation(event: wx.MouseEvent) -> int:
    """マウスのホイールを横に倒した場合に
    取得できる回転量の値は直感と逆転しているので
    この関数をラッパとして反転した値を取得する。
    """
    rotation: int = event.GetWheelRotation()
    if event.GetWheelAxis() == wx.MOUSE_WHEEL_HORIZONTAL:
        return -rotation
    else:
        return rotation


class CWTabArt(wx.lib.agw.aui.tabart.AuiDefaultTabArt):
    """wx.lib.agw.aui.tabart.AuiDefaultTabArtと同じように
    wx.lib.agw.aui.AuiNotebookのタブを描画するが、
    テキストのみ左寄せから中央寄せに変更する。
    """

    def __init__(self, indentsize: int = 0) -> None:
        wx.lib.agw.aui.tabart.AuiDefaultTabArt.__init__(self)
        self.indentsize = indentsize

    def Clone(self) -> "CWTabArt":
        return CWTabArt(self.indentsize)

    def DrawTab(self, dc: wx.DC, wnd: wx.lib.agw.aui.AuiNotebook, page: wx.lib.agw.aui.AuiNotebookPage,
                in_rect: wx.Rect, close_button_state: int, paint_control: bool = False) -> Tuple[wx.Rect, wx.Rect, int]:
        # テキストを一旦空にして背景だけ描画させる
        self._cwtabart_caption = page.caption
        self._tab_size = self.GetTabSize(dc, wnd, page.caption, page.bitmap, page.active, close_button_state,
                                         page.control)[0]
        page.caption = ""
        r: Tuple[wx.Rect, wx.Rect, int] = super(CWTabArt, self).DrawTab(dc, wnd, page, in_rect, close_button_state,
                                                                        paint_control)
        page.caption = self._cwtabart_caption
        # テキストを描画
        dc.SetFont(wnd.GetFont())
        te = dc.GetTextExtent(page.caption)
        rect = r[0]
        x = rect.X + (rect.Width - te[0]) // 2
        y = in_rect.Y + (in_rect.Height - te[1]) // 2
        dc.DrawText(page.caption, x, y)
        if not (self.GetAGWFlags() & wx.lib.agw.aui.tabart.AUI_NB_NO_TAB_FOCUS) and page.active and \
                wx.Window.FindFocus() is wnd:
            dc.SetBrush(wx.TRANSPARENT_BRUSH)
            dc.SetPen(self._focusPen)
            rect = wx.Rect(x - 2, y - 1, te[0] + 4, te[1] + 2)
            dc.DrawRoundedRectangleRect(rect, 0)
        return r

    def DrawFocusRectangle(self, dc: wx.DC, page: wx.lib.agw.aui.AuiNotebookPage, wnd: wx.lib.agw.aui.AuiNotebook,
                           draw_text: str, text_offset: int, bitmap_offset: int, drawn_tab_yoff: int,
                           drawn_tab_height: int, textx: int, texty: int) -> None:
        return

    def GetIndentSize(self) -> int:
        return self.indentsize


class FilePathRenderer(wx.grid.GridCellRenderer):
    def __init__(self, can_file: bool = True, can_dir: bool = True) -> None:
        wx.grid.GridCellRenderer.__init__(self)
        self._can_file = can_file
        self._can_dir = can_dir

    def Clone(self) -> "FilePathRenderer":
        return FilePathRenderer(self._can_file, self._can_dir)

    def Draw(self, grid: wx.grid.Grid, attr: wx.grid.GridCellAttr, dc: wx.DC, rect: wx.Rect,
             row: int, col: int, is_selected: bool) -> None:
        dc.DrawRectangle(rect.X, rect.Y, rect.Width, rect.Height)
        fpath = grid.GetCellValue(row, col)
        if not fpath:
            return
        x = rect.X + cw.ppis(2)
        if not ((self._can_file and os.path.isfile(fpath)) or
                (self._can_dir and os.path.isdir(fpath))):
            bmp = cw.cwpy.rsrc.debugs["WARNING_dbg"]
            y = rect.Y + (rect.Height - bmp.GetHeight()) // 2
            dc.DrawBitmap(bmp, x, y, True)
            x += bmp.GetWidth() + cw.ppis(2)
        dc.SetClippingRegion(*rect)
        y = rect.Y + (rect.Height - dc.GetTextExtent(fpath)[1]) // 2
        dc.DrawText(fpath, x, y)
        dc.DestroyClippingRegion()


class CWPyBitmapComboBox(wx.adv.OwnerDrawnComboBox):
    """
    FIXME: wx.adv.BitmapComboBoxの選択ウィンドウの幅が
    コントロールの幅に固定されてしまうため代替する。
    """

    def __init__(self, parent: wx.Panel, wid: int = wx.ID_ANY, value: str = "",
                 pos: wx.Point = wx.DefaultPosition, size: Tuple[int, int] = wx.DefaultSize,
                 choices: Optional[List[str]] = None, style: int = 0, validator: wx.Validator = wx.DefaultValidator,
                 name: str = " comboBox") -> None:
        if choices is None:
            choices = []
        wx.adv.OwnerDrawnComboBox.__init__(self, parent, wid, value, pos, size, choices, style, validator, name)
        self._items: List[Tuple[str, wx.Bitmap]] = []

    def Clear(self) -> None:
        del self._items[:]
        wx.adv.OwnerDrawnComboBox.Clear(self)

    def Append(self, s: str, bmp: wx.Bitmap) -> None:
        self._items.append((s, bmp))
        wx.adv.OwnerDrawnComboBox.Append(self, s)

    def SetString(self, item: int, s: str) -> None:
        self._items[item] = (s, self._items[item][1])
        wx.adv.OwnerDrawnComboBox.SetString(self, item, s)

    def GetWidestItem(self) -> int:
        dc = wx.ClientDC(self)
        mx = -1
        rw = 0
        for i, (s, bmp) in enumerate(self._items):
            w = bmp.GetSize()[0]
            w += dc.GetTextExtent(s)[0]
            if rw < w:
                mx = i
            rw = max(rw, w)
        return mx

    def GetWidestItemWidth(self) -> int:
        dc = wx.ClientDC(self)
        rw = 0
        for s, bmp in self._items:
            w = bmp.GetSize()[0]
            w += dc.GetTextExtent(s)[0]
            rw = max(rw, w)
        return rw

    def IsListEmpty(self) -> bool:
        return 0 == len(self._items)

    def IsTextEmpty(self) -> bool:
        index: int = self.GetSelection()
        return index == -1 or self._items[index][0] == ""

    def OnDrawBackground(self, dc: wx.DC, rect: wx.Rect, item: int, flags: int) -> None:
        wx.adv.OwnerDrawnComboBox.OnDrawBackground(self, dc, rect, item, flags)

    def OnDrawItem(self, dc: wx.DC, rect: wx.Rect, item: int, flags: int) -> None:
        s, bmp = self._items[item]
        x = rect[0]
        sz = bmp.GetSize()
        dc.DrawBitmap(bmp, (x, rect[1] + (rect[3] - sz[1]) // 2), True)
        x += sz[0]
        sz = dc.GetTextExtent(s)
        dc.DrawText(s, (x, rect[1] + (rect[3] - sz[1]) // 2))

    def OnMeasureItem(self, item: int) -> int:
        dc = wx.ClientDC(self)
        s, bmp = self._items[item]
        sz = dc.GetTextExtent(s)
        result: int = max(bmp.GetHeight(), sz[1])
        return result

    def OnMeasureItemWidth(self, item: int) -> int:
        dc = wx.ClientDC(self)
        s, bmp = self._items[item]
        sz = dc.GetTextExtent(s)
        result: int = bmp.GetWidth() + sz[0]
        return result


# ------------------------------------------------------------------------------
# ショートカット関係
# ------------------------------------------------------------------------------

# CoInitialize()を呼び出し終えたスレッドのset
_cominit_table: Set[threading.Thread] = set()


def _co_initialize() -> None:
    """スレッドごとにCoInitialize()を呼び出す。"""
    global _cominit_table
    if sys.platform == "win32":
        thr = threading.currentThread()
        if thr in _cominit_table:
            return  # 呼び出し済み
        pythoncom.CoInitialize()
        _cominit_table.add(thr)
        # 終了したスレッドがあれば除去
        for thr2 in _cominit_table.copy():
            if not thr2.is_alive():
                _cominit_table.remove(thr2)


def get_linktarget(fpath: str) -> str:
    """fileがショートカットだった場合はリンク先を、
    そうでない場合はfileを返す。
    """
    if sys.platform == "win32":
        if not fpath.lower().endswith(".lnk") or not os.path.isfile(fpath):
            return fpath

        _co_initialize()
        shortcut = pythoncom.CoCreateInstance(win32shell.CLSID_ShellLink, None,
                                              pythoncom.CLSCTX_INPROC_SERVER,
                                              win32shell.IID_IShellLink)
        try:
            STGM_READ = 0x00000000
            shortcut.QueryInterface(pythoncom.IID_IPersistFile).Load(fpath, STGM_READ)
            fpath = shortcut.GetPath(win32shell.SLGP_UNCPRIORITY)[0]
        except Exception:
            print_ex(file=sys.stderr)
            return fpath
        return get_linktarget(join_paths(fpath))
    else:
        return fpath


def set_linktarget(fpath: str, targetpath: str) -> None:
    """fpathがショートカットだった場合は
    リンク先をtargetpathに変更する。
    """
    if sys.platform == "win32":
        if not fpath.lower().endswith(".lnk") or not os.path.isfile(fpath):
            return

        _co_initialize()
        shortcut = pythoncom.CoCreateInstance(win32shell.CLSID_ShellLink, None,
                                              pythoncom.CLSCTX_INPROC_SERVER,
                                              win32shell.IID_IShellLink)
        try:
            STGM_READ = 0x00000000
            shortcut.QueryInterface(pythoncom.IID_IPersistFile).Load(fpath, STGM_READ)
            shortcut.SetPath(targetpath)
            shortcut.QueryInterface(pythoncom.IID_IPersistFile).Save(fpath, 0)
        except Exception:
            print_ex(file=sys.stderr)


def create_link(shortcutpath: str, targetpath: str) -> None:
    """targetpathへのショートカットを
    shortcutpathに作成する。
    """
    if sys.platform == "win32":
        shortcutpath = os.path.abspath(shortcutpath)
        dpath = os.path.dirname(shortcutpath)
        if not os.path.exists(dpath):
            os.makedirs(dpath)

        shortcutpath = os.path.normpath(shortcutpath)
        targetpath = os.path.normpath(targetpath)

        _co_initialize()
        targetpath = os.path.abspath(targetpath)
        shortcut = pythoncom.CoCreateInstance(win32shell.CLSID_ShellLink, None,
                                              pythoncom.CLSCTX_INPROC_SERVER,
                                              win32shell.IID_IShellLink)
        shortcut.SetPath(targetpath)
        shortcut.QueryInterface(pythoncom.IID_IPersistFile).Save(shortcutpath, 0)


def get_symlinktarget(path: str) -> str:
    """pathがシンボリックリンクであればリンク先を、そうでなければpathを返す。"""
    try:
        p = os.path.normpath(os.path.abspath(path))
        if os.path.islink(p):
            ln = os.readlink(p)
            if os.path.isabs(ln):
                path = ln
            else:
                p = os.path.join(os.path.dirname(p), ln)
                path = os.path.normpath(p)
    except ValueError:
        # パス制限に引っかかる場合あり
        # ValueError: lstat: path too long for Windows
        print_ex()
    return path


def get_keypath(path: str) -> str:
    """setやdicのキーとして使えるよう、pathをできるだけ均質化した文字列にする。"""
    return os.path.normcase(os.path.normpath(os.path.abspath(path)))


# ------------------------------------------------------------------------------
# パフォーマンスカウンタ
# ------------------------------------------------------------------------------

dictimes: Dict[str, float] = {}
times = [0.0] * 1024
timer = 0.0


def t_start() -> None:
    global timer
    timer = time.perf_counter_ns()


def t_end(index: int) -> None:
    global times, timer
    times[index] += time.perf_counter_ns() - timer
    timer = time.perf_counter_ns()


def td_end(key: str) -> None:
    global dictimes, timer
    if key in dictimes:
        dictimes[key] += time.perf_counter_ns() - timer
    else:
        dictimes[key] = time.perf_counter_ns() - timer
    timer = time.perf_counter_ns()


def t_reset() -> None:
    global times, dictimes
    times = [0 for v in times]
    dictimes.clear()


def t_print() -> None:
    global times, dictimes
    lines = []
    for i, t in enumerate(times):
        if 0 < t:
            s = "time[%s] = %s" % (i, t)
            lines.append(s)
            print(s)
    for key, t in dictimes.items():
        if 0 < t:
            s = "time[%s] = %s" % (key, t)
            lines.append(s)
            print(s)
    if lines:
        with open("performance.txt", "w") as f:
            f.write("\n".join(lines))
            f.flush()
            f.close()


# ------------------------------------------------------------------------------
# 同時起動制御
# ------------------------------------------------------------------------------

if sys.platform == "win32":
    class _OVERLAPPED(ctypes.Structure):
        _fields_ = [
            ('Internal', ctypes.wintypes.DWORD),
            ('InternalHigh', ctypes.wintypes.DWORD),
            ('Offset', ctypes.wintypes.DWORD),
            ('OffsetHigh', ctypes.wintypes.DWORD),
            ('hEvent', ctypes.wintypes.HANDLE),
        ]

    class _Unlock(object):
        def __init__(self, name: str, f: TextIO) -> None:
            self.name = name
            self.f: Optional[TextIO] = f

        def unlock(self) -> None:
            if self.f:
                kernel32 = ctypes.windll.kernel32
                handle = msvcrt.get_osfhandle(self.f.fileno())
                kernel32.UnlockFileEx(handle, 0, 0, 0xffff0000, ctypes.byref(_OVERLAPPED()))
                self.f = None
                remove(self.name)

    _mutex: List[Tuple[_Unlock, str]] = []

else:
    import fcntl

    _mutex: List[Tuple[BinaryIO, str]] = []

_lock_mutex = threading.Lock()


@synclock(_lock_mutex)
def create_mutex(dpath: str) -> bool:
    global _mutex
    if not os.path.isabs(dpath):
        dpath = os.path.abspath(dpath)
    dpath = os.path.normpath(dpath)
    dpath = os.path.normcase(dpath)
    name = os.path.join(dpath, ".CardWirthPy.lock")

    # 二重起動防止 for Windows
    if sys.platform == "win32":
        # BUG: win32file.LockFileEx()とwin32file.UnlockFileEx()を使うと、
        #      なぜかこの関数を抜けた後でロック解除がうまくいかなくなる
        kernel32 = ctypes.windll.kernel32

        f = open(name, "w")
        handle = msvcrt.get_osfhandle(f.fileno())
        if kernel32.LockFileEx(handle,
                               win32con.LOCKFILE_FAIL_IMMEDIATELY | win32con.LOCKFILE_EXCLUSIVE_LOCK,
                               0, 0, 0xffff0000, ctypes.byref(_OVERLAPPED())):
            _mutex.append((_Unlock(name, f), name))
            return True
        else:
            return False
    else:
        # Posix
        try:
            if not os.path.isfile(name):
                dpath = os.path.dirname(name)
                if not os.path.isdir(dpath):
                    os.makedirs(dpath)
            f = open(name, "wb", 0)
            fcntl.flock(f.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
            _mutex.append((f, name))
            return True
        except IOError:
            return False


@synclock(_lock_mutex)
def exists_mutex(dpath: str) -> bool:
    global _mutex
    if not os.path.isabs(dpath):
        dpath = os.path.abspath(dpath)
    dpath = os.path.normpath(dpath)
    dpath = os.path.normcase(dpath)
    name = os.path.join(dpath, ".CardWirthPy.lock")
    if name in [m[1] for m in _mutex]:
        return False

    if sys.platform == "win32":
        try:
            if not os.path.isfile(name):
                return False

            with open(name, "w") as f:
                pass
            remove(name)
        except Exception:
            return True

        return False

    else:
        # Posix
        try:
            if not os.path.isfile(name):
                dpath = os.path.dirname(name)
                if not os.path.isdir(dpath):
                    os.makedirs(dpath)
            with open(name, "wb") as f:
                fcntl.flock(f.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
                fcntl.flock(f.fileno(), fcntl.LOCK_UN)
                f.close()
            remove(name)
            return False
        except IOError:
            return True


@synclock(_lock_mutex)
def release_mutex(index: int = -1) -> None:
    global _mutex
    if _mutex:
        if sys.platform == "win32":
            _mutex[index][0].unlock()
        else:
            fio = _mutex[index][0]
            assert isinstance(fio, io.FileIO)
            fcntl.flock(fio.fileno(), fcntl.LOCK_UN)
            fio.close()
            remove(_mutex[index][1])
        _mutex.pop(index)


@synclock(_lock_mutex)
def clear_mutex() -> None:
    global _mutex
    for mutex, name in _mutex:
        if sys.platform == "win32":
            mutex.unlock()
        else:
            f = mutex
            fcntl.flock(f.fileno(), fcntl.LOCK_UN)
            f.close()
            remove(name)
    _mutex = []


def main() -> None:
    # FIXME: cx_Freeze用にimportしているwin32timezoneの未使用警告を潰す
    if sys.platform == "win32":
        win32timezone.now()


if __name__ == "__main__":
    main()
